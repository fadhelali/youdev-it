<?php

namespace App\Tests;

use App\Entity\Site;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    public function testSomething(): void
    {
        $site = new Site();
      
           $site->setAuthor('fadhel');
           $site->setName('fadhel');
           $site->setDescription('test description');
           $site->setUrl('/site/test');
           $site->setTheme('default');
             $date = new \DateTime('now');
            $site->setCreatedAt($date);
            $site->setUpdatedAt($date);
            $site->setPublished(false);
            $this->assertTrue($site->getAuthor()==='fadhel');
            $this->assertTrue($site->getName()==='fadhel');
            $this->assertTrue($site->getDescription()==='test description');
            $this->assertTrue($site->getUrl()==='/site/test');
            $this->assertTrue($site->getTheme()==='default');

    }
}
