<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateSiteTestCaseTest extends KernelTestCase
{
    public function testSomething(): void
    {

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('youdevcms:site:create');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            // pass arguments to the helper
            'name' => 'salah',
            'url' => '/salah',
            'author' => 'alifadhel',
            'description' => 'descriptions',

            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('name: salah', $output);
        $this->assertContains('url: /salah', $output);
        $this->assertContains('author: alifadhel', $output);
        $this->assertContains('description: descriptions', $output);
    }
}
