<?php

namespace App\Tests;

use App\Entity\Site;
use PHPUnit\Framework\TestCase;

class SiteIdTest extends TestCase
{
    public function testSomething(): void
    {
        $site = new Site();
      
        $site->setAuthor('ssss');
        $this->assertTrue($site->getAuthor()==='ssss');
        $this->assertTrue($site->getId()!==0);

    }
}
