<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/cms/contactblock' => [[['_route' => 'cms_contactblock_index', '_controller' => 'App\\Controller\\ContactController::index'], null, null, null, true, false, null]],
        '/create-checkout-session' => [[['_route' => 'checkout', '_controller' => 'App\\Controller\\DefaultController::checkout'], null, null, null, false, false, null]],
        '/success' => [[['_route' => 'success', '_controller' => 'App\\Controller\\DefaultController::success'], null, null, null, false, false, null]],
        '/error' => [[['_route' => 'error', '_controller' => 'App\\Controller\\DefaultController::error'], null, null, null, false, false, null]],
        '/noti' => [[['_route' => 'youdev_seen_notification', '_controller' => 'App\\Controller\\DefaultController::notiAction'], null, ['GET' => 0], null, true, false, null]],
        '/cms/feature' => [[['_route' => 'cms_feature_index', '_controller' => 'App\\Controller\\FeatureController::index'], null, null, null, true, false, null]],
        '/cms/footer' => [[['_route' => 'cms_footer_index', '_controller' => 'App\\Controller\\FooterController::index'], null, null, null, true, false, null]],
        '/cms/forum' => [[['_route' => 'cms_forum_index', '_controller' => 'App\\Controller\\ForumController::index'], null, null, null, true, false, null]],
        '/cms/galleryy' => [[['_route' => 'cms_gallery_index', '_controller' => 'App\\Controller\\GalleryController::index'], null, null, null, true, false, null]],
        '/cms/header' => [[['_route' => 'cms_header_index', '_controller' => 'App\\Controller\\HeaderController::index'], null, null, null, true, false, null]],
        '/cms/imagevideo' => [[['_route' => 'cms_imagevideo_index', '_controller' => 'App\\Controller\\ImageVideoController::index'], null, null, null, true, false, null]],
        '/cms/menu' => [[['_route' => 'cms_menu_index', '_controller' => 'App\\Controller\\MenuController::index'], null, ['GET' => 0], null, true, false, null]],
        '/cms/news' => [[['_route' => 'cms_news_index', '_controller' => 'App\\Controller\\NewsController::index'], null, null, null, true, false, null]],
        '/cms/pageservice' => [[['_route' => 'cms_pageservice_index', '_controller' => 'App\\Controller\\PageServiceController::index'], null, null, null, true, false, null]],
        '/cms/pricing' => [[['_route' => 'cms_pricing_index', '_controller' => 'App\\Controller\\PricingController::index'], null, null, null, true, false, null]],
        '/cms/services' => [[['_route' => 'cms_service_index', '_controller' => 'App\\Controller\\ServicesController::index'], null, null, null, true, false, null]],
        '/cms/teamMembre' => [[['_route' => 'cms_team_membre_index', '_controller' => 'App\\Controller\\TeamMembreController::index'], null, null, null, true, false, null]],
        '/cms/topic' => [[['_route' => 'cms_topic_index', '_controller' => 'App\\Controller\\TopicController::index'], null, null, null, true, false, null]],
        '/cms/invitation_user' => [[['_route' => 'cms_UserInvit_index', '_controller' => 'App\\Controller\\UserInvitController::index'], null, null, null, true, false, null]],
        '/cms/invitation_user/new' => [[['_route' => 'cms_UserInvit_new', '_controller' => 'App\\Controller\\UserInvitController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [
            [['_route' => 'fos_user_security_login', '_controller' => 'fos_user.security.controller:loginAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null],
            [['_route' => 'hwi_oauth_connect', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\LoginController::connectAction'], null, null, null, true, false, null],
        ],
        '/login_check' => [[['_route' => 'fos_user_security_check', '_controller' => 'fos_user.security.controller:checkAction'], null, ['POST' => 0], null, false, false, null]],
        '/logout' => [[['_route' => 'fos_user_security_logout', '_controller' => 'fos_user.security.controller:logoutAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/profile' => [[['_route' => 'fos_user_profile_show', '_controller' => 'fos_user.profile.controller:showAction'], null, ['GET' => 0], null, true, false, null]],
        '/register' => [[['_route' => 'fos_user_registration_register', '_controller' => 'fos_user.registration.controller:registerAction'], null, ['GET' => 0, 'POST' => 1], null, true, false, null]],
        '/register/check-email' => [[['_route' => 'fos_user_registration_check_email', '_controller' => 'fos_user.registration.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/register/confirmed' => [[['_route' => 'fos_user_registration_confirmed', '_controller' => 'fos_user.registration.controller:confirmedAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/request' => [[['_route' => 'fos_user_resetting_request', '_controller' => 'fos_user.resetting.controller:requestAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/send-email' => [[['_route' => 'fos_user_resetting_send_email', '_controller' => 'fos_user.resetting.controller:sendEmailAction'], null, ['POST' => 0], null, false, false, null]],
        '/resetting/check-email' => [[['_route' => 'fos_user_resetting_check_email', '_controller' => 'fos_user.resetting.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/profile/change-password' => [[['_route' => 'fos_user_change_password', '_controller' => 'fos_user.change_password.controller:changePasswordAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index1'], null, null, null, false, false, null]],
        '/cms' => [[['_route' => 'youdev_accueil', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/secured/login_facebook' => [[['_route' => 'facebook_login'], null, null, null, false, false, null]],
        '/secured/login_google' => [[['_route' => 'google_login'], null, null, null, false, false, null]],
        '/secured/login_linkedin' => [[['_route' => 'linkedin_login'], null, null, null, false, false, null]],
        '/froalaeditor/upload_image' => [[['_route' => 'kms_froala_editor_upload_image', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadImage'], null, null, null, false, false, null]],
        '/froalaeditor/delete_image' => [[['_route' => 'kms_froala_editor_delete_image', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::deleteImage'], null, null, null, false, false, null]],
        '/froalaeditor/load_images' => [[['_route' => 'kms_froala_editor_load_images', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::loadImages'], null, null, null, false, false, null]],
        '/froalaeditor/upload_file' => [[['_route' => 'kms_froala_editor_upload_file', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadFile'], null, null, null, false, false, null]],
        '/froalaeditor/upload_video' => [[['_route' => 'kms_froala_editor_upload_video', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadVideo'], null, null, null, false, false, null]],
        '/cms/profil' => [[['_route' => 'cms_user_profil', '_controller' => 'App\\Controller\\DefaultController::profil'], null, null, null, false, false, null]],
        '/cms/site/ajouter' => [[['_route' => 'cms_site_ajouter', '_controller' => 'App\\Controller\\DefaultController::AjouterSite'], null, null, null, false, false, null]],
        '/cms/site/add' => [[['_route' => 'cms_add_site', '_controller' => 'App\\Controller\\DefaultController::AddSite'], null, null, null, false, false, null]],
        '/cms/site/list' => [[['_route' => 'cms_site_list', '_controller' => 'App\\Controller\\DefaultController::ListSite'], null, null, null, false, false, null]],
        '/cms/page/list' => [[['_route' => 'cms_page_list', '_controller' => 'App\\Controller\\PageController::ListPage'], null, null, null, false, false, null]],
        '/cms/faq/list' => [[['_route' => 'cms_faq_list', '_controller' => 'App\\Controller\\PageController::ListFaq'], null, null, null, false, false, null]],
        '/cms/faq/add' => [[['_route' => 'cms_add_faq', '_controller' => 'App\\Controller\\PageController::AddFaq'], null, null, null, false, false, null]],
        '/cms/categories/list' => [[['_route' => 'cms_list_categories', '_controller' => 'App\\Controller\\CategoriesController::ListCat'], null, null, null, false, false, null]],
        '/cms/categories/add' => [[['_route' => 'cms_add_categories', '_controller' => 'App\\Controller\\CategoriesController::AddCat'], null, null, null, false, false, null]],
        '/cms/articles/List' => [[['_route' => 'cms_list_articles', '_controller' => 'App\\Controller\\ArticlesController::ListArticle'], null, null, null, false, false, null]],
        '/cms/articles/showlist_article' => [[['_route' => 'cms_show_list_articles', '_controller' => 'App\\Controller\\ArticlesController::ShowListArticle'], null, null, null, false, false, null]],
        '/cms/articles/showlist_article2' => [[['_route' => 'cms_show_list_articles2', '_controller' => 'App\\Controller\\ArticlesController::ShowListArticles'], null, null, null, false, false, null]],
        '/cms/tags/list' => [[['_route' => 'cms_list_tags', '_controller' => 'App\\Controller\\ArticlesController::ListTags'], null, null, null, false, false, null]],
        '/cms/tags/add' => [[['_route' => 'cms_add_tags', '_controller' => 'App\\Controller\\ArticlesController::AddTags'], null, null, null, false, false, null]],
        '/admin/list/themes' => [[['_route' => 'cms_list_themes', '_controller' => 'App\\Controller\\DefaultController::ListThemes'], null, null, null, false, false, null]],
        '/admin/add/themes' => [[['_route' => 'cms_add_themes', '_controller' => 'App\\Controller\\DefaultController::AddThemes'], null, null, null, false, false, null]],
        '/send_mail_contact' => [[['_route' => 'send_mail_contact', '_controller' => 'App\\Controller\\DefaultController::SendMailForClient'], null, null, null, false, false, null]],
        '/admin/users/list/view' => [[['_route' => 'cms_users_list', '_controller' => 'App\\Controller\\DefaultController::ListUsers'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/cms/contactblock/(?'
                    .'|new/contactnew/([^/]++)/new/([^/]++)(*:226)'
                    .'|([^/]++)/(?'
                        .'|updatecontact/([^/]++)/update/([^/]++)(*:284)'
                        .'|deletecontact/([^/]++)/delete/([^/]++)/\\#(*:333)'
                    .')'
                .')'
                .'|/your_notifications/([^/]++)(*:371)'
                .'|/([^/]++)/mark_as_seen/([^/]++)(*:410)'
                .'|/a(?'
                    .'|bs/([^/]++)(*:434)'
                    .'|dmin/(?'
                        .'|u(?'
                            .'|pdate/([^/]++)/themes(*:475)'
                            .'|ser(?'
                                .'|/([^/]++)/user(?'
                                    .'|update(*:512)'
                                    .'|delete(*:526)'
                                .')'
                                .'|s/([^/]++)/enable_disable(*:560)'
                            .')'
                        .')'
                        .'|delete/([^/]++)/themes(*:592)'
                    .')'
                .')'
                .'|/c(?'
                    .'|ms/(?'
                        .'|f(?'
                            .'|eature/(?'
                                .'|new/featureblock/([^/]++)/new/([^/]++)(*:665)'
                                .'|([^/]++)/(?'
                                    .'|update/featureblock/([^/]++)/update/([^/]++)(*:729)'
                                    .'|delete/featureblock/([^/]++)/delete/([^/]++)(*:781)'
                                .')'
                            .')'
                            .'|o(?'
                                .'|oter/(?'
                                    .'|new/blockfooterview/([^/]++)/\\#view(*:838)'
                                    .'|([^/]++)/(?'
                                        .'|updateviewfooter/([^/]++)/\\#update(*:892)'
                                        .'|deleteviewfooter/([^/]++)/\\#delete(*:934)'
                                    .')'
                                .')'
                                .'|rum/(?'
                                    .'|forum/new/([^/]++)(*:969)'
                                    .'|([^/]++)/(?'
                                        .'|updateforum/update/([^/]++)(*:1016)'
                                        .'|deletenews/delete/([^/]++)/\\#(*:1054)'
                                    .')'
                                .')'
                            .')'
                            .'|aq/([^/]++)/(?'
                                .'|upadte(*:1087)'
                                .'|delete(*:1102)'
                                .'|show_list(*:1120)'
                                .'|pub(*:1132)'
                            .')'
                        .')'
                        .'|galleryy/(?'
                            .'|new/([^/]++)/site/([^/]++)(*:1181)'
                            .'|([^/]++)/(?'
                                .'|update/([^/]++)/gallery/([^/]++)(*:1234)'
                                .'|delete/([^/]++)/([^/]++)(*:1267)'
                            .')'
                            .'|supprime/image/([^/]++)(*:1300)'
                        .')'
                        .'|header/(?'
                            .'|new/([^/]++)/([^/]++)(*:1341)'
                            .'|([^/]++)/(?'
                                .'|update/([^/]++)/([^/]++)/updateheader(*:1399)'
                                .'|delete/([^/]++)/([^/]++)(*:1432)'
                            .')'
                        .')'
                        .'|i(?'
                            .'|magevideo/(?'
                                .'|new/([^/]++)/blockvideo/([^/]++)(*:1492)'
                                .'|([^/]++)/(?'
                                    .'|updateimagevideo/([^/]++)/update/([^/]++)(*:1554)'
                                    .'|deleteimagevideo/([^/]++)/delete/([^/]++)(*:1604)'
                                .')'
                            .')'
                            .'|nvit(?'
                                .'|ation_user/([^/]++)/(?'
                                    .'|update(*:1651)'
                                    .'|delete(*:1666)'
                                .')'
                                .'|post/([^/]++)/view/([^/]++)(*:1703)'
                            .')'
                            .'|tem/([^/]++)/(?'
                                .'|show_subitem(*:1741)'
                                .'|add_subitem(*:1761)'
                                .'|([^/]++)/(?'
                                    .'|update_subitem(*:1796)'
                                    .'|delete_subitem(*:1819)'
                                .')'
                            .')'
                        .')'
                        .'|menu(?'
                            .'|/(?'
                                .'|new/menublock/([^/]++)/new(*:1868)'
                                .'|([^/]++)(?'
                                    .'|(*:1888)'
                                    .'|/(?'
                                        .'|editblockmenu/([^/]++)/edit(*:1928)'
                                        .'|deleteblockmenu/([^/]++)/delete(*:1968)'
                                        .'|check_active(*:1989)'
                                        .'|show_item(*:2007)'
                                        .'|add_item/block/([^/]++)/add(*:2043)'
                                        .'|([^/]++)/delete_item/([^/]++)/delete(*:2088)'
                                        .'|item/([^/]++)/edit_item/([^/]++)/edit(*:2134)'
                                    .')'
                                .')'
                            .')'
                            .'|block/([^/]++)/show_item/([^/]++)(*:2179)'
                        .')'
                        .'|news/(?'
                            .'|news/new/([^/]++)(*:2214)'
                            .'|([^/]++)/(?'
                                .'|updatenews/update/([^/]++)(*:2261)'
                                .'|deletenews/delete/([^/]++)/\\#(*:2299)'
                            .')'
                        .')'
                        .'|p(?'
                            .'|age(?'
                                .'|service/(?'
                                    .'|new/serviceinvit/([^/]++)/newservice/([^/]++)(*:2376)'
                                    .'|([^/]++)/(?'
                                        .'|update/([^/]++)/updateservice_block/([^/]++)/up(*:2444)'
                                        .'|delete/([^/]++)/deleteinvit_blockservice/([^/]++)/delete(*:2509)'
                                    .')'
                                .')'
                                .'|/([^/]++)/(?'
                                    .'|add(*:2536)'
                                    .'|update(*:2551)'
                                    .'|show(*:2564)'
                                    .'|delete(*:2579)'
                                    .'|pub(*:2591)'
                                .')'
                            .')'
                            .'|ricing/(?'
                                .'|new/([^/]++)/pricing/([^/]++)(*:2641)'
                                .'|([^/]++)/(?'
                                    .'|update/([^/]++)/pricingupdate/([^/]++)(*:2700)'
                                    .'|delete/([^/]++)/deletepricing/([^/]++)(*:2747)'
                                .')'
                            .')'
                        .')'
                        .'|s(?'
                            .'|ervices/(?'
                                .'|new/services/([^/]++)/newservice(*:2806)'
                                .'|([^/]++)/(?'
                                    .'|update/([^/]++)/updateservice(*:2856)'
                                    .'|delete/([^/]++)/deleteservices(*:2895)'
                                .')'
                            .')'
                            .'|ite/(?'
                                .'|([^/]++)/update(*:2928)'
                                .'|delete/([^/]++)/deletesite/([^/]++)(*:2972)'
                                .'|update/([^/]++)/\\#jfgfg/([^/]++)(*:3013)'
                                .'|s(?'
                                    .'|ervice(?'
                                        .'|/([^/]++)/(?'
                                            .'|([^/]++)/delete(*:3063)'
                                            .'|view(*:3076)'
                                        .')'
                                        .'|invit/([^/]++)/view(*:3105)'
                                    .')'
                                    .'|ite blockinvit/([^/]++)/view(*:3143)'
                                .')'
                                .'|([^/]++)/pub(*:3165)'
                            .')'
                        .')'
                        .'|t(?'
                            .'|eamMembre/(?'
                                .'|new/eamblock/([^/]++)/new/([^/]++)(*:3227)'
                                .'|([^/]++)/(?'
                                    .'|update/teaminvit/([^/]++)/update/([^/]++)(*:3289)'
                                    .'|delete/teaminvitblock/([^/]++)/delete/([^/]++)(*:3344)'
                                .')'
                            .')'
                            .'|opic/(?'
                                .'|new/topic/([^/]++)(*:3381)'
                                .'|([^/]++)/(?'
                                    .'|update/topic/([^/]++)(*:3423)'
                                    .'|delete/([^/]++)(*:3447)'
                                .')'
                            .')'
                            .'|ags/([^/]++)/(?'
                                .'|delete(*:3480)'
                                .'|update(*:3495)'
                            .')'
                            .'|heme/([^/]++)/(?'
                                .'|add_to_site(*:3533)'
                                .'|([^/]++)/apply_theme(*:3562)'
                            .')'
                        .')'
                        .'|user/editprofil/([^/]++)(*:3597)'
                        .'|c(?'
                            .'|ategories/([^/]++)/(?'
                                .'|update(*:3638)'
                                .'|delete(*:3653)'
                                .'|show_informations(*:3679)'
                            .')'
                            .'|omments/(?'
                                .'|([^/]++)/add(*:3712)'
                                .'|article/([^/]++)/([^/]++)/(?'
                                    .'|delete(*:3756)'
                                    .'|update(?'
                                        .'|(*:3774)'
                                        .'|_article(*:3791)'
                                    .')'
                                .')'
                            .')'
                        .')'
                        .'|articles/([^/]++)/(?'
                            .'|([^/]++)/(?'
                                .'|add(*:3840)'
                                .'|updatearticle/\\#([^/]++)(*:3873)'
                                .'|deletearticle/\\#([^/]++)(*:3906)'
                            .')'
                            .'|show(*:3920)'
                        .')'
                        .'|demo/([^/]++)/(?'
                            .'|t(?'
                                .'|hemes(*:3956)'
                                .'|eam_themes(*:3975)'
                            .')'
                            .'|about_themes(*:3997)'
                            .'|s(?'
                                .'|ervice_themes(*:4023)'
                                .'|hop_themes(*:4042)'
                            .')'
                            .'|blog_themes(*:4063)'
                            .'|contact_themes(*:4086)'
                        .')'
                        .'|view(?'
                            .'|i(?'
                                .'|nvit(?'
                                    .'|/([^/]++)/([^/]++)/view(*:4137)'
                                    .'|page/([^/]++)/([^/]++)/pageview(*:4177)'
                                .')'
                                .'|magevideo/invitblock/([^/]++)/([^/]++)/view(*:4230)'
                            .')'
                            .'|menu/invitblock/([^/]++)/view(*:4269)'
                            .'|f(?'
                                .'|ooter/invitblock/([^/]++)/view(*:4312)'
                                .'|eature/invitblock/([^/]++)/([^/]++)/view(*:4361)'
                            .')'
                            .'|header/invitblock/([^/]++)/([^/]++)/view(*:4411)'
                            .'|gallery/invitblock/([^/]++)/([^/]++)/view(*:4461)'
                            .'|pricing/invitblock/([^/]++)/([^/]++)/view(*:4511)'
                            .'|team/invitblock/([^/]++)/([^/]++)/view(*:4558)'
                            .'|services/invitblock/([^/]++)/([^/]++)/view(*:4609)'
                            .'|contact/invitblock/([^/]++)/([^/]++)/view(*:4659)'
                            .'|news/invitblock/([^/]++)/view(*:4697)'
                        .')'
                        .'|([^/]++)/(?'
                            .'|change_state/(?'
                                .'|topic_messages(*:4749)'
                                .'|article_comments(*:4774)'
                            .')'
                            .'|topic/(?'
                                .'|view_topic_messages(*:4812)'
                                .'|([^/]++)/delete_topic_messages(*:4851)'
                            .')'
                        .')'
                    .')'
                    .'|onnect/(?'
                        .'|([^/]++)(*:4881)'
                        .'|service/([^/]++)(*:4906)'
                        .'|registration/([^/]++)(*:4936)'
                    .')'
                    .'|hange_locale/([^/]++)(*:4967)'
                .')'
                .'|/profile/edit/([^/]++)(*:4999)'
                .'|/re(?'
                    .'|gister/confirm/([^/]++)(*:5037)'
                    .'|setting/reset/([^/]++)(*:5068)'
                .')'
                .'|/notifications/(?'
                    .'|([^/]++)(*:5104)'
                    .'|polo/([^/]++)(*:5126)'
                    .'|([^/]++)/mark(?'
                        .'|_as_(?'
                            .'|seen/([^/]++)(*:5171)'
                            .'|unseen/([^/]++)(*:5195)'
                        .')'
                        .'|AllAsSeen(*:5214)'
                    .')'
                    .'|ab(?'
                        .'|s/([^/]++)(*:5239)'
                        .'|/([^/]++)(*:5257)'
                    .')'
                    .'|noti(*:5271)'
                .')'
                .'|/front/(?'
                    .'|([^/]++)(?'
                        .'|(*:5302)'
                        .'|/(?'
                            .'|([^/]++)/(?'
                                .'|news(*:5331)'
                                .'|view_page(*:5349)'
                            .')'
                            .'|faqs_page(*:5368)'
                            .'|blog_page(*:5386)'
                        .')'
                    .')'
                    .'|mail/([^/]++)/([^/]++)/send_mail(*:5429)'
                    .'|post/([^/]++)/([^/]++)/(?'
                        .'|read_more(*:5473)'
                        .'|add_comment_post(*:5498)'
                        .'|update_comment_post/([^/]++)/update(*:5542)'
                        .'|delete_comment_post/([^/]++)/delete(*:5586)'
                        .'|form_update_comment_post/([^/]++)/update(*:5635)'
                    .')'
                    .'|([^/]++)/(?'
                        .'|serach(*:5663)'
                        .'|list_forum(*:5682)'
                        .'|forum/([^/]++)/list_topic(*:5716)'
                        .'|topic/([^/]++)/list_messages(*:5753)'
                        .'|add_message/([^/]++)/topic_messages(*:5797)'
                        .'|update_message/([^/]++)/topic/([^/]++)/(?'
                            .'|topic_messages(*:5862)'
                            .'|form_message_update(*:5890)'
                        .')'
                        .'|delete_message/([^/]++)/topic/([^/]++)/topic_messages(*:5953)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        226 => [[['_route' => 'cms_contactblock_new', '_controller' => 'App\\Controller\\ContactController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        284 => [[['_route' => 'cms_contactblock_update', '_controller' => 'App\\Controller\\ContactController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        333 => [[['_route' => 'cms_contactblock_delete', '_controller' => 'App\\Controller\\ContactController::delete'], ['id', 'idpage', 'idsite'], null, null, false, false, null]],
        371 => [[['_route' => 'youdev_notificationbyusers', '_controller' => 'App\\Controller\\DefaultController::listbyuserAction'], ['identifier'], ['GET' => 0], null, false, true, null]],
        410 => [[['_route' => 'seen_list2', '_controller' => 'App\\Controller\\DefaultController::markAsSeenAction'], ['notifiable', 'lu'], ['GET' => 0], null, false, true, null]],
        434 => [[['_route' => 'seen_list', '_controller' => 'App\\Controller\\DefaultController::luAction'], ['lu'], ['GET' => 0], null, false, true, null]],
        475 => [[['_route' => 'cms_update_themes', '_controller' => 'App\\Controller\\DefaultController::UpdateThemes'], ['id'], null, null, false, false, null]],
        512 => [[['_route' => 'cms_update_users', '_controller' => 'App\\Controller\\DefaultController::UpdateUsers'], ['id'], null, null, false, false, null]],
        526 => [[['_route' => 'cms_delete_user', '_controller' => 'App\\Controller\\DefaultController::DeleteUsers'], ['id'], null, null, false, false, null]],
        560 => [[['_route' => 'cms_enable_users', '_controller' => 'App\\Controller\\DefaultController::EnableDisableUser'], ['id'], null, null, true, false, null]],
        592 => [[['_route' => 'cms_delete_themes', '_controller' => 'App\\Controller\\DefaultController::DeleteThemes'], ['id'], null, null, false, false, null]],
        665 => [[['_route' => 'cms_feature_new', '_controller' => 'App\\Controller\\FeatureController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        729 => [[['_route' => 'cms_feature_update', '_controller' => 'App\\Controller\\FeatureController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        781 => [[['_route' => 'cms_feature_delete', '_controller' => 'App\\Controller\\FeatureController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        838 => [[['_route' => 'cms_footer_new', '_controller' => 'App\\Controller\\FooterController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        892 => [[['_route' => 'cms_footer_update', '_controller' => 'App\\Controller\\FooterController::update'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        934 => [[['_route' => 'cms_footer_delete', '_controller' => 'App\\Controller\\FooterController::delete'], ['id', 'idsite'], null, null, false, false, null]],
        969 => [[['_route' => 'cms_forum_new', '_controller' => 'App\\Controller\\ForumController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1016 => [[['_route' => 'cms_update_forum', '_controller' => 'App\\Controller\\ForumController::update'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1054 => [[['_route' => 'cms_forum_delete', '_controller' => 'App\\Controller\\ForumController::delete'], ['id', 'idsite'], null, null, false, false, null]],
        1087 => [[['_route' => 'cms_update_faq', '_controller' => 'App\\Controller\\PageController::UpdateFaq'], ['id'], null, null, false, false, null]],
        1102 => [[['_route' => 'cms_delete_faq', '_controller' => 'App\\Controller\\PageController::DeleteFaq'], ['id'], null, null, false, false, null]],
        1120 => [[['_route' => 'cms_showpage_faq', '_controller' => 'App\\Controller\\PageController::ShowFaq'], ['id'], null, null, false, false, null]],
        1132 => [[['_route' => 'pub_faq', '_controller' => 'App\\Controller\\PageController::PubFaq'], ['id'], null, null, false, false, null]],
        1181 => [[['_route' => 'cms_gallery_new', '_controller' => 'App\\Controller\\GalleryController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1234 => [[['_route' => 'cms_gallery_update', '_controller' => 'App\\Controller\\GalleryController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1267 => [[['_route' => 'cms_gallery_delete', '_controller' => 'App\\Controller\\GalleryController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        1300 => [[['_route' => 'cms_delete_image', '_controller' => 'App\\Controller\\GalleryController::deleteImage'], ['id'], ['DELETE' => 0], null, false, true, null]],
        1341 => [[['_route' => 'cms_header_new', '_controller' => 'App\\Controller\\HeaderController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1399 => [[['_route' => 'cms_header_update', '_controller' => 'App\\Controller\\HeaderController::update'], ['id', 'idsite', 'idpage'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1432 => [[['_route' => 'cms_header_delete', '_controller' => 'App\\Controller\\HeaderController::delete'], ['id', 'idsite', 'idpage'], null, null, false, true, null]],
        1492 => [[['_route' => 'cms_imagevideo_new', '_controller' => 'App\\Controller\\ImageVideoController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1554 => [[['_route' => 'cms_imagevideo_update', '_controller' => 'App\\Controller\\ImageVideoController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1604 => [[['_route' => 'cms_imagevideo_delete', '_controller' => 'App\\Controller\\ImageVideoController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        1651 => [[['_route' => 'cms_UserInvit_update', '_controller' => 'App\\Controller\\UserInvitController::update'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1666 => [[['_route' => 'cms_UserInvit_delete', '_controller' => 'App\\Controller\\UserInvitController::delete'], ['id'], null, null, false, false, null]],
        1703 => [[['_route' => 'cms_post_invit_view', '_controller' => 'App\\Controller\\DefaultController::ViewInvitPost'], ['name', 'idsite'], null, null, false, true, null]],
        1741 => [[['_route' => 'cms_menu_show_subitems', '_controller' => 'App\\Controller\\MenuController::ShowSubItem'], ['id'], null, null, false, false, null]],
        1761 => [[['_route' => 'cms_menu_add_subitem', '_controller' => 'App\\Controller\\MenuController::AddSubItem'], ['id'], null, null, false, false, null]],
        1796 => [[['_route' => 'cms_menu_update_subitem', '_controller' => 'App\\Controller\\MenuController::UpdateSubItem'], ['id', 'idi'], null, null, false, false, null]],
        1819 => [[['_route' => 'cms_menu_delete_subitem', '_controller' => 'App\\Controller\\MenuController::DeleteSubItem'], ['id', 'idi'], null, null, false, false, null]],
        1868 => [[['_route' => 'cms_menu_new', '_controller' => 'App\\Controller\\MenuController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1888 => [[['_route' => 'cms_menu_show', '_controller' => 'App\\Controller\\MenuController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        1928 => [[['_route' => 'cms_menu_edit', '_controller' => 'App\\Controller\\MenuController::edit'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1968 => [[['_route' => 'cms_menu_delete', '_controller' => 'App\\Controller\\MenuController::delete'], ['id', 'idsite'], null, null, false, false, null]],
        1989 => [[['_route' => 'cms_menu_active', '_controller' => 'App\\Controller\\MenuController::CheckMenu'], ['id'], null, null, false, false, null]],
        2007 => [[['_route' => 'cms_menu_show_item', '_controller' => 'App\\Controller\\MenuController::ShowItem'], ['id'], null, null, false, false, null]],
        2043 => [[['_route' => 'cms_menu_add_item', '_controller' => 'App\\Controller\\MenuController::AddItem'], ['id', 'idsite'], null, null, false, false, null]],
        2088 => [[['_route' => 'cms_menu_delete_item', '_controller' => 'App\\Controller\\MenuController::DeleteItem'], ['id', 'idm', 'idsite'], null, null, false, false, null]],
        2134 => [[['_route' => 'cms_menu_edit_item', '_controller' => 'App\\Controller\\MenuController::EditItem'], ['id', 'idi', 'idsite'], null, null, false, false, null]],
        2179 => [[['_route' => 'cms_menu_show_itemblock', '_controller' => 'App\\Controller\\MenuController::ShowItemBlock'], ['id', 'idsite'], null, null, false, true, null]],
        2214 => [[['_route' => 'cms_news_new', '_controller' => 'App\\Controller\\NewsController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2261 => [[['_route' => 'cms_news_update', '_controller' => 'App\\Controller\\NewsController::update'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2299 => [[['_route' => 'cms_news_delete', '_controller' => 'App\\Controller\\NewsController::delete'], ['id', 'idsite'], null, null, false, false, null]],
        2376 => [[['_route' => 'cms_pageservice_new', '_controller' => 'App\\Controller\\PageServiceController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2444 => [[['_route' => 'cms_pageservice_update', '_controller' => 'App\\Controller\\PageServiceController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        2509 => [[['_route' => 'cms_pageservice_delete', '_controller' => 'App\\Controller\\PageServiceController::delete'], ['id', 'idpage', 'idsite'], null, null, false, false, null]],
        2536 => [[['_route' => 'cms_add_page', '_controller' => 'App\\Controller\\PageController::AddPage'], ['idsite'], null, null, false, false, null]],
        2551 => [[['_route' => 'cms_update_page', '_controller' => 'App\\Controller\\PageController::UpdatePage'], ['id'], null, null, false, false, null]],
        2564 => [[['_route' => 'cms_show_page', '_controller' => 'App\\Controller\\PageController::ShowPage'], ['id'], null, null, false, false, null]],
        2579 => [[['_route' => 'cms_delete_page', '_controller' => 'App\\Controller\\PageController::DeletePage'], ['id'], null, null, false, false, null]],
        2591 => [[['_route' => 'pub_page', '_controller' => 'App\\Controller\\PageController::PubPage'], ['id'], null, null, false, false, null]],
        2641 => [[['_route' => 'cms_pricing_new', '_controller' => 'App\\Controller\\PricingController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2700 => [[['_route' => 'cms_pricing_update', '_controller' => 'App\\Controller\\PricingController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2747 => [[['_route' => 'cms_pricing_delete', '_controller' => 'App\\Controller\\PricingController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        2806 => [[['_route' => 'cms_services_new', '_controller' => 'App\\Controller\\ServicesController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        2856 => [[['_route' => 'cms_service_update', '_controller' => 'App\\Controller\\ServicesController::update'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        2895 => [[['_route' => 'cms_service_delete', '_controller' => 'App\\Controller\\ServicesController::delete'], ['id', 'idsite'], null, null, false, false, null]],
        2928 => [[['_route' => 'cms_update_site', '_controller' => 'App\\Controller\\DefaultController::ModifySite'], ['id'], null, null, false, false, null]],
        2972 => [[['_route' => 'cms_site_delete', '_controller' => 'App\\Controller\\DefaultController::DeleteSite'], ['id', 'idsite'], null, null, false, true, null]],
        3013 => [[['_route' => 'cms_site_update', '_controller' => 'App\\Controller\\DefaultController::UpdateSite'], ['id', 'idsite'], null, null, false, true, null]],
        3063 => [[['_route' => 'cms_site_delete_services', '_controller' => 'App\\Controller\\DefaultController::DeleteServices'], ['id', 'ids'], null, null, false, false, null]],
        3076 => [[['_route' => 'cms_site_view_services', '_controller' => 'App\\Controller\\DefaultController::ViewServices'], ['id'], null, null, false, false, null]],
        3105 => [[['_route' => 'cms_site_view_servicesInvit', '_controller' => 'App\\Controller\\DefaultController::ViewServicesInvit'], ['id'], null, null, false, false, null]],
        3143 => [[['_route' => 'cms_site_view_siteblock', '_controller' => 'App\\Controller\\DefaultController::ViewSiteInvit'], ['id'], null, null, false, false, null]],
        3165 => [[['_route' => 'pub_site', '_controller' => 'App\\Controller\\DefaultController::PubSite'], ['id'], null, null, false, false, null]],
        3227 => [[['_route' => 'cms_team_membre_new', '_controller' => 'App\\Controller\\TeamMembreController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3289 => [[['_route' => 'cms_team_membre_update', '_controller' => 'App\\Controller\\TeamMembreController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3344 => [[['_route' => 'cms_team_membre_delete', '_controller' => 'App\\Controller\\TeamMembreController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        3381 => [[['_route' => 'cms_topic_new', '_controller' => 'App\\Controller\\TopicController::new'], ['idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3423 => [[['_route' => 'cms_topic_update', '_controller' => 'App\\Controller\\TopicController::update'], ['id', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3447 => [[['_route' => 'cms_topic_delete', '_controller' => 'App\\Controller\\TopicController::delete'], ['id', 'idsite'], null, null, false, true, null]],
        3480 => [[['_route' => 'cms_delete_tags', '_controller' => 'App\\Controller\\ArticlesController::DeleteTags'], ['id'], null, null, false, false, null]],
        3495 => [[['_route' => 'cms_update_tags', '_controller' => 'App\\Controller\\ArticlesController::UpdateTags'], ['id'], null, null, false, false, null]],
        3533 => [[['_route' => 'cms_add_theme_site', '_controller' => 'App\\Controller\\DefaultController::AddSiteTheme'], ['id'], null, null, false, false, null]],
        3562 => [[['_route' => 'cms_apply_themes', '_controller' => 'App\\Controller\\DefaultController::ApplyTheme'], ['id', 'idt'], null, null, false, false, null]],
        3597 => [[['_route' => 'cms_user_profil_edit', '_controller' => 'App\\Controller\\DefaultController::Editprofil'], ['id'], null, null, false, true, null]],
        3638 => [[['_route' => 'cms_update_categories', '_controller' => 'App\\Controller\\CategoriesController::UpdateCat'], ['id'], null, null, false, false, null]],
        3653 => [[['_route' => 'cms_delete_categories', '_controller' => 'App\\Controller\\CategoriesController::DeleteCat'], ['id'], null, null, false, false, null]],
        3679 => [[['_route' => 'cms_show_categories', '_controller' => 'App\\Controller\\CategoriesController::ShowCat'], ['id'], null, null, false, false, null]],
        3712 => [[['_route' => 'cms_add_comments', '_controller' => 'App\\Controller\\ArticlesController::AddComment'], ['id'], null, null, false, false, null]],
        3756 => [[['_route' => 'cms_delete_comments', '_controller' => 'App\\Controller\\ArticlesController::DeleteComment'], ['idt', 'id'], null, null, false, false, null]],
        3774 => [[['_route' => 'cms_update_comments', '_controller' => 'App\\Controller\\ArticlesController::UpdateComment'], ['idt', 'id'], null, null, false, false, null]],
        3791 => [[['_route' => 'cms_update_comment_article', '_controller' => 'App\\Controller\\ArticlesController::UpdateArticleComment'], ['id', 'idd'], null, null, false, false, null]],
        3840 => [[['_route' => 'cms_add_articles', '_controller' => 'App\\Controller\\ArticlesController::AddArticle'], ['name', 'idsite'], null, null, false, false, null]],
        3873 => [[['_route' => 'cms_update_articles', '_controller' => 'App\\Controller\\ArticlesController::UpdateArticle'], ['id', 'name', 'idsite'], null, null, false, true, null]],
        3906 => [[['_route' => 'cms_delete_articles', '_controller' => 'App\\Controller\\ArticlesController::DeleteArticle'], ['id', 'name', 'idsite'], null, null, false, true, null]],
        3920 => [[['_route' => 'cms_show_articles', '_controller' => 'App\\Controller\\ArticlesController::ShowArticle'], ['id'], null, null, false, false, null]],
        3956 => [[['_route' => 'cms_demo_themes', '_controller' => 'App\\Controller\\DefaultController::DemoThemes'], ['id'], null, null, false, false, null]],
        3975 => [[['_route' => 'cms_demo_team_themes', '_controller' => 'App\\Controller\\DefaultController::TeamThemes'], ['id'], null, null, false, false, null]],
        3997 => [[['_route' => 'cms_demo_about_themes', '_controller' => 'App\\Controller\\DefaultController::AboutThemes'], ['id'], null, null, false, false, null]],
        4023 => [[['_route' => 'cms_demo_service_themes', '_controller' => 'App\\Controller\\DefaultController::ServiceThemes'], ['id'], null, null, false, false, null]],
        4042 => [[['_route' => 'cms_demo_shop_themes', '_controller' => 'App\\Controller\\DefaultController::ShopThemes'], ['id'], null, null, false, false, null]],
        4063 => [[['_route' => 'cms_demo_blog_themes', '_controller' => 'App\\Controller\\DefaultController::BlogThemes'], ['id'], null, null, false, false, null]],
        4086 => [[['_route' => 'cms_demo_contact_themes', '_controller' => 'App\\Controller\\DefaultController::ContactThemes'], ['id'], null, null, false, false, null]],
        4137 => [[['_route' => 'cms_page_invit_view', '_controller' => 'App\\Controller\\DefaultController::ViewInvitPage'], ['idsite', 'name'], null, null, false, false, null]],
        4177 => [[['_route' => 'cms_view_block_page', '_controller' => 'App\\Controller\\DefaultController::ViewBlockPage'], ['id', 'idsite'], null, null, false, false, null]],
        4230 => [[['_route' => 'cms_invit_view_imagevideo', '_controller' => 'App\\Controller\\ImageVideoController::ViewImageVideoInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4269 => [[['_route' => 'cms_invit_view_Menus', '_controller' => 'App\\Controller\\DefaultController::ViewMenuInvit'], ['id'], null, null, false, false, null]],
        4312 => [[['_route' => 'cms_invit_view_footer', '_controller' => 'App\\Controller\\DefaultController::ViewFooterInvit'], ['id'], null, null, false, false, null]],
        4361 => [[['_route' => 'cms_invit_view_feature', '_controller' => 'App\\Controller\\FeatureController::ViewFeatureInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4411 => [[['_route' => 'cms_invit_view_header', '_controller' => 'App\\Controller\\HeaderController::ViewHeaderInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4461 => [[['_route' => 'cms_invit_view_gallery', '_controller' => 'App\\Controller\\GalleryController::ViewGalleryInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4511 => [[['_route' => 'cms_invit_view_pricing', '_controller' => 'App\\Controller\\PricingController::ViewPricingInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4558 => [[['_route' => 'cms_invit_view_team', '_controller' => 'App\\Controller\\TeamMembreController::ViewTeamInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4609 => [[['_route' => 'cms_invit_view_service', '_controller' => 'App\\Controller\\PageServiceController::ViewServiceInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4659 => [[['_route' => 'cms_invit_view_contact', '_controller' => 'App\\Controller\\ContactController::ViewContactInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        4697 => [[['_route' => 'cms_invit_view_news', '_controller' => 'App\\Controller\\NewsController::ViewNewsInvitPage'], ['id'], null, null, false, false, null]],
        4749 => [[['_route' => 'cms_change_state_topic', '_controller' => 'App\\Controller\\ForumController::ChangeState'], ['id'], null, null, false, false, null]],
        4774 => [[['_route' => 'cms_change_state_article', '_controller' => 'App\\Controller\\ArticlesController::ChangeStateComment'], ['id'], null, null, false, false, null]],
        4812 => [[['_route' => 'cms_view_messages_for_topic', '_controller' => 'App\\Controller\\ForumController::ViewMessages'], ['id'], null, null, false, false, null]],
        4851 => [[['_route' => 'cms_message_delete_for_topic', '_controller' => 'App\\Controller\\ForumController::DeleteMessagess'], ['id', 'idsite'], null, null, false, false, null]],
        4881 => [[['_route' => 'hwi_oauth_service_redirect', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\RedirectToServiceController::redirectToServiceAction'], ['service'], null, null, false, true, null]],
        4906 => [[['_route' => 'hwi_oauth_connect_service', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::connectServiceAction'], ['service'], null, null, false, true, null]],
        4936 => [[['_route' => 'hwi_oauth_connect_registration', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::registrationAction'], ['key'], null, null, false, true, null]],
        4967 => [[['_route' => 'change_locale', '_controller' => 'App\\Controller\\DefaultController::ChangeLocale'], ['locale'], null, null, false, true, null]],
        4999 => [[['_route' => 'fos_user_profile_edit', '_controller' => 'fos_user.profile.controller:editAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        5037 => [[['_route' => 'fos_user_registration_confirm', '_controller' => 'fos_user.registration.controller:confirmAction'], ['token'], ['GET' => 0], null, false, true, null]],
        5068 => [[['_route' => 'fos_user_resetting_reset', '_controller' => 'fos_user.resetting.controller:resetAction'], ['token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        5104 => [[['_route' => 'notification_list', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::listAction'], ['notifiable'], null, null, false, true, null]],
        5126 => [[['_route' => 'notificationbyuser', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::listbyuserAction'], ['identifier'], null, null, false, true, null]],
        5171 => [[['_route' => 'notification_mark_as_seenn', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAsSeenAction'], ['notifiable', 'notification'], null, null, false, true, null]],
        5195 => [[['_route' => 'notification_mark_as_unseen', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAsUnSeenAction'], ['notifiable', 'notification'], null, null, false, true, null]],
        5214 => [[['_route' => 'notification_mark_all_as_seen', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::markAllAsSeenAction'], ['notifiable'], null, null, false, false, null]],
        5239 => [[['_route' => 'seen_lists', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::luAction'], ['lu'], null, null, false, true, null]],
        5257 => [[['_route' => 'seen_listNot', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::seenAction'], ['notification'], null, null, false, true, null]],
        5271 => [[['_route' => 'seen_Not', '_controller' => 'Mgilet\\NotificationBundle\\Controller\\NotificationController::notiAction'], [], null, null, true, false, null]],
        5302 => [[['_route' => 'cms_site_view', '_controller' => 'App\\Controller\\DefaultController::ViewSite'], ['name'], null, null, false, true, null]],
        5331 => [[['_route' => 'list_news_front', '_controller' => 'App\\Controller\\DefaultController::ViewNewsFront'], ['name', 'id'], null, null, false, false, null]],
        5349 => [[['_route' => 'cms_view_page', '_controller' => 'App\\Controller\\DefaultController::ViewPage'], ['id', 'idsite'], null, null, false, false, null]],
        5368 => [[['_route' => 'list_faq_front', '_controller' => 'App\\Controller\\DefaultController::FaqPage'], ['id'], null, null, false, false, null]],
        5386 => [[['_route' => 'list_blog_front', '_controller' => 'App\\Controller\\DefaultController::BlogPage'], ['id'], null, null, false, false, null]],
        5429 => [[['_route' => 'send_mail', '_controller' => 'App\\Controller\\DefaultController::SendMail'], ['id', 'ids'], null, null, false, false, null]],
        5473 => [[['_route' => 'cms_article_read_more', '_controller' => 'App\\Controller\\ArticlesController::ReadMore'], ['id', 'ids'], null, null, false, false, null]],
        5498 => [[['_route' => 'cms_add_comment_post', '_controller' => 'App\\Controller\\ArticlesController::AddCommentPost'], ['id', 'ids'], null, null, false, false, null]],
        5542 => [[['_route' => 'cms_update_comment_post', '_controller' => 'App\\Controller\\ArticlesController::UpdateCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        5586 => [[['_route' => 'cms_delete_comment_post', '_controller' => 'App\\Controller\\ArticlesController::DeleteCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        5635 => [[['_route' => 'cms_update_comment_form', '_controller' => 'App\\Controller\\ArticlesController::UpdateFormCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        5663 => [[['_route' => 'cms_search_page', '_controller' => 'App\\Controller\\DefaultController::Search'], ['id'], null, null, false, false, null]],
        5682 => [[['_route' => 'list_forum_front', '_controller' => 'App\\Controller\\ForumController::ListForum'], ['id'], null, null, false, false, null]],
        5716 => [[['_route' => 'cms_view_forum_topic', '_controller' => 'App\\Controller\\ForumController::ListTopicForum'], ['id', 'ids'], null, null, false, false, null]],
        5753 => [[['_route' => 'cms_view_messages_topic', '_controller' => 'App\\Controller\\ForumController::ListTopicMessages'], ['id', 'ids'], null, null, false, false, null]],
        5797 => [[['_route' => 'cms_add_message_topic', '_controller' => 'App\\Controller\\ForumController::AddMessage'], ['id', 'ids'], null, null, false, false, null]],
        5862 => [[['_route' => 'cms_update_message_topic', '_controller' => 'App\\Controller\\ForumController::UpdateMessage'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        5890 => [[['_route' => 'cms_update_message_form', '_controller' => 'App\\Controller\\ForumController::UpdateMessageForm'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        5953 => [
            [['_route' => 'cms_delete_message_topic', '_controller' => 'App\\Controller\\ForumController::DeleteMessage'], ['id', 'idt', 'ids'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
