<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/cms/contactblock' => [[['_route' => 'cms_contactblock_index', '_controller' => 'App\\Controller\\ContactController::index'], null, null, null, true, false, null]],
        '/create-checkout-session' => [[['_route' => 'checkout', '_controller' => 'App\\Controller\\DefaultController::checkout'], null, null, null, false, false, null]],
        '/success' => [[['_route' => 'success', '_controller' => 'App\\Controller\\DefaultController::success'], null, null, null, false, false, null]],
        '/error' => [[['_route' => 'error', '_controller' => 'App\\Controller\\DefaultController::error'], null, null, null, false, false, null]],
        '/cms/feature' => [[['_route' => 'cms_feature_index', '_controller' => 'App\\Controller\\FeatureController::index'], null, null, null, true, false, null]],
        '/cms/footer' => [[['_route' => 'cms_footer_index', '_controller' => 'App\\Controller\\FooterController::index'], null, null, null, true, false, null]],
        '/cms/footer/new' => [[['_route' => 'cms_footer_new', '_controller' => 'App\\Controller\\FooterController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/cms/galleryy' => [[['_route' => 'cms_gallery_index', '_controller' => 'App\\Controller\\GalleryController::index'], null, null, null, true, false, null]],
        '/cms/header' => [[['_route' => 'cms_header_index', '_controller' => 'App\\Controller\\HeaderController::index'], null, null, null, true, false, null]],
        '/cms/imagevideo' => [[['_route' => 'cms_imagevideo_index', '_controller' => 'App\\Controller\\ImageVideoController::index'], null, null, null, true, false, null]],
        '/cms/menu' => [[['_route' => 'cms_menu_index', '_controller' => 'App\\Controller\\MenuController::index'], null, ['GET' => 0], null, true, false, null]],
        '/cms/menu/new' => [[['_route' => 'cms_menu_new', '_controller' => 'App\\Controller\\MenuController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/cms/pageservice' => [[['_route' => 'cms_pageservice_index', '_controller' => 'App\\Controller\\PageServiceController::index'], null, null, null, true, false, null]],
        '/cms/pricing' => [[['_route' => 'cms_pricing_index', '_controller' => 'App\\Controller\\PricingController::index'], null, null, null, true, false, null]],
        '/cms/services' => [[['_route' => 'cms_service_index', '_controller' => 'App\\Controller\\ServicesController::index'], null, null, null, true, false, null]],
        '/cms/services/new' => [[['_route' => 'cms_services_new', '_controller' => 'App\\Controller\\ServicesController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/cms/teamMembre' => [[['_route' => 'cms_team_membre_index', '_controller' => 'App\\Controller\\TeamMembreController::index'], null, null, null, true, false, null]],
        '/cms/invitation_user' => [[['_route' => 'cms_UserInvit_index', '_controller' => 'App\\Controller\\UserInvitController::index'], null, null, null, true, false, null]],
        '/cms/invitation_user/new' => [[['_route' => 'cms_UserInvit_new', '_controller' => 'App\\Controller\\UserInvitController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [
            [['_route' => 'fos_user_security_login', '_controller' => 'fos_user.security.controller:loginAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null],
            [['_route' => 'hwi_oauth_connect', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\LoginController::connectAction'], null, null, null, true, false, null],
        ],
        '/login_check' => [[['_route' => 'fos_user_security_check', '_controller' => 'fos_user.security.controller:checkAction'], null, ['POST' => 0], null, false, false, null]],
        '/logout' => [[['_route' => 'fos_user_security_logout', '_controller' => 'fos_user.security.controller:logoutAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/profile' => [[['_route' => 'fos_user_profile_show', '_controller' => 'fos_user.profile.controller:showAction'], null, ['GET' => 0], null, true, false, null]],
        '/register' => [[['_route' => 'fos_user_registration_register', '_controller' => 'fos_user.registration.controller:registerAction'], null, ['GET' => 0, 'POST' => 1], null, true, false, null]],
        '/register/check-email' => [[['_route' => 'fos_user_registration_check_email', '_controller' => 'fos_user.registration.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/register/confirmed' => [[['_route' => 'fos_user_registration_confirmed', '_controller' => 'fos_user.registration.controller:confirmedAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/request' => [[['_route' => 'fos_user_resetting_request', '_controller' => 'fos_user.resetting.controller:requestAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/send-email' => [[['_route' => 'fos_user_resetting_send_email', '_controller' => 'fos_user.resetting.controller:sendEmailAction'], null, ['POST' => 0], null, false, false, null]],
        '/resetting/check-email' => [[['_route' => 'fos_user_resetting_check_email', '_controller' => 'fos_user.resetting.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/profile/change-password' => [[['_route' => 'fos_user_change_password', '_controller' => 'fos_user.change_password.controller:changePasswordAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/secured/login_facebook' => [[['_route' => 'facebook_login'], null, null, null, false, false, null]],
        '/secured/login_google' => [[['_route' => 'google_login'], null, null, null, false, false, null]],
        '/secured/login_linkedin' => [[['_route' => 'linkedin_login'], null, null, null, false, false, null]],
        '/froalaeditor/upload_image' => [[['_route' => 'kms_froala_editor_upload_image', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadImage'], null, null, null, false, false, null]],
        '/froalaeditor/delete_image' => [[['_route' => 'kms_froala_editor_delete_image', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::deleteImage'], null, null, null, false, false, null]],
        '/froalaeditor/load_images' => [[['_route' => 'kms_froala_editor_load_images', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::loadImages'], null, null, null, false, false, null]],
        '/froalaeditor/upload_file' => [[['_route' => 'kms_froala_editor_upload_file', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadFile'], null, null, null, false, false, null]],
        '/froalaeditor/upload_video' => [[['_route' => 'kms_froala_editor_upload_video', '_controller' => 'KMS\\FroalaEditorBundle\\Controller\\MediaController::uploadVideo'], null, null, null, false, false, null]],
        '/cms/profil' => [[['_route' => 'cms_user_profil', '_controller' => 'App\\Controller\\DefaultController::profil'], null, null, null, false, false, null]],
        '/cms/site/ajouter' => [[['_route' => 'cms_site_ajouter', '_controller' => 'App\\Controller\\DefaultController::AjouterSite'], null, null, null, false, false, null]],
        '/cms/site/add' => [[['_route' => 'cms_add_site', '_controller' => 'App\\Controller\\DefaultController::AddSite'], null, null, null, false, false, null]],
        '/cms/site/list' => [[['_route' => 'cms_site_list', '_controller' => 'App\\Controller\\DefaultController::ListSite'], null, null, null, false, false, null]],
        '/cms/page/list' => [[['_route' => 'cms_page_list', '_controller' => 'App\\Controller\\PageController::ListPage'], null, null, null, false, false, null]],
        '/cms/faq/list' => [[['_route' => 'cms_faq_list', '_controller' => 'App\\Controller\\PageController::ListFaq'], null, null, null, false, false, null]],
        '/cms/faq/add' => [[['_route' => 'cms_add_faq', '_controller' => 'App\\Controller\\PageController::AddFaq'], null, null, null, false, false, null]],
        '/cms/categories/list' => [[['_route' => 'cms_list_categories', '_controller' => 'App\\Controller\\CategoriesController::ListCat'], null, null, null, false, false, null]],
        '/cms/categories/add' => [[['_route' => 'cms_add_categories', '_controller' => 'App\\Controller\\CategoriesController::AddCat'], null, null, null, false, false, null]],
        '/cms/articles/List' => [[['_route' => 'cms_list_articles', '_controller' => 'App\\Controller\\ArticlesController::ListArticle'], null, null, null, false, false, null]],
        '/cms/articles/showlist_article' => [[['_route' => 'cms_show_list_articles', '_controller' => 'App\\Controller\\ArticlesController::ShowListArticle'], null, null, null, false, false, null]],
        '/cms/articles/showlist_article2' => [[['_route' => 'cms_show_list_articles2', '_controller' => 'App\\Controller\\ArticlesController::ShowListArticles'], null, null, null, false, false, null]],
        '/cms/tags/list' => [[['_route' => 'cms_list_tags', '_controller' => 'App\\Controller\\ArticlesController::ListTags'], null, null, null, false, false, null]],
        '/cms/tags/add' => [[['_route' => 'cms_add_tags', '_controller' => 'App\\Controller\\ArticlesController::AddTags'], null, null, null, false, false, null]],
        '/admin/list/themes' => [[['_route' => 'cms_list_themes', '_controller' => 'App\\Controller\\DefaultController::ListThemes'], null, null, null, false, false, null]],
        '/admin/add/themes' => [[['_route' => 'cms_add_themes', '_controller' => 'App\\Controller\\DefaultController::AddThemes'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/c(?'
                    .'|ms/(?'
                        .'|c(?'
                            .'|o(?'
                                .'|ntactblock/(?'
                                    .'|new/contactnew/([^/]++)/new/([^/]++)(*:76)'
                                    .'|([^/]++)/(?'
                                        .'|updatecontact/([^/]++)/update/([^/]++)(*:133)'
                                        .'|deletecontact/([^/]++)/delete/([^/]++)/\\#(*:182)'
                                    .')'
                                .')'
                                .'|mments/(?'
                                    .'|([^/]++)/add(*:214)'
                                    .'|article/([^/]++)/([^/]++)/(?'
                                        .'|delete(*:257)'
                                        .'|update(?'
                                            .'|(*:274)'
                                            .'|_article(*:290)'
                                        .')'
                                    .')'
                                .')'
                            .')'
                            .'|ategories/([^/]++)/(?'
                                .'|update(*:330)'
                                .'|delete(*:344)'
                                .'|show_informations(*:369)'
                            .')'
                        .')'
                        .'|f(?'
                            .'|eature/(?'
                                .'|new/featureblock/([^/]++)/new/([^/]++)(*:431)'
                                .'|([^/]++)/(?'
                                    .'|update/featureblock/([^/]++)/update/([^/]++)(*:495)'
                                    .'|delete/featureblock/([^/]++)/delete/([^/]++)(*:547)'
                                .')'
                            .')'
                            .'|ooter/([^/]++)/(?'
                                .'|update(*:581)'
                                .'|delete(*:595)'
                            .')'
                            .'|aq/([^/]++)/(?'
                                .'|upadte(*:625)'
                                .'|delete(*:639)'
                                .'|show_list(*:656)'
                                .'|pub(*:667)'
                            .')'
                        .')'
                        .'|galleryy/(?'
                            .'|new/([^/]++)/site/([^/]++)(*:715)'
                            .'|([^/]++)/(?'
                                .'|update/([^/]++)/gallery/([^/]++)(*:767)'
                                .'|delete/([^/]++)/([^/]++)(*:799)'
                            .')'
                            .'|supprime/image/([^/]++)(*:831)'
                        .')'
                        .'|header/(?'
                            .'|new/([^/]++)/([^/]++)(*:871)'
                            .'|([^/]++)/(?'
                                .'|update/([^/]++)/([^/]++)/updateheader(*:928)'
                                .'|delete/([^/]++)/([^/]++)(*:960)'
                            .')'
                        .')'
                        .'|i(?'
                            .'|magevideo/(?'
                                .'|new/([^/]++)/blockvideo/([^/]++)(*:1019)'
                                .'|([^/]++)/(?'
                                    .'|updateimagevideo/([^/]++)/update/([^/]++)(*:1081)'
                                    .'|deleteimagevideo/([^/]++)/delete/([^/]++)(*:1131)'
                                .')'
                            .')'
                            .'|nvit(?'
                                .'|ation_user/([^/]++)/(?'
                                    .'|update(*:1178)'
                                    .'|delete(*:1193)'
                                .')'
                                .'|post/([^/]++)/view/([^/]++)(*:1230)'
                            .')'
                            .'|tem/([^/]++)/(?'
                                .'|show_subitem(*:1268)'
                                .'|add_subitem(*:1288)'
                                .'|([^/]++)/(?'
                                    .'|update_subitem(*:1323)'
                                    .'|delete_subitem(*:1346)'
                                .')'
                            .')'
                        .')'
                        .'|menu/([^/]++)(?'
                            .'|(*:1374)'
                            .'|/(?'
                                .'|edit(*:1391)'
                                .'|delete(*:1406)'
                                .'|check_active(*:1427)'
                                .'|show_item(*:1445)'
                                .'|add_item(*:1462)'
                                .'|([^/]++)/delete_item(*:1491)'
                                .'|item/([^/]++)/edit_item(*:1523)'
                            .')'
                        .')'
                        .'|p(?'
                            .'|age(?'
                                .'|service/(?'
                                    .'|new/serviceinvit/([^/]++)/newservice/([^/]++)(*:1600)'
                                    .'|([^/]++)/(?'
                                        .'|update/([^/]++)/updateservice_block/([^/]++)/up(*:1668)'
                                        .'|delete/([^/]++)/deleteinvit_blockservice/([^/]++)/delete(*:1733)'
                                    .')'
                                .')'
                                .'|/([^/]++)/(?'
                                    .'|add(*:1760)'
                                    .'|update(*:1775)'
                                    .'|show(*:1788)'
                                    .'|delete(*:1803)'
                                    .'|pub(*:1815)'
                                .')'
                            .')'
                            .'|ricing/(?'
                                .'|new/([^/]++)/pricing/([^/]++)(*:1865)'
                                .'|([^/]++)/(?'
                                    .'|update/([^/]++)/pricingupdate/([^/]++)(*:1924)'
                                    .'|delete/([^/]++)/deletepricing/([^/]++)(*:1971)'
                                .')'
                            .')'
                        .')'
                        .'|s(?'
                            .'|ervices/([^/]++)/(?'
                                .'|update(*:2013)'
                                .'|delete(*:2028)'
                            .')'
                            .'|ite/(?'
                                .'|([^/]++)/update(*:2060)'
                                .'|delete/([^/]++)(*:2084)'
                                .'|update/([^/]++)(*:2108)'
                                .'|service/([^/]++)/(?'
                                    .'|([^/]++)/delete(*:2152)'
                                    .'|view(*:2165)'
                                .')'
                                .'|([^/]++)/pub(*:2187)'
                            .')'
                        .')'
                        .'|t(?'
                            .'|eamMembre/(?'
                                .'|new/eamblock/([^/]++)/new/([^/]++)(*:2249)'
                                .'|([^/]++)/(?'
                                    .'|update/teaminvit/([^/]++)/update/([^/]++)(*:2311)'
                                    .'|delete/teaminvitblock/([^/]++)/delete/([^/]++)(*:2366)'
                                .')'
                            .')'
                            .'|ags/([^/]++)/(?'
                                .'|delete(*:2399)'
                                .'|update(*:2414)'
                            .')'
                            .'|heme/([^/]++)/(?'
                                .'|add_to_site(*:2452)'
                                .'|([^/]++)/apply_theme(*:2481)'
                            .')'
                        .')'
                        .'|user/editprofil/([^/]++)(*:2516)'
                        .'|articles/([^/]++)/(?'
                            .'|add(*:2549)'
                            .'|update(*:2564)'
                            .'|delete(*:2579)'
                            .'|show(*:2592)'
                        .')'
                        .'|demo/([^/]++)/(?'
                            .'|t(?'
                                .'|hemes(*:2628)'
                                .'|eam_themes(*:2647)'
                            .')'
                            .'|about_themes(*:2669)'
                            .'|s(?'
                                .'|ervice_themes(*:2695)'
                                .'|hop_themes(*:2714)'
                            .')'
                            .'|blog_themes(*:2735)'
                            .'|contact_themes(*:2758)'
                        .')'
                        .'|view(?'
                            .'|i(?'
                                .'|nvit(?'
                                    .'|/([^/]++)/([^/]++)/view(*:2809)'
                                    .'|page/([^/]++)/([^/]++)/pageview(*:2849)'
                                .')'
                                .'|magevideo/invitblock/([^/]++)/([^/]++)/view(*:2902)'
                            .')'
                            .'|header/invitblock/([^/]++)/([^/]++)/view(*:2952)'
                            .'|gallery/invitblock/([^/]++)/([^/]++)/view(*:3002)'
                            .'|pricing/invitblock/([^/]++)/([^/]++)/view(*:3052)'
                            .'|feature/invitblock/([^/]++)/([^/]++)/view(*:3102)'
                            .'|team/invitblock/([^/]++)/([^/]++)/view(*:3149)'
                            .'|services/invitblock/([^/]++)/([^/]++)/view(*:3200)'
                            .'|contact/invitblock/([^/]++)/([^/]++)/view(*:3250)'
                        .')'
                    .')'
                    .'|onnect/(?'
                        .'|([^/]++)(*:3279)'
                        .'|service/([^/]++)(*:3304)'
                        .'|registration/([^/]++)(*:3334)'
                    .')'
                .')'
                .'|/profile/edit/([^/]++)(*:3367)'
                .'|/re(?'
                    .'|gister/confirm/([^/]++)(*:3405)'
                    .'|setting/reset/([^/]++)(*:3436)'
                .')'
                .'|/front/(?'
                    .'|([^/]++)(?'
                        .'|(*:3467)'
                        .'|/(?'
                            .'|([^/]++)/view_page(*:3498)'
                            .'|faqs_page(*:3516)'
                            .'|blog_page(*:3534)'
                        .')'
                    .')'
                    .'|mail/([^/]++)/([^/]++)/send_mail(*:3577)'
                    .'|post/([^/]++)/([^/]++)/(?'
                        .'|read_more(*:3621)'
                        .'|add_comment_post(*:3646)'
                        .'|update_comment_post/([^/]++)/update(*:3690)'
                        .'|delete_comment_post/([^/]++)/delete(*:3734)'
                        .'|form_update_comment_post/([^/]++)/update(*:3783)'
                    .')'
                    .'|([^/]++)/serach(*:3808)'
                .')'
                .'|/admin/(?'
                    .'|update/([^/]++)/themes(*:3850)'
                    .'|delete/([^/]++)/themes(*:3881)'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        76 => [[['_route' => 'cms_contactblock_new', '_controller' => 'App\\Controller\\ContactController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        133 => [[['_route' => 'cms_contactblock_update', '_controller' => 'App\\Controller\\ContactController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        182 => [[['_route' => 'cms_contactblock_delete', '_controller' => 'App\\Controller\\ContactController::delete'], ['id', 'idpage', 'idsite'], null, null, false, false, null]],
        214 => [[['_route' => 'cms_add_comments', '_controller' => 'App\\Controller\\ArticlesController::AddComment'], ['id'], null, null, false, false, null]],
        257 => [[['_route' => 'cms_delete_comments', '_controller' => 'App\\Controller\\ArticlesController::DeleteComment'], ['idt', 'id'], null, null, false, false, null]],
        274 => [[['_route' => 'cms_update_comments', '_controller' => 'App\\Controller\\ArticlesController::UpdateComment'], ['idt', 'id'], null, null, false, false, null]],
        290 => [[['_route' => 'cms_update_comment_article', '_controller' => 'App\\Controller\\ArticlesController::UpdateArticleComment'], ['id', 'idd'], null, null, false, false, null]],
        330 => [[['_route' => 'cms_update_categories', '_controller' => 'App\\Controller\\CategoriesController::UpdateCat'], ['id'], null, null, false, false, null]],
        344 => [[['_route' => 'cms_delete_categories', '_controller' => 'App\\Controller\\CategoriesController::DeleteCat'], ['id'], null, null, false, false, null]],
        369 => [[['_route' => 'cms_show_categories', '_controller' => 'App\\Controller\\CategoriesController::ShowCat'], ['id'], null, null, false, false, null]],
        431 => [[['_route' => 'cms_feature_new', '_controller' => 'App\\Controller\\FeatureController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        495 => [[['_route' => 'cms_feature_update', '_controller' => 'App\\Controller\\FeatureController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        547 => [[['_route' => 'cms_feature_delete', '_controller' => 'App\\Controller\\FeatureController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        581 => [[['_route' => 'cms_footer_update', '_controller' => 'App\\Controller\\FooterController::update'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        595 => [[['_route' => 'cms_footer_delete', '_controller' => 'App\\Controller\\FooterController::delete'], ['id'], null, null, false, false, null]],
        625 => [[['_route' => 'cms_update_faq', '_controller' => 'App\\Controller\\PageController::UpdateFaq'], ['id'], null, null, false, false, null]],
        639 => [[['_route' => 'cms_delete_faq', '_controller' => 'App\\Controller\\PageController::DeleteFaq'], ['id'], null, null, false, false, null]],
        656 => [[['_route' => 'cms_showpage_faq', '_controller' => 'App\\Controller\\PageController::ShowFaq'], ['id'], null, null, false, false, null]],
        667 => [[['_route' => 'pub_faq', '_controller' => 'App\\Controller\\PageController::PubFaq'], ['id'], null, null, false, false, null]],
        715 => [[['_route' => 'cms_gallery_new', '_controller' => 'App\\Controller\\GalleryController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        767 => [[['_route' => 'cms_gallery_update', '_controller' => 'App\\Controller\\GalleryController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        799 => [[['_route' => 'cms_gallery_delete', '_controller' => 'App\\Controller\\GalleryController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        831 => [[['_route' => 'cms_delete_image', '_controller' => 'App\\Controller\\GalleryController::deleteImage'], ['id'], ['DELETE' => 0], null, false, true, null]],
        871 => [[['_route' => 'cms_header_new', '_controller' => 'App\\Controller\\HeaderController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        928 => [[['_route' => 'cms_header_update', '_controller' => 'App\\Controller\\HeaderController::update'], ['id', 'idsite', 'idpage'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        960 => [[['_route' => 'cms_header_delete', '_controller' => 'App\\Controller\\HeaderController::delete'], ['id', 'idsite', 'idpage'], null, null, false, true, null]],
        1019 => [[['_route' => 'cms_imagevideo_new', '_controller' => 'App\\Controller\\ImageVideoController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1081 => [[['_route' => 'cms_imagevideo_update', '_controller' => 'App\\Controller\\ImageVideoController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1131 => [[['_route' => 'cms_imagevideo_delete', '_controller' => 'App\\Controller\\ImageVideoController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        1178 => [[['_route' => 'cms_UserInvit_update', '_controller' => 'App\\Controller\\UserInvitController::update'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1193 => [[['_route' => 'cms_UserInvit_delete', '_controller' => 'App\\Controller\\UserInvitController::delete'], ['id'], null, null, false, false, null]],
        1230 => [[['_route' => 'cms_post_invit_view', '_controller' => 'App\\Controller\\DefaultController::ViewInvitPost'], ['name', 'idsite'], null, null, false, true, null]],
        1268 => [[['_route' => 'cms_menu_show_subitems', '_controller' => 'App\\Controller\\MenuController::ShowSubItem'], ['id'], null, null, false, false, null]],
        1288 => [[['_route' => 'cms_menu_add_subitem', '_controller' => 'App\\Controller\\MenuController::AddSubItem'], ['id'], null, null, false, false, null]],
        1323 => [[['_route' => 'cms_menu_update_subitem', '_controller' => 'App\\Controller\\MenuController::UpdateSubItem'], ['id', 'idi'], null, null, false, false, null]],
        1346 => [[['_route' => 'cms_menu_delete_subitem', '_controller' => 'App\\Controller\\MenuController::DeleteSubItem'], ['id', 'idi'], null, null, false, false, null]],
        1374 => [[['_route' => 'cms_menu_show', '_controller' => 'App\\Controller\\MenuController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        1391 => [[['_route' => 'cms_menu_edit', '_controller' => 'App\\Controller\\MenuController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1406 => [[['_route' => 'cms_menu_delete', '_controller' => 'App\\Controller\\MenuController::delete'], ['id'], null, null, false, false, null]],
        1427 => [[['_route' => 'cms_menu_active', '_controller' => 'App\\Controller\\MenuController::CheckMenu'], ['id'], null, null, false, false, null]],
        1445 => [[['_route' => 'cms_menu_show_item', '_controller' => 'App\\Controller\\MenuController::ShowItem'], ['id'], null, null, false, false, null]],
        1462 => [[['_route' => 'cms_menu_add_item', '_controller' => 'App\\Controller\\MenuController::AddItem'], ['id'], null, null, false, false, null]],
        1491 => [[['_route' => 'cms_menu_delete_item', '_controller' => 'App\\Controller\\MenuController::DeleteItem'], ['id', 'idm'], null, null, false, false, null]],
        1523 => [[['_route' => 'cms_menu_edit_item', '_controller' => 'App\\Controller\\MenuController::EditItem'], ['id', 'idi'], null, null, false, false, null]],
        1600 => [[['_route' => 'cms_pageservice_new', '_controller' => 'App\\Controller\\PageServiceController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1668 => [[['_route' => 'cms_pageservice_update', '_controller' => 'App\\Controller\\PageServiceController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1733 => [[['_route' => 'cms_pageservice_delete', '_controller' => 'App\\Controller\\PageServiceController::delete'], ['id', 'idpage', 'idsite'], null, null, false, false, null]],
        1760 => [[['_route' => 'cms_add_page', '_controller' => 'App\\Controller\\PageController::AddPage'], ['idsite'], null, null, false, false, null]],
        1775 => [[['_route' => 'cms_update_page', '_controller' => 'App\\Controller\\PageController::UpdatePage'], ['id'], null, null, false, false, null]],
        1788 => [[['_route' => 'cms_show_page', '_controller' => 'App\\Controller\\PageController::ShowPage'], ['id'], null, null, false, false, null]],
        1803 => [[['_route' => 'cms_delete_page', '_controller' => 'App\\Controller\\PageController::DeletePage'], ['id'], null, null, false, false, null]],
        1815 => [[['_route' => 'pub_page', '_controller' => 'App\\Controller\\PageController::PubPage'], ['id'], null, null, false, false, null]],
        1865 => [[['_route' => 'cms_pricing_new', '_controller' => 'App\\Controller\\PricingController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1924 => [[['_route' => 'cms_pricing_update', '_controller' => 'App\\Controller\\PricingController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1971 => [[['_route' => 'cms_pricing_delete', '_controller' => 'App\\Controller\\PricingController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        2013 => [[['_route' => 'cms_service_update', '_controller' => 'App\\Controller\\ServicesController::update'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        2028 => [[['_route' => 'cms_service_delete', '_controller' => 'App\\Controller\\ServicesController::delete'], ['id'], null, null, false, false, null]],
        2060 => [[['_route' => 'cms_update_site', '_controller' => 'App\\Controller\\DefaultController::ModifySite'], ['id'], null, null, false, false, null]],
        2084 => [[['_route' => 'cms_site_delete', '_controller' => 'App\\Controller\\DefaultController::DeleteSite'], ['id'], null, null, false, true, null]],
        2108 => [[['_route' => 'cms_site_update', '_controller' => 'App\\Controller\\DefaultController::UpdateSite'], ['id'], null, null, false, true, null]],
        2152 => [[['_route' => 'cms_site_delete_services', '_controller' => 'App\\Controller\\DefaultController::DeleteServices'], ['id', 'ids'], null, null, false, false, null]],
        2165 => [[['_route' => 'cms_site_view_services', '_controller' => 'App\\Controller\\DefaultController::ViewServices'], ['id'], null, null, false, false, null]],
        2187 => [[['_route' => 'pub_site', '_controller' => 'App\\Controller\\DefaultController::PubSite'], ['id'], null, null, false, false, null]],
        2249 => [[['_route' => 'cms_team_membre_new', '_controller' => 'App\\Controller\\TeamMembreController::new'], ['idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2311 => [[['_route' => 'cms_team_membre_update', '_controller' => 'App\\Controller\\TeamMembreController::update'], ['id', 'idpage', 'idsite'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        2366 => [[['_route' => 'cms_team_membre_delete', '_controller' => 'App\\Controller\\TeamMembreController::delete'], ['id', 'idpage', 'idsite'], null, null, false, true, null]],
        2399 => [[['_route' => 'cms_delete_tags', '_controller' => 'App\\Controller\\ArticlesController::DeleteTags'], ['id'], null, null, false, false, null]],
        2414 => [[['_route' => 'cms_update_tags', '_controller' => 'App\\Controller\\ArticlesController::UpdateTags'], ['id'], null, null, false, false, null]],
        2452 => [[['_route' => 'cms_add_theme_site', '_controller' => 'App\\Controller\\DefaultController::AddSiteTheme'], ['id'], null, null, false, false, null]],
        2481 => [[['_route' => 'cms_apply_themes', '_controller' => 'App\\Controller\\DefaultController::ApplyTheme'], ['id', 'idt'], null, null, false, false, null]],
        2516 => [[['_route' => 'cms_user_profil_edit', '_controller' => 'App\\Controller\\DefaultController::Editprofil'], ['id'], null, null, false, true, null]],
        2549 => [[['_route' => 'cms_add_articles', '_controller' => 'App\\Controller\\ArticlesController::AddArticle'], ['name'], null, null, false, false, null]],
        2564 => [[['_route' => 'cms_update_articles', '_controller' => 'App\\Controller\\ArticlesController::UpdateArticle'], ['id'], null, null, false, false, null]],
        2579 => [[['_route' => 'cms_delete_articles', '_controller' => 'App\\Controller\\ArticlesController::DeleteArticle'], ['id'], null, null, false, false, null]],
        2592 => [[['_route' => 'cms_show_articles', '_controller' => 'App\\Controller\\ArticlesController::ShowArticle'], ['id'], null, null, false, false, null]],
        2628 => [[['_route' => 'cms_demo_themes', '_controller' => 'App\\Controller\\DefaultController::DemoThemes'], ['id'], null, null, false, false, null]],
        2647 => [[['_route' => 'cms_demo_team_themes', '_controller' => 'App\\Controller\\DefaultController::TeamThemes'], ['id'], null, null, false, false, null]],
        2669 => [[['_route' => 'cms_demo_about_themes', '_controller' => 'App\\Controller\\DefaultController::AboutThemes'], ['id'], null, null, false, false, null]],
        2695 => [[['_route' => 'cms_demo_service_themes', '_controller' => 'App\\Controller\\DefaultController::ServiceThemes'], ['id'], null, null, false, false, null]],
        2714 => [[['_route' => 'cms_demo_shop_themes', '_controller' => 'App\\Controller\\DefaultController::ShopThemes'], ['id'], null, null, false, false, null]],
        2735 => [[['_route' => 'cms_demo_blog_themes', '_controller' => 'App\\Controller\\DefaultController::BlogThemes'], ['id'], null, null, false, false, null]],
        2758 => [[['_route' => 'cms_demo_contact_themes', '_controller' => 'App\\Controller\\DefaultController::ContactThemes'], ['id'], null, null, false, false, null]],
        2809 => [[['_route' => 'cms_page_invit_view', '_controller' => 'App\\Controller\\DefaultController::ViewInvitPage'], ['idsite', 'name'], null, null, false, false, null]],
        2849 => [[['_route' => 'cms_view_block_page', '_controller' => 'App\\Controller\\DefaultController::ViewBlockPage'], ['id', 'idsite'], null, null, false, false, null]],
        2902 => [[['_route' => 'cms_invit_view_imagevideo', '_controller' => 'App\\Controller\\ImageVideoController::ViewImageVideoInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        2952 => [[['_route' => 'cms_invit_view_header', '_controller' => 'App\\Controller\\HeaderController::ViewHeaderInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3002 => [[['_route' => 'cms_invit_view_gallery', '_controller' => 'App\\Controller\\GalleryController::ViewGalleryInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3052 => [[['_route' => 'cms_invit_view_pricing', '_controller' => 'App\\Controller\\PricingController::ViewPricingInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3102 => [[['_route' => 'cms_invit_view_feature', '_controller' => 'App\\Controller\\FeatureController::ViewFeatureInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3149 => [[['_route' => 'cms_invit_view_team', '_controller' => 'App\\Controller\\TeamMembreController::ViewTeamInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3200 => [[['_route' => 'cms_invit_view_service', '_controller' => 'App\\Controller\\PageServiceController::ViewServiceInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3250 => [[['_route' => 'cms_invit_view_contact', '_controller' => 'App\\Controller\\ContactController::ViewContactInvitPage'], ['id', 'idsite'], null, null, false, false, null]],
        3279 => [[['_route' => 'hwi_oauth_service_redirect', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\RedirectToServiceController::redirectToServiceAction'], ['service'], null, null, false, true, null]],
        3304 => [[['_route' => 'hwi_oauth_connect_service', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::connectServiceAction'], ['service'], null, null, false, true, null]],
        3334 => [[['_route' => 'hwi_oauth_connect_registration', '_controller' => 'HWI\\Bundle\\OAuthBundle\\Controller\\ConnectController::registrationAction'], ['key'], null, null, false, true, null]],
        3367 => [[['_route' => 'fos_user_profile_edit', '_controller' => 'fos_user.profile.controller:editAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3405 => [[['_route' => 'fos_user_registration_confirm', '_controller' => 'fos_user.registration.controller:confirmAction'], ['token'], ['GET' => 0], null, false, true, null]],
        3436 => [[['_route' => 'fos_user_resetting_reset', '_controller' => 'fos_user.resetting.controller:resetAction'], ['token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        3467 => [[['_route' => 'cms_site_view', '_controller' => 'App\\Controller\\DefaultController::ViewSite'], ['name'], null, null, false, true, null]],
        3498 => [[['_route' => 'cms_view_page', '_controller' => 'App\\Controller\\DefaultController::ViewPage'], ['id', 'idsite'], null, null, false, false, null]],
        3516 => [[['_route' => 'list_faq_front', '_controller' => 'App\\Controller\\DefaultController::FaqPage'], ['id'], null, null, false, false, null]],
        3534 => [[['_route' => 'list_blog_front', '_controller' => 'App\\Controller\\DefaultController::BlogPage'], ['id'], null, null, false, false, null]],
        3577 => [[['_route' => 'send_mail', '_controller' => 'App\\Controller\\DefaultController::SendMail'], ['id', 'ids'], null, null, false, false, null]],
        3621 => [[['_route' => 'cms_article_read_more', '_controller' => 'App\\Controller\\ArticlesController::ReadMore'], ['id', 'ids'], null, null, false, false, null]],
        3646 => [[['_route' => 'cms_add_comment_post', '_controller' => 'App\\Controller\\ArticlesController::AddCommentPost'], ['id', 'ids'], null, null, false, false, null]],
        3690 => [[['_route' => 'cms_update_comment_post', '_controller' => 'App\\Controller\\ArticlesController::UpdateCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        3734 => [[['_route' => 'cms_delete_comment_post', '_controller' => 'App\\Controller\\ArticlesController::DeleteCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        3783 => [[['_route' => 'cms_update_comment_form', '_controller' => 'App\\Controller\\ArticlesController::UpdateFormCommentPost'], ['id', 'idt', 'ids'], null, null, false, false, null]],
        3808 => [[['_route' => 'cms_search_page', '_controller' => 'App\\Controller\\DefaultController::Search'], ['id'], null, null, false, false, null]],
        3850 => [[['_route' => 'cms_update_themes', '_controller' => 'App\\Controller\\DefaultController::UpdateThemes'], ['id'], null, null, false, false, null]],
        3881 => [
            [['_route' => 'cms_delete_themes', '_controller' => 'App\\Controller\\DefaultController::DeleteThemes'], ['id'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
