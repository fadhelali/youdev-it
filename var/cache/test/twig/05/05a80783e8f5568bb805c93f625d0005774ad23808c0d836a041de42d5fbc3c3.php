<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_2df06c4b2a8bb9a79f67632b0cebff265ba1adbcee16a4ab94d6f5f2659af2ea extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 3, $this->source); })())) {
            // line 4
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 4, $this->source); })()), "messageKey", [], "any", false, false, false, 4), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 4, $this->source); })()), "messageData", [], "any", false, false, false, 4), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo "<style>
.login_form_wrapper {
    float: left;
    width: 100%;
    padding-top: 40px;
    padding-bottom: 100px;
    background-color: grey
}

.login_wrapper {
    padding-bottom: 20px;
    margin-bottom: 20px;
    border-bottom: 1px solid #e4e4e4;
    float: left;
    width: 100%;
    background: #fff;
    padding: 50px
}

.login_wrapper a.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: left;
    border-radius: 5px;
    background: #4385f5;
    font-size: 16px;
    border: 1px solid #4385f5
}

.login_wrapper a i {
    float: right;
    margin: 0;
    line-height: 35px
}

.login_wrapper a.google-plus {
    background: #db4c3e;
    border: 1px solid #db4c3e
}

.login_wrapper h2 {
    font-size: 18px;
    font-weight: 500;
    margin-bottom: 20px;
    color: #111;
    line-height: 20px;
    text-transform: uppercase;
    text-align: center;
    position: relative
}

.login_wrapper .formsix-pos,
.formsix-e {
    position: relative
}

.form-group {
    margin-bottom: 15px
}

.login_wrapper .form-control {
    height: 53px;
    padding: 15px 20px;
    font-size: 14px;
    line-height: 24px;
    border: 1px solid #fafafa;
    border-radius: 3px;
    box-shadow: none;
    font-family: 'Roboto';
    -webkit-transition: all 0.3s ease 0s;
    -moz-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    background-color: #fafafa
}

.login_wrapper .form-control:focus {
    color: #999;
    background-color: fafafa;
    border: 1px solid #4285f4 !important
}

.login_remember_box {
    margin-top: 30px;
    margin-bottom: 30px;
    color: #999
}

.login_remember_box .control {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    font-size: 12px;
    font-weight: 500;
    margin: 0
}

.login_remember_box .control input {
    position: absolute;
    z-index: -1;
    opacity: 0
}

.login_remember_box .control__indicator {
    position: absolute;
    top: 0;
    left: 0;
    width: 15px;
    height: 15px;
    background: #fff;
    border: 1px solid #999
}

.login_remember_box .forget_password {
    float: right;
    color: #db4c3e;
    line-height: 12px;
    text-decoration: underline
}

.login_btn_wrapper {
    padding-bottom: 20px;
    margin-bottom: 30px;
    border-bottom: 1px solid #e4e4e4
}

.login_btn_wrapper a.login_btn {
    text-align: center;
    text-transform: uppercase
}

.login_message p {
    text-align: center;
    font-size: 16px;
    margin: 0
}

p {
    color: #999999;
    font-size: 14px;
    line-height: 24px
}

.login_wrapper a.login_btn:hover {
    background-color: #2c6ad4;
    border-color: #2c6ad4
}

.login_wrapper a.btn:hover {
    background-color: #2c6ad4;
    border-color: #2c6ad4
}
</style>
 <div class=\"wrapper wrapper-login\">
<div class=\"container container-login animated fadeIn\">
\t\t
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script> 
<style>
\t.login-form {
\t\twidth: 385px;
\t\tmargin: 30px auto;
\t}
    .login-form form {        
    \tmargin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .login-btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .input-group-addon .fa {
        font-size: 18px;
    }
    .login-btn {
        font-size: 15px;
        font-weight: bold;
    }
\t.social-btn .btn {
\t\tborder: none;
        margin: 10px 3px 0;
        opacity: 1;
\t}
    .social-btn .btn:hover {
        opacity: 0.9;
    }
\t.social-btn .btn-primary {
        background: #507cc0;
    }
\t.social-btn .btn-info {
\t\tbackground: #64ccf1;
\t}
\t.social-btn .btn-danger {
\t\tbackground: #df4930;
\t}
    .or-seperator {
        margin-top: 20px;
        text-align: center;
        border-top: 1px solid #ccc;
    }
    .or-seperator i {
        padding: 0 10px;
        background: #f7f7f7;
        position: relative;
        top: -11px;
        z-index: 1;
    }   
</style>

<form action=\"";
        // line 226
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    ";
        // line 227
        if ((isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new RuntimeError('Variable "csrf_token" does not exist.', 227, $this->source); })())) {
            // line 228
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new RuntimeError('Variable "csrf_token" does not exist.', 228, $this->source); })()), "html", null, true);
            echo "\" />
    ";
        }
        // line 230
        echo "<div class=\"form-group\">
\t\t\t\t\t<label for=\"username\" class=\"placeholder\"><b>Username or Email</b></label>
\t\t\t\t\t<input id=\"username\" name=\"_username\" type=\"text\" autocomplete=\"username\" value=\"";
        // line 232
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 232, $this->source); })()), "html", null, true);
        echo "\" class=\"form-control\" required>
\t\t\t\t</div>
   <div class=\"form-group\">
\t\t\t\t\t<label for=\"password\" class=\"placeholder\"><b>";
        // line 235
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.password", [], "FOSUserBundle"), "html", null, true);
        echo "</b></label>
\t\t\t\t\t<a href=\"";
        // line 236
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_resetting_request");
        echo "\" class=\"link float-right\">Forget Password ?</a>
\t\t\t\t\t<div class=\"position-relative\">
\t\t\t\t\t\t<input id=\"password\" name=\"_password\" type=\"password\" class=\"form-control\" autocomplete=\"current-password\" required>
\t\t\t\t\t\t<div class=\"show-password\">
\t\t\t\t\t\t\t<i class=\"flaticon-interface\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

   <div class=\"form-group form-action-d-flex mb-3\">
\t\t\t\t\t<div class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t<input type=\"checkbox\" name=\"_remember_me\" value=\"on\" class=\"custom-control-input\" id=\"remember_me\">
\t\t\t\t\t\t<label class=\"custom-control-label m-0\" for=\"remember_me\">";
        // line 248
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.remember_me", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
\t\t\t\t\t</div>
    <input type=\"submit\" id=\"_submit\" class=\"btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold\" name=\"_submit\" value=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login.submit", [], "FOSUserBundle"), "html", null, true);
        echo "\" />
\t\t\t\t</div>
<div class=\"login-account\">
\t\t\t\t\t<span class=\"msg\">Don't have an account yet ?</span>
\t\t\t\t\t<a href=\"";
        // line 254
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_registration_register");
        echo "\" id=\"show-signup\" class=\"link\">Sign Up</a>
\t\t\t\t</div>
  <br>
<div id=\"background\">
<div class=\"login-account\">
<div class=\"or-seperator\"><i>or</i></div>
        <p class=\"text-center\">Login with your social media account</p>
 <div class=\"text-center social-btn\">
            <a href=\"";
        // line 262
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "facebook"]);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-facebook\"></i>&nbsp; Facebook</a>
            <a href=\"";
        // line 263
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "linkedin"]);
        echo "\" class=\"btn btn-info\"><i class=\"fa fa-linkedin\"></i>&nbsp; Linkedin</a>
\t\t\t<a href=\"";
        // line 264
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "google"]);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-google\"></i>&nbsp; Google</a>
        </div>
            
        </form>

        <div id=\"fb-root\"></div>

        <script
        
  src=\"https://code.jquery.com/jquery-3.2.1.min.js\"
  integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"
  crossorigin=\"anonymous\"></script>
        <script>
        window.fbAsyncInit = function() {
    FB.init({
      appId      : '236392778193295',
      cookie     : true,
      xfbml      : true,
      version    : 'v10.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = \"https://connect.facebook.net/en_US/sdk.js\";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   

            function fb_login() {
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        // connected
                        document.location = \"";
        // line 302
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hwi_oauth_service_redirect", ["service" => "facebook"]);
        echo "\";
                    } else {
                        // not_authorized
                        FB.login(function(response) {
                            if (response.authResponse) {
                                document.location = \"";
        // line 307
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hwi_oauth_service_redirect", ["service" => "facebook"]);
        echo "\";
                            }
                        }, {scope:'email'});
                    }
                });
            }
        </script>
    </div>
</form>
\t</div>
    </div>
    \t</div>
           
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  397 => 307,  389 => 302,  348 => 264,  344 => 263,  340 => 262,  329 => 254,  322 => 250,  317 => 248,  302 => 236,  298 => 235,  292 => 232,  288 => 230,  282 => 228,  280 => 227,  276 => 226,  54 => 6,  48 => 4,  46 => 3,  43 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain 'FOSUserBundle' %}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
<style>
.login_form_wrapper {
    float: left;
    width: 100%;
    padding-top: 40px;
    padding-bottom: 100px;
    background-color: grey
}

.login_wrapper {
    padding-bottom: 20px;
    margin-bottom: 20px;
    border-bottom: 1px solid #e4e4e4;
    float: left;
    width: 100%;
    background: #fff;
    padding: 50px
}

.login_wrapper a.btn {
    color: #fff;
    width: 100%;
    height: 50px;
    padding: 6px 25px;
    line-height: 36px;
    margin-bottom: 20px;
    text-align: left;
    border-radius: 5px;
    background: #4385f5;
    font-size: 16px;
    border: 1px solid #4385f5
}

.login_wrapper a i {
    float: right;
    margin: 0;
    line-height: 35px
}

.login_wrapper a.google-plus {
    background: #db4c3e;
    border: 1px solid #db4c3e
}

.login_wrapper h2 {
    font-size: 18px;
    font-weight: 500;
    margin-bottom: 20px;
    color: #111;
    line-height: 20px;
    text-transform: uppercase;
    text-align: center;
    position: relative
}

.login_wrapper .formsix-pos,
.formsix-e {
    position: relative
}

.form-group {
    margin-bottom: 15px
}

.login_wrapper .form-control {
    height: 53px;
    padding: 15px 20px;
    font-size: 14px;
    line-height: 24px;
    border: 1px solid #fafafa;
    border-radius: 3px;
    box-shadow: none;
    font-family: 'Roboto';
    -webkit-transition: all 0.3s ease 0s;
    -moz-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    background-color: #fafafa
}

.login_wrapper .form-control:focus {
    color: #999;
    background-color: fafafa;
    border: 1px solid #4285f4 !important
}

.login_remember_box {
    margin-top: 30px;
    margin-bottom: 30px;
    color: #999
}

.login_remember_box .control {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    font-size: 12px;
    font-weight: 500;
    margin: 0
}

.login_remember_box .control input {
    position: absolute;
    z-index: -1;
    opacity: 0
}

.login_remember_box .control__indicator {
    position: absolute;
    top: 0;
    left: 0;
    width: 15px;
    height: 15px;
    background: #fff;
    border: 1px solid #999
}

.login_remember_box .forget_password {
    float: right;
    color: #db4c3e;
    line-height: 12px;
    text-decoration: underline
}

.login_btn_wrapper {
    padding-bottom: 20px;
    margin-bottom: 30px;
    border-bottom: 1px solid #e4e4e4
}

.login_btn_wrapper a.login_btn {
    text-align: center;
    text-transform: uppercase
}

.login_message p {
    text-align: center;
    font-size: 16px;
    margin: 0
}

p {
    color: #999999;
    font-size: 14px;
    line-height: 24px
}

.login_wrapper a.login_btn:hover {
    background-color: #2c6ad4;
    border-color: #2c6ad4
}

.login_wrapper a.btn:hover {
    background-color: #2c6ad4;
    border-color: #2c6ad4
}
</style>
 <div class=\"wrapper wrapper-login\">
<div class=\"container container-login animated fadeIn\">
\t\t
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script> 
<style>
\t.login-form {
\t\twidth: 385px;
\t\tmargin: 30px auto;
\t}
    .login-form form {        
    \tmargin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .login-btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .input-group-addon .fa {
        font-size: 18px;
    }
    .login-btn {
        font-size: 15px;
        font-weight: bold;
    }
\t.social-btn .btn {
\t\tborder: none;
        margin: 10px 3px 0;
        opacity: 1;
\t}
    .social-btn .btn:hover {
        opacity: 0.9;
    }
\t.social-btn .btn-primary {
        background: #507cc0;
    }
\t.social-btn .btn-info {
\t\tbackground: #64ccf1;
\t}
\t.social-btn .btn-danger {
\t\tbackground: #df4930;
\t}
    .or-seperator {
        margin-top: 20px;
        text-align: center;
        border-top: 1px solid #ccc;
    }
    .or-seperator i {
        padding: 0 10px;
        background: #f7f7f7;
        position: relative;
        top: -11px;
        z-index: 1;
    }   
</style>

<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
    {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    {% endif %}
<div class=\"form-group\">
\t\t\t\t\t<label for=\"username\" class=\"placeholder\"><b>Username or Email</b></label>
\t\t\t\t\t<input id=\"username\" name=\"_username\" type=\"text\" autocomplete=\"username\" value=\"{{ last_username }}\" class=\"form-control\" required>
\t\t\t\t</div>
   <div class=\"form-group\">
\t\t\t\t\t<label for=\"password\" class=\"placeholder\"><b>{{ 'security.login.password'|trans }}</b></label>
\t\t\t\t\t<a href=\"{{ path('fos_user_resetting_request') }}\" class=\"link float-right\">Forget Password ?</a>
\t\t\t\t\t<div class=\"position-relative\">
\t\t\t\t\t\t<input id=\"password\" name=\"_password\" type=\"password\" class=\"form-control\" autocomplete=\"current-password\" required>
\t\t\t\t\t\t<div class=\"show-password\">
\t\t\t\t\t\t\t<i class=\"flaticon-interface\"></i>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

   <div class=\"form-group form-action-d-flex mb-3\">
\t\t\t\t\t<div class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t<input type=\"checkbox\" name=\"_remember_me\" value=\"on\" class=\"custom-control-input\" id=\"remember_me\">
\t\t\t\t\t\t<label class=\"custom-control-label m-0\" for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>
\t\t\t\t\t</div>
    <input type=\"submit\" id=\"_submit\" class=\"btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" />
\t\t\t\t</div>
<div class=\"login-account\">
\t\t\t\t\t<span class=\"msg\">Don't have an account yet ?</span>
\t\t\t\t\t<a href=\"{{ path('fos_user_registration_register') }}\" id=\"show-signup\" class=\"link\">Sign Up</a>
\t\t\t\t</div>
  <br>
<div id=\"background\">
<div class=\"login-account\">
<div class=\"or-seperator\"><i>or</i></div>
        <p class=\"text-center\">Login with your social media account</p>
 <div class=\"text-center social-btn\">
            <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'facebook'}) }}\" class=\"btn btn-primary\"><i class=\"fa fa-facebook\"></i>&nbsp; Facebook</a>
            <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'linkedin'}) }}\" class=\"btn btn-info\"><i class=\"fa fa-linkedin\"></i>&nbsp; Linkedin</a>
\t\t\t<a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'google'}) }}\" class=\"btn btn-danger\"><i class=\"fa fa-google\"></i>&nbsp; Google</a>
        </div>
            
        </form>

        <div id=\"fb-root\"></div>

        <script
        
  src=\"https://code.jquery.com/jquery-3.2.1.min.js\"
  integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"
  crossorigin=\"anonymous\"></script>
        <script>
        window.fbAsyncInit = function() {
    FB.init({
      appId      : '236392778193295',
      cookie     : true,
      xfbml      : true,
      version    : 'v10.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = \"https://connect.facebook.net/en_US/sdk.js\";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   

            function fb_login() {
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        // connected
                        document.location = \"{{ url(\"hwi_oauth_service_redirect\", {service: \"facebook\"}) }}\";
                    } else {
                        // not_authorized
                        FB.login(function(response) {
                            if (response.authResponse) {
                                document.location = \"{{ url(\"hwi_oauth_service_redirect\", {service: \"facebook\"}) }}\";
                            }
                        }, {scope:'email'});
                    }
                });
            }
        </script>
    </div>
</form>
\t</div>
    </div>
    \t</div>
           
", "@FOSUser/Security/login_content.html.twig", "C:\\wamp64\\www\\youdev-it\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login_content.html.twig");
    }
}
