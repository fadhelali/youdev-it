# YooDev-It CMS  

"YooDev-IT CMS is a content management systeme  for making websites and multimedia applications
" ![maintened](https://bitbucket.org/fadhelali/youdev-it/downloads/maintened.PNG) 
------------------------------------------

![YooDev-IT](/public/assets/img/youdev1.png){width=640 height=480}


#  Installation  

* Requirements *

* Symfony 4.4
* Composer 2.0
## Clone of the project  

git clone https://bitbucket.org/fadhelali/youdev-it.git  or composer create-project youdevcms/cms -s dev 
##### make configurations for the variable database_url ,mailer_url in .env file  
next run this lists of this commands  

* composer install  

* php bin/console doctrine:database:create  

* php bin/console doctrine:database:migrate  

* php bin/console doctrine:migration:migrate  

# Useful Commands  

* php bin/console youdevcms:site:create  

* php bin/console youdevcms:page:create


## to run a developement server for the assets run 
yarn run dev or npm run dev (make sure to install one of this package manager)

# Technologies  

![interface5](https://bitbucket.org/fadhelali/youdev-it/downloads/progression.PNG)   


# Advantage of this CMS  
* hight security

* Free CMS

* ergonomic

* based on symfony  

# ScreenShots
![interface2](https://bitbucket.org/fadhelali/youdev-it/downloads/interface_accueil_client.PNG)   

![interface1](https://bitbucket.org/fadhelali/youdev-it/downloads/exemple_inteface_blog_pour_theme4.PNG) 

![interface3](https://bitbucket.org/fadhelali/youdev-it/downloads/interface_ajouteritem_formenu2.PNG)   

![interface4](https://bitbucket.org/fadhelali/youdev-it/downloads/exemple_deblockgallery_depage_creer_avectheme4.PNG) 






 

