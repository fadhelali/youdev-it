<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517194146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE services_site');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE feature CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE image_video CHANGE content content MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE news CHANGE content content MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE page CHANGE content content MEDIUMTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE page_service CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE pricing CHANGE options options MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE topic ADD forums_id INT NOT NULL, CHANGE description description VARCHAR(255) NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B618BA34B FOREIGN KEY (forums_id) REFERENCES forum (id)');
        $this->addSql('CREATE INDEX IDX_9D40DE1B618BA34B ON topic (forums_id)');
        $this->addSql('ALTER TABLE topic_message CHANGE contenu contenu MEDIUMTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE services_site (services_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_ED7A5155AEF5A6C1 (services_id), INDEX IDX_ED7A5155F6BD1646 (site_id), PRIMARY KEY(services_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155AEF5A6C1 FOREIGN KEY (services_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE feature CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE image_video CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE news CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE page CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE page_service CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE pricing CHANGE options options MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE topic MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1B618BA34B');
        $this->addSql('DROP INDEX IDX_9D40DE1B618BA34B ON topic');
        $this->addSql('ALTER TABLE topic DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE topic DROP forums_id, CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE topic_message CHANGE contenu contenu MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
