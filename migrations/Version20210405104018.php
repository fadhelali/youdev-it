<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210405104018 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE header (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, createdby VARCHAR(255) NOT NULL, createdat DATETIME NOT NULL, updatedat DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE services_site');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE page ADD header_id INT DEFAULT NULL, CHANGE content content MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202EF91FD8 FOREIGN KEY (header_id) REFERENCES header (id)');
        $this->addSql('CREATE INDEX IDX_140AB6202EF91FD8 ON page (header_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202EF91FD8');
        $this->addSql('CREATE TABLE services_site (services_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_ED7A5155AEF5A6C1 (services_id), INDEX IDX_ED7A5155F6BD1646 (site_id), PRIMARY KEY(services_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155AEF5A6C1 FOREIGN KEY (services_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE header');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX IDX_140AB6202EF91FD8 ON page');
        $this->addSql('ALTER TABLE page DROP header_id, CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
