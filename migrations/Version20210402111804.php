<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210402111804 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE site_services (site_id INT NOT NULL, services_id INT NOT NULL, INDEX IDX_7F7F5EB3F6BD1646 (site_id), INDEX IDX_7F7F5EB3AEF5A6C1 (services_id), PRIMARY KEY(site_id, services_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_services ADD CONSTRAINT FK_7F7F5EB3F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_services ADD CONSTRAINT FK_7F7F5EB3AEF5A6C1 FOREIGN KEY (services_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE services_site');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE services ADD description VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE services_site (services_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_ED7A5155AEF5A6C1 (services_id), INDEX IDX_ED7A5155F6BD1646 (site_id), PRIMARY KEY(services_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155AEF5A6C1 FOREIGN KEY (services_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE site_services');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE services DROP description');
    }
}
