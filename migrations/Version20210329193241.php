<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329193241 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE footer ADD linkfacebook VARCHAR(255) DEFAULT NULL, ADD linktwitter VARCHAR(255) DEFAULT NULL, ADD linkgithub VARCHAR(255) DEFAULT NULL, ADD linklinkedin VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE footer DROP linkfacebook, DROP linktwitter, DROP linkgithub, DROP linklinkedin');
    }
}
