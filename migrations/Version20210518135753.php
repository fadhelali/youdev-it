<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518135753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE messages_topic (id INT AUTO_INCREMENT NOT NULL, topics_id INT NOT NULL, pseudo VARCHAR(255) NOT NULL, sender VARCHAR(255) NOT NULL, ip VARCHAR(255) DEFAULT NULL, contenu MEDIUMTEXT NOT NULL, INDEX IDX_2842FC60BF06A414 (topics_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE messages_topic ADD CONSTRAINT FK_2842FC60BF06A414 FOREIGN KEY (topics_id) REFERENCES topic (id)');
        $this->addSql('DROP TABLE services_site');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE feature CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE image_video CHANGE content content MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE news CHANGE content content MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE page CHANGE content content MEDIUMTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE page_service CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE pricing CHANGE options options MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE topic CHANGE contenu contenu MEDIUMTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE services_site (services_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_ED7A5155AEF5A6C1 (services_id), INDEX IDX_ED7A5155F6BD1646 (site_id), PRIMARY KEY(services_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155AEF5A6C1 FOREIGN KEY (services_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE services_site ADD CONSTRAINT FK_ED7A5155F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE messages_topic');
        $this->addSql('ALTER TABLE categories CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE feature CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE image_video CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE news CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE page CHANGE content content MEDIUMTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE page_service CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE pricing CHANGE options options MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE topic CHANGE contenu contenu MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
