<?php

namespace App\Entity;

use App\Repository\FooterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FooterRepository::class)
 */
class Footer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailcontact;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="footers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sites;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdby;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkfacebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linktwitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkgithub;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linklinkedin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmailcontact(): ?string
    {
        return $this->emailcontact;
    }

    public function setEmailcontact(string $emailcontact): self
    {
        $this->emailcontact = $emailcontact;

        return $this;
    }

    public function getSites(): ?Site
    {
        return $this->sites;
    }

    public function setSites(?Site $sites): self
    {
        $this->sites = $sites;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getCreatedby(): ?string
    {
        return $this->createdby;
    }

    public function setCreatedby(string $createdby): self
    {
        $this->createdby = $createdby;

        return $this;
    }

    public function getLinkfacebook(): ?string
    {
        return $this->linkfacebook;
    }

    public function setLinkfacebook(?string $linkfacebook): self
    {
        $this->linkfacebook = $linkfacebook;

        return $this;
    }

    public function getLinktwitter(): ?string
    {
        return $this->linktwitter;
    }

    public function setLinktwitter(?string $linktwitter): self
    {
        $this->linktwitter = $linktwitter;

        return $this;
    }

    public function getLinkgithub(): ?string
    {
        return $this->linkgithub;
    }

    public function setLinkgithub(?string $linkgithub): self
    {
        $this->linkgithub = $linkgithub;

        return $this;
    }

    public function getLinklinkedin(): ?string
    {
        return $this->linklinkedin;
    }

    public function setLinklinkedin(?string $linklinkedin): self
    {
        $this->linklinkedin = $linklinkedin;

        return $this;
    }
}
