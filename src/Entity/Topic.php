<?php

namespace App\Entity;

use App\Repository\TopicRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TopicRepository::class)
 */
class Topic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=100000)
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedat;

    /**
     * @ORM\ManyToOne(targetEntity=Forum::class, inversedBy="topics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $formus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdby;

    /**
     * @ORM\OneToMany(targetEntity=MessagesTopic::class, mappedBy="topics", orphanRemoval=true)
     */
    private $messagesTopics;

    /**
     * @ORM\Column(type="boolean")
     */
    private $messagestate=true;

    public function __construct()
    {
        $this->messagesTopics = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getFormus(): ?Forum
    {
        return $this->formus;
    }

    public function setFormus(?Forum $formus): self
    {
        $this->formus = $formus;

        return $this;
    }

    public function getCreatedby(): ?string
    {
        return $this->createdby;
    }

    public function setCreatedby(string $createdby): self
    {
        $this->createdby = $createdby;

        return $this;
    }

    /**
     * @return Collection|MessagesTopic[]
     */
    public function getMessagesTopics(): Collection
    {
        return $this->messagesTopics;
    }

    public function addMessagesTopic(MessagesTopic $messagesTopic): self
    {
        if (!$this->messagesTopics->contains($messagesTopic)) {
            $this->messagesTopics[] = $messagesTopic;
            $messagesTopic->setTopics($this);
        }

        return $this;
    }

    public function removeMessagesTopic(MessagesTopic $messagesTopic): self
    {
        if ($this->messagesTopics->removeElement($messagesTopic)) {
            // set the owning side to null (unless already changed)
            if ($messagesTopic->getTopics() === $this) {
                $messagesTopic->setTopics(null);
            }
        }

        return $this;
    }

    public function getMessagestate(): ?bool
    {
        return $this->messagestate;
    }

    public function setMessagestate(bool $messagestate): self
    {
        $this->messagestate = $messagestate;

        return $this;
    }
}
