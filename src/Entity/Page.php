<?php

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PageRepository::class)
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100000,nullable=true)
     */
    private $content;

   

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdby;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="url_page",orphanRemoval=true)
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity=SubItem::class, mappedBy="url_page",orphanRemoval=true)
     */
    private $subItems;

    /**
     * @ORM\ManyToMany(targetEntity=Site::class, inversedBy="pages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Sites;

    /**
     * @ORM\OneToMany(targetEntity=Header::class, mappedBy="page",orphanRemoval=true)
     */
    private $headers;

    /**
     * @ORM\OneToMany(targetEntity=TeamMembre::class, mappedBy="page", orphanRemoval=true)
     */
    private $teamMembres;

    /**
     * @ORM\OneToMany(targetEntity=Pricing::class, mappedBy="page")
     */
    private $pricings;

    /**
     * @ORM\OneToMany(targetEntity=Feature::class, mappedBy="pages", orphanRemoval=true)
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity=PageService::class, mappedBy="pages", orphanRemoval=true)
     */
    private $pageServices;

    /**
     * @ORM\OneToMany(targetEntity=Gallery::class, mappedBy="pages")
     */
    private $galleries;

    /**
     * @ORM\OneToMany(targetEntity=ImageVideo::class, mappedBy="pages", orphanRemoval=true)
     */
    private $imageVideos;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="pages", orphanRemoval=true)
     */
    private $contacts;

   

   

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->subItems = new ArrayCollection();
        $this->Sites = new ArrayCollection();
        $this->headers = new ArrayCollection();
        $this->teamMembres = new ArrayCollection();
        $this->pricings = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->pageServices = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->imageVideos = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

   

    public function getCreated_At(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdated_At(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdby;
    }

    public function setCreatedBy(string $created_by): self
    {
        $this->createdby = $created_by;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setUrlPage($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getUrlPage() === $this) {
                $item->setUrlPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SubItem[]
     */
    public function getSubItems(): Collection
    {
        return $this->subItems;
    }

    public function addSubItem(SubItem $subItem): self
    {
        if (!$this->subItems->contains($subItem)) {
            $this->subItems[] = $subItem;
            $subItem->setUrlPage($this);
        }

        return $this;
    }

    public function removeSubItem(SubItem $subItem): self
    {
        if ($this->subItems->removeElement($subItem)) {
            // set the owning side to null (unless already changed)
            if ($subItem->getUrlPage() === $this) {
                $subItem->setUrlPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Site[]
     */
    public function getSites(): Collection
    {
        return $this->Sites;
    }

    public function addSite(Site $site): self
    {
        if (!$this->Sites->contains($site)) {
            $this->Sites[] = $site;
        }

        return $this;
    }

    public function removeSite(Site $site): self
    {
        $this->Sites->removeElement($site);

        return $this;
    }

    /**
     * @return Collection|Header[]
     */
    public function getHeaders(): Collection
    {
        return $this->headers;
    }

    public function addHeader(Header $header): self
    {
        if (!$this->headers->contains($header)) {
            $this->headers[] = $header;
            $header->setPage($this);
        }

        return $this;
    }

    public function removeHeader(Header $header): self
    {
        if ($this->headers->removeElement($header)) {
            // set the owning side to null (unless already changed)
            if ($header->getPage() === $this) {
                $header->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TeamMembre[]
     */
    public function getTeamMembres(): Collection
    {
        return $this->teamMembres;
    }

    public function addTeamMembre(TeamMembre $teamMembre): self
    {
        if (!$this->teamMembres->contains($teamMembre)) {
            $this->teamMembres[] = $teamMembre;
            $teamMembre->setPage($this);
        }

        return $this;
    }

    public function removeTeamMembre(TeamMembre $teamMembre): self
    {
        if ($this->teamMembres->removeElement($teamMembre)) {
            // set the owning side to null (unless already changed)
            if ($teamMembre->getPage() === $this) {
                $teamMembre->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pricing[]
     */
    public function getPricings(): Collection
    {
        return $this->pricings;
    }

    public function addPricing(Pricing $pricing): self
    {
        if (!$this->pricings->contains($pricing)) {
            $this->pricings[] = $pricing;
            $pricing->setPage($this);
        }

        return $this;
    }

    public function removePricing(Pricing $pricing): self
    {
        if ($this->pricings->removeElement($pricing)) {
            // set the owning side to null (unless already changed)
            if ($pricing->getPage() === $this) {
                $pricing->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Feature[]
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(Feature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features[] = $feature;
            $feature->setPages($this);
        }

        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->features->removeElement($feature)) {
            // set the owning side to null (unless already changed)
            if ($feature->getPages() === $this) {
                $feature->setPages(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PageService[]
     */
    public function getPageServices(): Collection
    {
        return $this->pageServices;
    }

    public function addPageService(PageService $pageService): self
    {
        if (!$this->pageServices->contains($pageService)) {
            $this->pageServices[] = $pageService;
            $pageService->setPages($this);
        }

        return $this;
    }

    public function removePageService(PageService $pageService): self
    {
        if ($this->pageServices->removeElement($pageService)) {
            // set the owning side to null (unless already changed)
            if ($pageService->getPages() === $this) {
                $pageService->setPages(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Gallery[]
     */
    public function getGalleries(): Collection
    {
        return $this->galleries;
    }

    public function addGallery(Gallery $gallery): self
    {
        if (!$this->galleries->contains($gallery)) {
            $this->galleries[] = $gallery;
            $gallery->setPages($this);
        }

        return $this;
    }

    public function removeGallery(Gallery $gallery): self
    {
        if ($this->galleries->removeElement($gallery)) {
            // set the owning side to null (unless already changed)
            if ($gallery->getPages() === $this) {
                $gallery->setPages(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ImageVideo[]
     */
    public function getImageVideos(): Collection
    {
        return $this->imageVideos;
    }

    public function addImageVideo(ImageVideo $imageVideo): self
    {
        if (!$this->imageVideos->contains($imageVideo)) {
            $this->imageVideos[] = $imageVideo;
            $imageVideo->setPages($this);
        }

        return $this;
    }

    public function removeImageVideo(ImageVideo $imageVideo): self
    {
        if ($this->imageVideos->removeElement($imageVideo)) {
            // set the owning side to null (unless already changed)
            if ($imageVideo->getPages() === $this) {
                $imageVideo->setPages(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPages($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getPages() === $this) {
                $contact->setPages(null);
            }
        }

        return $this;
    }


   
public function to_String():String
{
    return " ".$this->getName()."for site ".$this->getSites();
}
  
}
