<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="items")
     */
    private $url_page;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $menus_item;

    /**
     * @ORM\OneToMany(targetEntity=SubItem::class, mappedBy="items", orphanRemoval=true)
     */
    private $subItems;

    public function __construct()
    {
        $this->subItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrlPage(): ?Page
    {
        return $this->url_page;
    }

    public function setUrlPage(?Page $url_page): self
    {
        $this->url_page = $url_page;

        return $this;
    }
    public function geturl_page(): ?Page
    {
        return $this->url_page;
    }


    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMenusItem(): ?Menu
    {
        return $this->menus_item;
    }
    public function getmenus_item(): ?Menu
    {
        return $this->menus_item;
    }

    public function setMenusItem(?Menu $menus_item): self
    {
        $this->menus_item = $menus_item;

        return $this;
    }

    /**
     * @return Collection|SubItem[]
     */
    public function getSubItems(): Collection
    {
        return $this->subItems;
    }

    public function addSubItem(SubItem $subItem): self
    {
        if (!$this->subItems->contains($subItem)) {
            $this->subItems[] = $subItem;
            $subItem->setItems($this);
        }

        return $this;
    }

    public function removeSubItem(SubItem $subItem): self
    {
        if ($this->subItems->removeElement($subItem)) {
            // set the owning side to null (unless already changed)
            if ($subItem->getItems() === $this) {
                $subItem->setItems(null);
            }
        }

        return $this;
    }
}
