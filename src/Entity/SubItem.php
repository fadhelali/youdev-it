<?php

namespace App\Entity;

use App\Repository\SubItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubItemRepository::class)
 */
class SubItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="subItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $items;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="subItems")
     */
    private $url_page;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getItems(): ?Item
    {
        return $this->items;
    }

    public function setItems(?Item $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrlPage(): ?Page
    {
        return $this->url_page;
    }
    public function geturl_page(): ?Page
    {
        return $this->url_page;
    }


    public function setUrlPage(?Page $url_page): self
    {
        $this->url_page = $url_page;

        return $this;
    }
}
