<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\NotifiableInterface;
use Mgilet\NotificationBundle\Annotation\Notifiable;
/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @Notifiable(name="users")
 */
class User extends BaseUser implements NotifiableInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedinId;

    /**
     * @ORM\OneToMany(targetEntity=Articles::class, mappedBy="users", orphanRemoval=true)
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="users", orphanRemoval=true)
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity=UserInvit::class, mappedBy="receiverinvit", orphanRemoval=true)
     */
    private $userInvits;

    /**
     * @ORM\OneToMany(targetEntity=MessagesTopic::class, mappedBy="users")
     */
    private $messagesTopics;

  

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->articles = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->userInvits = new ArrayCollection();
        $this->messagesTopics = new ArrayCollection();
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getLinkedinId(): ?string
    {
        return $this->linkedinId;
    }

    public function setLinkedinId(?string $linkedinId): self
    {
        $this->linkedinId = $linkedinId;

        return $this;
    }

    /**
     * @return Collection|Articles[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Articles $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setUsers($this);
        }

        return $this;
    }

    public function removeArticle(Articles $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getUsers() === $this) {
                $article->setUsers(null);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setUsers($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getUsers() === $this) {
                $commentaire->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserInvit[]
     */
    public function getUserInvits(): Collection
    {
        return $this->userInvits;
    }

    public function addUserInvit(UserInvit $userInvit): self
    {
        if (!$this->userInvits->contains($userInvit)) {
            $this->userInvits[] = $userInvit;
            $userInvit->setReceiverInvit($this);
        }

        return $this;
    }

    public function removeUserInvit(UserInvit $userInvit): self
    {
        if ($this->userInvits->removeElement($userInvit)) {
            // set the owning side to null (unless already changed)
            if ($userInvit->getReceiverInvit() === $this) {
                $userInvit->setReceiverInvit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MessagesTopic[]
     */
    public function getMessagesTopics(): Collection
    {
        return $this->messagesTopics;
    }

    public function addMessagesTopic(MessagesTopic $messagesTopic): self
    {
        if (!$this->messagesTopics->contains($messagesTopic)) {
            $this->messagesTopics[] = $messagesTopic;
            $messagesTopic->setUsers($this);
        }

        return $this;
    }

    public function removeMessagesTopic(MessagesTopic $messagesTopic): self
    {
        if ($this->messagesTopics->removeElement($messagesTopic)) {
            // set the owning side to null (unless already changed)
            if ($messagesTopic->getUsers() === $this) {
                $messagesTopic->setUsers(null);
            }
        }

        return $this;
    }

   
}