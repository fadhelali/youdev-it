<?php

namespace App\Entity;

use App\Repository\UserInvitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserInvitRepository::class)
 */
class UserInvit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $createdby;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedat;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userInvits")
     */
    private $receiverinvit;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="userInvitsite")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Sites;

    /**
     * @ORM\Column(type="array")
     * @var array
     */
    private $Roles ;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;
    public function __construct()
    {
        $this->Roles = array();
    }
   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedby(): ?string
    {
        return $this->createdby;
    }

    public function setCreatedby(string $createdby): self
    {
        $this->createdby = $createdby;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getReceiverinvit(): ?User
    {
        return $this->receiverinvit;
    }

    public function setReceiverinvit(?User $ReceiverInvit): self
    {
        $this->receiverinvit = $ReceiverInvit;

        return $this;
    }
    

    public function getSites(): ?Site
    {
        return $this->Sites;
    }

    public function setSites(?Site $Sites): self
    {
        $this->Sites = $Sites;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->Roles;
    }

    public function setRoles(array $Roles): self
    {
        $this->Roles = $Roles;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
    public function addRole($role)
    {
        $role = strtoupper($role);
     

        if (!in_array($role, $this->Roles, true)) {
            $this->Roles[] = $role;
        }

        return $this;
    }

  
    public function hasRoles($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }
}
