<?php

namespace App\Entity;

use App\Repository\SiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SiteRepository::class)
 * @Vich\Uploadable
 */
class Site
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Unique
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

   

    /**
     * @ORM\OneToMany(targetEntity=FAQ::class, mappedBy="sites", orphanRemoval=true)
     */
    private $fAQs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;
    /**
     * @Vich\UploadableField(mapping="featured_images", fileNameProperty="logo")
     * @var File
     * @ORM\JoinColumn(nullable=true)
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity=Menu::class, mappedBy="sites", orphanRemoval=true)
     */
    private $menus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @ORM\OneToMany(targetEntity=Footer::class, mappedBy="sites", orphanRemoval=true)
     */
    private $footers;

    /**
     * @ORM\ManyToMany(targetEntity=Services::class, inversedBy="sites")
     * @ORM\JoinColumn(nullable=true)
     */
    private $services;

    /**
     * @ORM\ManyToMany(targetEntity=Page::class, mappedBy="Sites",orphanRemoval=true)
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity=UserInvit::class, mappedBy="Sites", orphanRemoval=true)
     */
    private $userInvitsite;

    /**
     * @ORM\ManyToMany(targetEntity=News::class, mappedBy="sites")
     */
    private $news;

    public function __construct()
    {
        $this->fAQs = new ArrayCollection();
        $this->menus = new ArrayCollection();
        $this->footers = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->userInvitsite = new ArrayCollection();
        $this->news = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published)
    {
        $this->published = $published;

        
    }

    public function getCreated_At(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at)
    {
        $this->created_at = $created_at;

        
    }

    public function getUpdated_At(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at)
    {
        $this->updated_at = $updated_at;

        
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author)
    {
        $this->author = $author;

       
                
    }

   

    /**
     * @return Collection|FAQ[]
     */
    public function getFAQs(): Collection
    {
        return $this->fAQs;
    }

    public function addFAQ(FAQ $fAQ): self
    {
        if (!$this->fAQs->contains($fAQ)) {
            $this->fAQs[] = $fAQ;
            $fAQ->setSites($this);
        }

        return $this;
    }

    public function removeFAQ(FAQ $fAQ): self
    {
        if ($this->fAQs->removeElement($fAQ)) {
            // set the owning side to null (unless already changed)
            if ($fAQ->getSites() === $this) {
                $fAQ->setSites(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
    
        if ($image) {
            $this->updated_at = new \DateTime('now');
        }
    }
    
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->setSites($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->removeElement($menu)) {
            // set the owning side to null (unless already changed)
            if ($menu->getSites() === $this) {
                $menu->setSites(null);
            }
        }

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @return Collection|Footer[]
     */
    public function getFooters(): Collection
    {
        return $this->footers;
    }

    public function addFooter(Footer $footer): self
    {
        if (!$this->footers->contains($footer)) {
            $this->footers[] = $footer;
            $footer->setSites($this);
        }

        return $this;
    }

    public function removeFooter(Footer $footer): self
    {
        if ($this->footers->removeElement($footer)) {
            // set the owning side to null (unless already changed)
            if ($footer->getSites() === $this) {
                $footer->setSites(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Services[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Services $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
        }

        return $this;
    }

    public function removeService(Services $service): self
    {
        $this->services->removeElement($service);

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->addSite($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->removeElement($page)) {
            $page->removeSite($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserInvit[]
     */
    public function getUserInvitsite(): Collection
    {
        return $this->userInvitsite;
    }

    public function addUserInvitsite(UserInvit $userInvitsite): self
    {
        if (!$this->userInvitsite->contains($userInvitsite)) {
            $this->userInvitsite[] = $userInvitsite;
            $userInvitsite->setSites($this);
        }

        return $this;
    }

    public function removeUserInvitsite(UserInvit $userInvitsite): self
    {
        if ($this->userInvitsite->removeElement($userInvitsite)) {
            // set the owning side to null (unless already changed)
            if ($userInvitsite->getSites() === $this) {
                $userInvitsite->setSites(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->addSite($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->removeElement($news)) {
            $news->removeSite($this);
        }

        return $this;
    }

   
    
}
