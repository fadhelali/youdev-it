<?php

namespace App\Command;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreatePageCommand extends Command
{
    protected static $defaultName = 'youdevcms:page:create';
    protected static $defaultDescription = 'Add a short description for your command';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setDescription('Create Site Command')
        ->setDefinition(array(
            new InputArgument('name', InputArgument::REQUIRED, 'The page name'),
            new InputArgument('createdby', InputArgument::REQUIRED, 'The page user'),
            new InputArgument('description', InputArgument::REQUIRED, 'The page description'),
        ));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Command Create page',
            '==========================================================='
        ]);
        $page = new Page();
        $page->setName($input->getArgument("name"));
        $output->writeln("With name : ". $page->getName());
        $url = $input->getArgument("createdby");
        $page->setCreatedby($input->getArgument("createdby"));
        $date=new \DateTime('now');
        $page->setCreatedAt($date);
        $page->setUpdatedAt($date);
        $page->setPublished(false);
        $page->setDescription($input->getArgument("description"));
        $output->writeln("With description : ".$page->getDescription() );
        $this->entityManager->persist($page);
        $this->entityManager->flush();
        $output->writeln([
            'Finished!',
            '==========================================================='
        ]);
        $output->writeln('New Page Has Been Created ');
        return 0;
    
    }


    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('name')) {
            $question = new Question('Please choose a page name:');
            $question->setValidator(function ($name) {
                if (empty($name)) {
                    throw new \Exception('The site name can not be empty');
                }

                return $name;
            });
            $questions['name'] = $question;
        }

       

        if (!$input->getArgument('createdby')) {
            $question = new Question('Please Enter your Username :');
            $question->setValidator(function ($createdby) {
                if (empty($createdby)) {
                    throw new \Exception('The page Author can not be empty');
                }

                return $createdby;
            });
            $question->setHidden(false);
            $questions['createdby'] = $question;
        }
        if (!$input->getArgument('description')) {
            $question = new Question('Please enter a description for page:');
            $question->setValidator(function ($description) {
                if (empty($description)) {
                    throw new \Exception('The page description can not be empty');
                }

                return $description;
            });
            $question->setHidden(false);
            $questions['description'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
