<?php

namespace App\Command;

use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class YoudevItUserEnableCommand extends Command
{
    protected static $defaultName = 'youdevcms:user:enable';
    protected static $defaultDescription = 'Add a short description for your command';
    private $userManipulator="fos_user.util.user_manipulator";


    public function __construct(UserManagerInterface $userManipulator)
    {
        parent::__construct();

        $this->userManipulator = $userManipulator;
    }
    protected function configure()
    {
        $this
        ->setDefinition(array(
            new InputArgument('username', InputArgument::REQUIRED, 'The username'),
        ))
        ->setHelp(<<<'EOT'
The <info>fos:user:activate</info> command activates a user (so they will be able to log in):

<info>php %command.full_name% matthieu</info>
EOT
        );
}
    

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = $input->getArgument('username');
         $user=$this->userManipulator->findUserByUsername($username);
        $user->setEnabled(true);

        $output->writeln(sprintf('User "%s" has been activated.', $username));
        return 0;
    }
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }

                return $username;
            });
            $answer = $this->getHelper('question')->ask($input, $output, $question);

            $input->setArgument('username', $answer);
        }
    }
}
