<?php

namespace App\Command;

use App\Entity\Site;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Security;

class CreateSiteCommand extends Command
{
    protected static $defaultName = 'youdevcms:site:create';
    protected static $defaultDescription = 'Create your own';
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setDescription('Create Site Command')
        ->setDefinition(array(
            new InputArgument('name', InputArgument::REQUIRED, 'The site name'),
            new InputArgument('url', InputArgument::REQUIRED, 'The site url '),
            new InputArgument('description', InputArgument::REQUIRED, 'The site description'),
            new InputArgument('author', InputArgument::REQUIRED, 'the site user'),
        ))
        ->setHelp(<<<'EOT'
        The <info>youdev-it:site:create</info> command creates a site:
        
          <info>php %command.full_name% real</info>
        
        
        EOT
                    );
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $output->writeln([
            'Command Create Site',
            '============'
        ]);
        $site = new Site();
        $site->setName($input->getArgument("name"));
        $output->writeln("With name : ". $site->getName());
        $url = $input->getArgument("url");
        $site->setUrl($input->getArgument("url"));
        $site->setAuthor($input->getArgument("author"));
        $date=new \DateTime('now');
        $site->setCreatedAt($date);
        $site->setUpdatedAt($date);
        $site->setPublished(false);
        $site->setTheme("default");
        $output->writeln("With url : ". $url);
        $site->setDescription($input->getArgument("description"));
        $output->writeln("With description : ".$site->getDescription() );
        $this->entityManager->persist($site);
        $this->entityManager->flush();
        $output->writeln('site with default theme created successffully');
        return 0;
    
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('name')) {
            $question = new Question('Please choose a site name:');
            $question->setValidator(function ($name) {
                if (empty($name)) {
                    throw new \Exception('The site name can not be empty');
                }

                return $name;
            });
            $questions['name'] = $question;
        }

        if (!$input->getArgument('url')) {
            $question = new Question('Please choose an url:');
            $question->setValidator(function ($url) {
                if (empty($url)) {
                    throw new \Exception('The site url can not be empty');
                }

                return $url;
            });
            $questions['url'] = $question;
        }

        if (!$input->getArgument('author')) {
            $question = new Question('Please choose a Author site:');
            $question->setValidator(function ($author) {
                if (empty($author)) {
                    throw new \Exception('The site Author can not be empty');
                }

                return $author;
            });
            $question->setHidden(false);
            $questions['author'] = $question;
        }
        if (!$input->getArgument('description')) {
            $question = new Question('Please enter a description for site:');
            $question->setValidator(function ($description) {
                if (empty($description)) {
                    throw new \Exception('The site description can not be empty');
                }

                return $description;
            });
            $question->setHidden(false);
            $questions['description'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
