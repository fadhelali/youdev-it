<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 */
class Mailer implements MailerInterface
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param EngineInterface       $templating
     * @param array                 $parameters
     */
    public function __construct($mailer, UrlGeneratorInterface  $router, EngineInterface $templating, array $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['confirmation.template'];
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
      /*  $rendered = $this->templating->render("Emails/inscription.html.twig", array(
            'user' => $user,
            'confirmationUrl' => $url,
        ));*/
        
        $subject="Inscription Confirmation";
        $message = (new \Swift_Message())
        ->setSubject($subject)
        ->setFrom($this->parameters['from_email']['confirmation'])
        ->setTo((string) $user->getEmail())
        ->setContentType('text/html');
        $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
        $img1 = $message->embed(\Swift_Image::fromPath('images/ico-facebook.png'));
        $img2 = $message->embed(\Swift_Image::fromPath('images/ico-twitter.png'));
        $img3 = $message->embed(\Swift_Image::fromPath('images/ico-google-plus.png'));
        $img4 = $message->embed(\Swift_Image::fromPath('images/ico-linkedin.png'));
        $img7 = $message->embed(\Swift_Image::fromPath('images/img-07.jpg'));
        $img8 = $message->embed(\Swift_Image::fromPath('images/img-08.jpg'));
     
        $img9 = $message->embed(\Swift_Image::fromPath('images/img-09.jpg'));
        $img10 = $message->embed(\Swift_Image::fromPath('images/img-10.jpg'));
        $img11 = $message->embed(\Swift_Image::fromPath('images/img-11.jpg'));
        $img12 = $message->embed(\Swift_Image::fromPath('images/img-12.jpg'));
        $message->setBody($this->templating->render('Emails/inscription.html.twig', ['user'=>$user,'confirmationUrl' => $url,'img' => $img,'img1' => $img1,'img2' => $img2,'img3' => $img3,'img4' => $img4,'img7' => $img7,'img8' => $img8,'img9' => $img9,'img10' => $img10,'img11' => $img11,'img12' => $img12]));

    $this->mailer->send($message);
       // $this->sendEmailMessage($rendered, $this->parameters['from_email']['confirmation'], (string) $user->getEmail());
    }

    /**
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['resetting.template'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
      /*  $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url,
        ));*/
        $subject="Resetting Password";
        $message = (new \Swift_Message())
        ->setSubject($subject)
        ->setFrom($this->parameters['from_email']['resetting'])
        ->setTo((string) $user->getEmail())
        ->setContentType('text/html');
        $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
        $img1 = $message->embed(\Swift_Image::fromPath('images/ico-facebook.png'));
        $img2 = $message->embed(\Swift_Image::fromPath('images/ico-twitter.png'));
        $img3 = $message->embed(\Swift_Image::fromPath('images/ico-google-plus.png'));
        $img4 = $message->embed(\Swift_Image::fromPath('images/ico-linkedin.png'));
        $img7 = $message->embed(\Swift_Image::fromPath('images/img-07.jpg'));
        $img8 = $message->embed(\Swift_Image::fromPath('images/img-08.jpg'));
     
        $img9 = $message->embed(\Swift_Image::fromPath('images/img-09.jpg'));
        $img10 = $message->embed(\Swift_Image::fromPath('images/img-10.jpg'));
        $img11 = $message->embed(\Swift_Image::fromPath('images/img-11.jpg'));
        $img12 = $message->embed(\Swift_Image::fromPath('images/img-12.jpg'));
        $message->setBody($this->templating->render('Emails/resetting.html.twig', ['user'=>$user,'confirmationUrl' => $url,'img' => $img,'img1' => $img1,'img2' => $img2,'img3' => $img3,'img4' => $img4,'img7' => $img7,'img8' => $img8,'img9' => $img9,'img10' => $img10,'img11' => $img11,'img12' => $img12]));

    $this->mailer->send($message);
       // $this->sendEmailMessage($rendered, $this->parameters['from_email']['resetting'], (string) $user->getEmail());
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $fromEmail
     * @param array|string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail)
    {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            
            ->setBody($body);

        $this->mailer->send($message);
    }
}
