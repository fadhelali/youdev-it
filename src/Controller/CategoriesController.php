<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Repository\CategoriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class CategoriesController extends AbstractController
{
  
    public function ListCat(CategoriesRepository $catrep): Response
    {
        $em=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
       $list_cat =$catrep->findByCreatedby($username); 
        return $this->render('categories/index.html.twig', [
            'list_cat' => $list_cat,
        ]);
    }
    public function AddCat(Request $request ,TranslatorInterface $translator): Response
    { 
        $Category = new Categories();
        $form = $this->createForm('App\Form\CategoriesType',$Category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $Category->setCreatedBy($user->getUsername());
             $date = new \DateTime('now');
             $Category->setcreated_at($date);
             $Category->setupdated_at($date);
   
      
            $em->persist($Category);
            $em->flush();
            $message=$translator->trans('New Category has been added successfully');
            $this->get('session')->getFlashBag()->add(
    'cat',
    array(
       
        'message' => $message 
    )
);
            return $this->redirectToRoute('cms_list_categories');
        }
        return $this->render('categories/add_cat.html.twig', array(
           
            'form' => $form->createView(),
        ));
    }
    public function UpdateCat(Request $request,CategoriesRepository $catrep,$id,TranslatorInterface $translator): Response
    {
        $cat = $catrep->find($id);
        $form = $this->createForm('App\Form\CategoriesType', $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
           
      
            $date = new \DateTime('now');
     
            $cat->setupdated_at($date);
            
   
      
           
            $em->flush();
            $message=$translator->trans('Category has been updated successfully');

            $this->get('session')->getFlashBag()->add(
                'cat',
                array(
       
        'message' => $message
    )
            );
            return $this->redirectToRoute('cms_list_categories');
        }

        return $this->render('categories/modifier_cat.html.twig', array(
           
            'form' => $form->createView(),
        ));
    }
    public function DeleteCat(CategoriesRepository $catrep,$id): Response
    {
        $em=$this->getDoctrine()->getManager();
        $cat=$catrep->find($id); 
        $em->remove($cat);
        $em->flush();
        return $this->redirect($this->generateUrl('cms_list_categories'));  
    }
    public function ShowCat(CategoriesRepository $catrep,$id): Response
    {
                $em=$this->getDoctrine()->getManager();
                $cat=$catrep->find($id); 
               
                
        return $this->render('categories/show_cat.html.twig', array(
           
            'cat' => $cat,
        ));  
    }
}
