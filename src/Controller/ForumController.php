<?php

namespace App\Controller;

use App\Entity\Forum;
use App\Entity\MessagesTopic;
use App\Entity\Site;
use App\Entity\Topic;
use App\Form\ForumType;
use App\Repository\ForumRepository;
use App\Repository\MessagesTopicRepository;
use App\Repository\SiteRepository;
use App\Repository\TopicRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/forum")
 */
class ForumController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_forum_index")
     */
    public function index(ForumRepository $np): Response
    {
        $em=$this->getDoctrine()->getManager();
        $formus=$np->findByCreatedby($this->getUser()->getUserName());
        return $this->render('forum/index.html.twig', [
            'list_forums' =>$formus,
        ]);
    }

    /**
     * @Route("/forum/new/{idsite}", name="cms_forum_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $forum = new Forum();
        
      //  $form = $this->createForm(ForumType::class, $forum);
        //$form->handleRequest($request);
        if ($idsite=="null") {
    //if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $forum->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $forum->setCreatedAt($date);
                $forum->setUpdatedAt($date);
                $forum->setName($_POST['name']);

               
           

                $entityManager->persist($forum);
                $entityManager->flush();
                $message=$translator->trans('New  Forum  added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'forum',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_forum_index');
            }

           /* return $this->render('forum/new.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),
        ]);
        } */
        elseif ( $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                  //  if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                   
                        $em = $this->getDoctrine()->getManager();
                        $forum->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $forum->setCreatedAt($date);
                        $forum->setUpdatedAt($date);
                        $forum->setName($_POST['name']);


        
                        $entityManager->persist($forum);
                        $entityManager->flush();
                        $message=$translator->trans(' Forum added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'forum',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding  forum!');
                        $notif->setMessage('new forum has been added by '. $this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_news', array(
                            'id'=> $idsite,
                        ));
                    
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a Forum to you ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add a Forum to you');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        /*return $this->render('news/new.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),

            ]);*/
        
    }
    


    /**
     * @Route("/{id}/updateforum/update/{idsite}", name="cms_update_forum", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,TranslatorInterface $translator,ForumRepository $sr, $id,$idsite,UserInvitRepository $ui): Response
    {
        $forum =$sr->find($id);
        
       // $form = $this->createForm(ForumType::class, $forum);
        //$form->handleRequest($request);

        if ($idsite=="null") {
           // if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $forum->setUpdatedAt($date);
                $forum->setName($_POST['name1']);


                $entityManager->flush();
                $message=$translator->trans('forum  updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'forum',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_forum_index');
            }

            /*return $this->render('forum/update.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),
        ]);
        }*/ elseif ($idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                   // if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $forum->setUpdatedAt($date);
                        $forum->setName($_POST['name1']);

        
                        $entityManager->flush();
                        $message=$translator->trans(' forum  updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'forum',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('editing Forums!');
                        $notif->setMessage('editing forums  by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_news', array(
                            'id'=> $idsite,
                        ));
                    
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit forum');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to edit forum');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
       /* return $this->render('news/update.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),

            ]);*/
        
    }
    /**
     * @Route("/{id}/deletenews/delete/{idsite}/#", name="cms_forum_delete")
     */
    
    public function delete($id, ForumRepository $sr,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $forum =$sr->find($id);
                    $em->remove($forum);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting forum!');
                    $notif->setMessage('deleting forum by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_news', array(
                    'id'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your forum');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your forum');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $forum =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$forum->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($forum);
                $em->flush();
                return $this->redirectToRoute('cms_forum_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your  forum');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    }


/*

    public function ViewNewsInvitPage($id,UserInvitRepository $ui,NewsRepository $pr,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();
     
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $news=$site->getNews();
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/Viewnews.html.twig', array(
                   
          
            'list_news' => $news,
            'idsite'=>$id,
           
            
        ));
           }
           else{

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access to your news  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access to your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
*/



// list forum for users
public function ListForum(Request $request,SiteRepository $siteRepository,$id,UserRepository $userrep,ForumRepository $forumrep,PaginatorInterface $paginator){
    $em=$this->getDoctrine()->getManager();
    $site=$siteRepository->find($id);
    $utilisateur=$userrep->findOneByUsername($site->getAuthor());
    $pagination=$forumrep->findByCreatedby($utilisateur->getUsername());
    //$cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();
    $forums = $paginator->paginate(
        $pagination, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        4 /*limit per page*/
    );
  
    if ($site->getTheme()=="default" || $site->getTheme()=="") {
        return $this->render('frontend/fontsite/view_forum_front.html.twig', array(

    'site' => $site,
    'list_forums'=> $forums,

    ));
    }  
    elseif ($site->getTheme()=="template1") {
        $forums = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('frontend/fontsite/template1/view_forum_front.html.twig', array(

            'site' => $site,
            'list_forums'=> $forums,

    ));
    } 
    elseif ($site->getTheme()=="template2") {
        $forums = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('frontend/fontsite/template2/view_forum_front.html.twig', array(

            'site' => $site,
            'list_forums'=> $forums,

    ));
    }  
    elseif ($site->getTheme()=="template3") {
        return $this->render('frontend/fontsite/template3/view_forum_front.html.twig', array(

            'site' => $site,
            'list_forums'=> $forums,


    ));
    }
    elseif ($site->getTheme()=="template4") {
        return $this->render('frontend/fontsite/template4/view_forum_front.html.twig', array(

            'site' => $site,
            'list_forums'=> $forums,

    ));
    }
}
// list topic for the forum selected
public function ListTopicForum(Request $request,TopicRepository $toprep,SiteRepository $siteRepository,$id,$ids,UserRepository $userrep,ForumRepository $forumrep,PaginatorInterface $paginator){
    $em=$this->getDoctrine()->getManager();
    $site=$siteRepository->find($ids);
    $utilisateur=$userrep->findOneByUsername($site->getAuthor());
    $forum=$forumrep->find($id);
    $pagination=$toprep->findByFormus($forum);
    //$cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();

    $topics = $paginator->paginate(
        $pagination, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        4 /*limit per page*/
    );
  
    if ($site->getTheme()=="default" || $site->getTheme()=="") {
        return $this->render('frontend/fontsite/view_topic_front.html.twig', array(

    'site' => $site,
    'list_topics'=> $topics,

    ));
    }  
    elseif ($site->getTheme()=="template1") {
      
        return $this->render('frontend/fontsite/template1/view_topic_front.html.twig', array(

            'site' => $site,
            'list_topics'=> $topics,

    ));
    } 
    elseif ($site->getTheme()=="template2") {
      
        return $this->render('frontend/fontsite/template2/view_topic_front.html.twig', array(

            'site' => $site,
            'list_topics'=> $topics,

    ));
    }  
    elseif ($site->getTheme()=="template3") {
        return $this->render('frontend/fontsite/template3/view_topic_front.html.twig', array(

            'site' => $site,
            'list_topics'=> $topics,


    ));
    }
    elseif ($site->getTheme()=="template4") {
        return $this->render('frontend/fontsite/template4/view_topic_front.html.twig', array(

            'site' => $site,
            'list_topics'=> $topics,

    ));
    }
}
// verifier si les messages sont activer pour les topics
public function ChangeState($id,TopicRepository $toprep){
    $topic=$toprep->find($id);
    $em=$this->getDoctrine()->getManager();
    if($topic->getMessagestate()==true){
        $topic->setMessagestate(false) ; 
        $em->flush();
    }
    else{
        $topic->setMessagestate(true);
        $em->flush();

    }
    return $this->redirectToRoute('cms_topic_index');
}

public function ListTopicMessages(Request $request,MessagesTopicRepository $messtoprep,TopicRepository $toprep,SiteRepository $siteRepository,$id,$ids,UserRepository $userrep,ForumRepository $forumrep,PaginatorInterface $paginator){
    $em=$this->getDoctrine()->getManager();
    $site=$siteRepository->find($ids);
    $utilisateur=$userrep->findOneByUsername($site->getAuthor());
$topic=$toprep->find($id);
    $pagination=$messtoprep->findByTopics($topic);
    //$cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();

    $messages = $paginator->paginate(
        $pagination, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        4 /*limit per page*/
    );
    $idc=0;
  
    if ($site->getTheme()=="default" || $site->getTheme()=="") {
        return $this->render('frontend/fontsite/view_messages_front.html.twig', array(

    'site' => $site,
    'list_messages'=> $messages,
    'topic'=> $topic,
    'idc'=> $idc,


    ));
    }  
    elseif ($site->getTheme()=="template1") {
      
        return $this->render('frontend/fontsite/template1/view_messages_front.html.twig', array(
            'site' => $site,
            'list_messages'=> $messages,
            'topic'=> $topic,
            'idc'=> $idc,


    ));
    } 
    elseif ($site->getTheme()=="template2") {
      
        return $this->render('frontend/fontsite/template2/view_messages_front.html.twig', array(

            'site' => $site,
          'list_messages'=> $messages,
           'topic'=> $topic,
           'idc'=> $idc,


    ));
    }  
    elseif ($site->getTheme()=="template3") {
        return $this->render('frontend/fontsite/template3/view_messages_front.html.twig', array(

            'site' => $site,
            'list_messages'=> $messages,
            'topic'=> $topic,
            'idc'=> $idc,



    ));
    }
    elseif ($site->getTheme()=="template4") {
        return $this->render('frontend/fontsite/template4/view_messages_front.html.twig', array(

            'site' => $site,
            'list_messages'=> $messages,
            'topic'=> $topic,
            'idc'=> $idc,


    ));
    }
}



public function AddMessage($id,$ids,SiteRepository $siterep,TopicRepository $toprep,Request $request,UserRepository $sr)
  {
      $message= new MessagesTopic();
      $topic=$this->getDoctrine()->getRepository(Topic::class)->find($id);
      $site=$siterep->find($ids);
      //Request::setTrustedProxies(['10.0.0.1']);

      $Ip = $request->getClientIp();
          // the client is a known one, so give it some more privilege
      
      // $user = $this->getUser();
         
      $em = $this->getDoctrine()->getManager();
      $user=$sr->findOneByEmail($_POST['email']);
      if ($user !=null) {
          $message->setusers($user);
      
      $message->setPseudo($_POST['name']);
      $message->setIp($Ip);
      $message->setSender($_POST['name']);


  } else{
    $message->setSender($_POST['name']);
     
      $message->setPseudo($_POST['name']);
      $message->setusers(null);
      $message->setIp($Ip);

       }

         $message->setTopics($topic);
           $date = new \DateTime('now');
          $message->setCreatedAt($date);
          $message->setUpdatedAt($date);
          $message->setContenu($_POST['contenu']);
 
    
          $em->persist($message);
          $em->flush();
          
          $userrecivernot=$sr->findOneByUsername($site->getAuthor());
          $langue= $request->getSession()->get('_locale');
          if ($langue=="fr") {
              $notif = $this->manager->createNotification('Ajout d\'un message !');
              $notif->setMessage('Nouvel message ajouter a votre topic'.$topic->getTitre());
              $notif->setLink('https://Youdev-it.com/');
              // or the one-line method :
              // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
              // you can add a notification to a list of entities
              // the third parameter `$flush` allows you to directly flush the entities
              $this->manager->addNotification(array($userrecivernot), $notif, true);
          } elseif ($langue=="ar") {
              $notif = $this->manager->createNotification(' اضافة تعليق !');
              $notif->setMessage($topic->getTitre().' تم إضافة تعليق جديد لمنشورك ');
              $notif->setLink('https://Youdev-it.com/');
              // or the one-line method :
              // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
              // you can add a notification to a list of entities
              // the third parameter `$flush` allows you to directly flush the entities
              $this->manager->addNotification(array($userrecivernot), $notif, true);
          } elseif ($langue=="de") {
              $notif = $this->manager->createNotification('Kommentar hinzufügen !');
              $notif->setMessage('Neuer message zu Ihrem Artikel hinzufügen'.$topic->getTitre());
              $notif->setLink('https://Youdev-it.com/');
              // or the one-line method :
              // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
              // you can add a notification to a list of entities
              // the third parameter `$flush` allows you to directly flush the entities
              $this->manager->addNotification(array($userrecivernot), $notif, true);
          } else {
              $notif = $this->manager->createNotification('New Comment Adding!');
              $notif->setMessage('New message has been added to your post'.$topic->getTitre());
              $notif->setLink('https://Youdev-it.com/');
              // or the one-line method :
              // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
              // you can add a notification to a list of entities
              // the third parameter `$flush` allows you to directly flush the entities
              $this->manager->addNotification(array($userrecivernot), $notif, true);
          }
     
          return $this->redirectToRoute('cms_view_messages_topic',[
              'id'=>$id,
              'ids'=>$ids,
          ]);

  }


  public function DeleteMessage($id,$idt,$ids,UserRepository $userrep  ,SiteRepository $siterep,PaginatorInterface $paginator,Request $request,MessagesTopicRepository $messagerep): Response
  {
      $em=$this->getDoctrine()->getManager();
      $message=$this->getDoctrine()->getRepository(MessagesTopic::class)->find($id); 
      $topic=$this->getDoctrine()->getRepository(Topic::class)->find($idt);
      $user=$this->getUser();
      $site=$siterep->find($ids);
      $pagination=$messagerep->findByTopics($topic);
      $messages = $paginator->paginate(
          $pagination, /* query NOT result */
          $request->query->getInt('page', 1), /*page number*/
          4 /*limit per page*/
      );
      if ($user!= null) {
          if (($user == $message->getUsers()) or $user->hasRole('ROLE_ADMIN')) {
              $em->remove($message);
              $em->flush();
              $usernotif=$userrep->findOneByUsername($topic->getCreatedby());
              $langue= $request->getSession()->get('_locale');
              if ($langue=="fr") {
                  $notif = $this->manager->createNotification('suppression message!');
                  $notif->setMessage(' message  associee aux topic'.$topic->getTitre().'est supprimer  ');
                  $notif->setLink('https://Youdev-it.com/');
                  // or the one-line method :
                  // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                  // you can add a notification to a list of entities
                  // the third parameter `$flush` allows you to directly flush the entities
                  $this->manager->addNotification(array($usernotif), $notif, true);
              } elseif ($langue=="ar") {
                  $notif = $this->manager->createNotification('حذف تعليق!');
                  $notif->setMessage($topic->getTitre().'تم حذف تعليق لمنشورك');
                  $notif->setLink('https://Youdev-it.com/');
                  // or the one-line method :
                  // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                  // you can add a notification to a list of entities
                  // the third parameter `$flush` allows you to directly flush the entities
                  $this->manager->addNotification(array($usernotif), $notif, true);
              } elseif ($langue=="de") {
                  $notif = $this->manager->createNotification('Löschen der Kommentar !');
                  $notif->setMessage('neuer Kommentar zu den Artikeln'.$topic->getTitre().'ist gelöscht');
                  $notif->setLink('https://Youdev-it.com/');
                  // or the one-line method :
                  // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                  // you can add a notification to a list of entities
                  // the third parameter `$flush` allows you to directly flush the entities
                  $this->manager->addNotification(array($usernotif), $notif, true);
              } else {
                  $notif = $this->manager->createNotification('Message Deleted!');
                  $notif->setMessage('a message associated to the post  '.$topic->getTitre().' has been deleted successfully');
                  $notif->setLink('https://Youdev-it.com/');
                  // or the one-line method :
                  // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                  // you can add a notification to a list of entities
                  // the third parameter `$flush` allows you to directly flush the entities
                  $this->manager->addNotification(array($usernotif), $notif, true);
              }
              return $this->redirectToRoute('cms_view_messages_topic', [
          'id'=>$idt,
          'ids'=>$ids,
      ]);
          } else {
              return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
             
              'topic' => $topic,
              'idc'=>$id,
              'site'=>$site,
              'messages'=>$messages,
          ));
          }
      }
      else {
          if ($request->getClientIp() == $message->getIp()) {
              $em->remove($message);
              $em->flush();
              return $this->redirectToRoute('cms_view_messages_topic', [
          'id'=>$idt,
          'ids'=>$ids,
      ]);
          } else {
              return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
             
              'topic' => $topic,
              'idc'=>$id,
              'site'=>$site,
              'messages'=>$messages,
          ));
          }
          
      }
  }

  public function UpdateMessageForm($id,$idt,$ids,MessagesTopicRepository $messagerep,SiteRepository $siterep,PaginatorInterface $paginator,Request $request): Response
    {
        $em=$this->getDoctrine()->getManager();
        $message=$this->getDoctrine()->getRepository(MessagesTopic::class)->find($id); 
        $topic=$this->getDoctrine()->getRepository(Topic::class)->find($idt);
        $user=$this->getUser();
        $site=$siterep->find($ids);
        $pagination=$messagerep->findByTopics($topic);
        $messages = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        /*
        $pagination1=$this->getDoctrine()->getRepository(Categories::class)->findAll();
        $cat = $paginator->paginate(
         $pagination1, 
         $request->query->getInt('page', 1), 
         6 
     ); 
     */
     if ($user!=null) {
         if (($user == $message->getUsers()) or $user->hasRole('ROLE_ADMIN')) {
             if ($site->getTheme()=="default" || $site->getTheme()=="") {
                 return $this->render('frontend/fontsite/view_messages_front.html.twig', array(
               
                'topic' => $topic,
                'idc'=>$id,
                'site'=>$site,
                'list_messages'=>$messages,
            ));
             } elseif ($site->getTheme()=="template1") {
                 return $this->render('frontend/fontsite/template1/view_messages_front.html.twig', array(
               
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
            ));
             } elseif ($site->getTheme()=="template2") {
                 return $this->render('frontend/fontsite/template2/view_messages_front.html.twig', array(
               
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
            ));
             } elseif ($site->getTheme()=="template3") {
                 return $this->render('frontend/fontsite/template3/view_messages_front.html.twig', array(
               
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
            ));
             } elseif ($site->getTheme()=="template4") {
                 return $this->render('frontend/fontsite/template4/view_messages_front.html.twig', array(
               
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
            ));
             }
         } else {
             return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
               
                'topic' => $topic,
                'idc'=>$id,
                'site'=>$site,
                'list_messages'=>$messages,
            ));
         }
     }
    else
    {
        if (($request->getClientIp()== $message->getIp())) {
            if ($site->getTheme()=="default" || $site->getTheme()=="") {
                return $this->render('frontend/fontsite/view_messages_front.html.twig', array(
              
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
           ));
            } elseif ($site->getTheme()=="template1") {
                return $this->render('frontend/fontsite/template1/view_messages_front.html.twig', array(
              
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
           ));
            } elseif ($site->getTheme()=="template2") {
                return $this->render('frontend/fontsite/template2/view_messages_front.html.twig', array(
              
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
           ));
            } elseif ($site->getTheme()=="template3") {
                return $this->render('frontend/fontsite/template3/view_messages_front.html.twig', array(
              
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
           ));
            } elseif ($site->getTheme()=="template4") {
                return $this->render('frontend/fontsite/template4/view_messages_front.html.twig', array(
              
                    'topic' => $topic,
                    'idc'=>$id,
                    'site'=>$site,
                    'list_messages'=>$messages,
           ));
            }
        } else {
            return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
              
               'topic' => $topic,
               'idc'=>$id,
               'site'=>$site,
               'list_messages'=>$messages,
           ));
        } 
    }

    }




    public function  UpdateMessage(\Symfony\Component\HttpFoundation\Request $request,$id,$idt,$ids):Response
    {
        $topic=$this->getDoctrine()->getRepository(Topic::class)->find($idt);
        $message=$this->getDoctrine()->getRepository(MessagesTopic::class)->find($id);
        $site=$this->getDoctrine()->getRepository(Site::class)->find($ids);

       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
         
          
             $date = new \DateTime('now');
        
            $message->setUpdatedAt($date);
            $message->setContenu($_POST['contenu']);
         
            $em->flush();
            
            return $this->redirectToRoute('cms_view_messages_topic',[
                'id'=>$idt,
                'ids'=>$ids
            ]);

     
        
    }

    public function ViewMessages($id,TopicRepository $topicrep){
        $em=$this->getDoctrine()->getManager();
        $topic=$topicrep->find($id);
        return $this->render('topic/view_messages.html.twig',[
            'messages'=>$topic->getMessagesTopics(),
        ]);
    }

    public function DeleteMessagess($id, MessagesTopicRepository $sr,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $message =$sr->find($id);
                    $em->remove($message);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting message!');
                    $notif->setMessage('deleting message by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_view_messages_for_topic', array(
                    'id'=> $message->getTopics()->getId(),
                ));
                } else {
                    //$userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a message for topic');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userinvit), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a message for topic');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userinvit), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $message =$sr->find($id);
         
   
            if (($this->getUser()->getUsername() == $message->getTopics()->getCreatedby()) || ($this->getUser()->hasRole('ROLE_ADMIN'))) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($message);
                $em->flush();
                return $this->redirectToRoute('cms_view_messages_for_topic', array(
                    'id'=> $message->getTopics()->getId(),
                ));
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a message for topic');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($this->getUser()), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    }




}