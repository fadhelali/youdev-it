<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\PageService;
use App\Entity\Site;
use App\Form\PageServiceType;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\PageServiceRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/pageservice")
 */
class PageServiceController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_pageservice_index")
     */
    public function index(PageServiceRepository  $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
    $page_service=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('page_service/index.html.twig', [
            'list_pageservice' =>$page_service,
        ]);
    }

    /**
     * @Route("/new/serviceinvit/{idpage}/newservice/{idsite}", name="cms_pageservice_new", methods={"GET","POST"})
     */
    public function new(UserRepository $userrep,Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator): Response
    {
        $pageservice = new PageService();
        $form = $this->createForm(PageServiceType::class, $pageservice);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $pageservice->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $pageservice->setCreatedAt($date);
                $pageservice->setUpdatedAt($date);
           

                $entityManager->persist($pageservice);
                $entityManager->flush();
                $message=$translator->trans('New Block service added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'page_service',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_pageservice_index');
            }

            return $this->render('page_service/new.html.twig', [
            'pageservice' => $pageservice,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $pageservice->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $pageservice->setCreatedAt($date);
                        $pageservice->setUpdatedAt($date);
                        $pageservice->setPages($page);
                        $page->addPageService($pageservice);
                   
        
                        $entityManager->persist($pageservice);
                        $entityManager->flush();
                        $message=$translator->trans('New Block service added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'page_service',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block service for page!');
                        $notif->setMessage('adding block servicefor page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_service', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add a service for page  block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a service for page  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('page_service/new.html.twig', [
            'pageservice' => $pageservice,
            'form' => $form->createView(),

            ]);
    }
    


    /**
     * @Route("/{id}/update/{idpage}/updateservice_block/{idsite}/up", name="cms_pageservice_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,TranslatorInterface $translator,PageServiceRepository $sr, $id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $pageservice =$sr->find($id);
        $form = $this->createForm(PageServiceType::class, $pageservice);
        $form->handleRequest($request);

        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $pageservice->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('Block service updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'page_service',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_pageservice_index');
            }

            return $this->render('page_service/update.html.twig', [
            'pageservice' => $pageservice,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $pageservice->setUpdatedAt($date);
                        $pageservice->setPages($page);
                        $page->addPageService($pageservice);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans('Block service updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'page_service',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('editing block service for page!');
                        $notif->setMessage('editing block servicefor page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_service', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to edit a service for page  block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit a service for page  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('page_service/update.html.twig', [
            'pageservice' => $pageservice,
            'form' => $form->createView(),

            ]);
    }
    /**
     * @Route("/{id}/delete/{idpage}/deleteinvit_blockservice/{idsite}/delete", name="cms_pageservice_delete")
     */
    
    public function delete($id, PageServiceRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $pageservice =$sr->find($id);
                    $em->remove($pageservice);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('deleting block service for page!');
                        $notif->setMessage('deleting block servicefor page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_service', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete a service for page  block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete a service for page  block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $pageservice =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$pageservice->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($pageservice);
                $em->flush();
                return $this->redirectToRoute('cms_pageservice_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete a service for page  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }




    public function ViewServiceInvitPage($id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,UserRepository $userrep,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $services=$page->getPageServices();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewservices.html.twig', array(
                   
          
            'list_pageservice' => $services,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access lists services for page  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access lists services for page  block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
}