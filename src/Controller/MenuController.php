<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Menu;
use App\Entity\Page;
use App\Entity\User;
use App\Entity\Site;
use App\Entity\SubItem;
use App\Form\MenuType;
use App\Repository\ItemRepository;
use App\Repository\MenuRepository;
use App\Repository\SubItemRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/menu")
 */
class MenuController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }



    /**
     * @Route("/", name="cms_menu_index", methods={"GET"})
     */
    public function index(MenuRepository $menuRepository): Response
    {

        $user=$this->getUser();
                $em=$this->getDoctrine()->getManager();
                $menu=$menuRepository->findByCreatedby($user->getUsername()); 
                 
        return $this->render('menu/index.html.twig', [
            'menus' => $menu,
        ]);
    }

    /**
     * @Route("/new/menublock/{idsite}/new", name="cms_menu_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);


        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $menu->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $menu->setCreatedAt($date);
                $menu->setUpdatedAt($date);
              
                $menu->setActive(false);
                $menus=$this->getDoctrine()->getRepository(Menu::class)->findAll();
                foreach($menus as $ft){
                    if($ft->getSites()->getName()==$menu->getSites()->getName()){
                        $entityManager->remove($ft); 
                    }
                }


                $entityManager->persist($menu);
                $entityManager->flush();
                $message=$translator->trans('New menu has been added successfully');

                $this->get('session')->getFlashBag()->add(
                    'menu',
                    array(
                       
                        'message' => $message
                    )
                );
                return $this->redirectToRoute('cms_menu_index');
            }

            return $this->render('menu/new.html.twig', [
          'menu' => $menu,
          'form' => $form->createView(),
      ]);
        }
        elseif($idsite!="null"){

          $user=$this->getUser();
      $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
      $userinvit=$ui->findByCreatedby($site->getAuthor());
      $test= false;
      $tt=null;
      foreach($userinvit as $usr){
          if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
              $test=true;
              $tt=$usr;
          }
      }

      if ($test==true) {
          if (($tt->hasRoles('ROLE_MODERATOR'))) {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
                  $menu->setCreatedby($user->getUsername());
                  $date = new \DateTime('now');
                  $menu->setCreatedAt($date);

                  $menu->setUpdatedAt($date);
                  $menus=$this->getDoctrine()->getRepository(Menu::class)->findAll();
                 

                  foreach($menus as $ft){
                      if ($ft->getSites() !=null) {
                          if ($ft->getSites()->getName()==$menu->getSites()->getName()) {
                              $entityManager->remove($ft);
                          }
                      }
                  }
                  // On boucle sur les images
               
                  $site->addMenu($menu);
                  $menu->setSites($site);
                 

                  $entityManager->persist($menu);
                  $entityManager->flush();
                  $message=$translator->trans('New menu has been added successfully');

                  $this->get('session')->getFlashBag()->add(
                    'menu',
                    array(
                       
                        'message' => $message
                    )
                );
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('Adding block menu...!');
                $notif->setMessage('Blocj menu adding by '.$this->getUser().'to your website '. $site->getName());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                  return $this->redirectToRoute('cms_invit_view_Menus', array(
                      'id' => $idsite,
                  ));
              }

          }
          else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add  menu block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                 return $this->render('Invits/page/erreur_permissions.html.twig');

          }
      }
      else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add  menu block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

      }

      return $this->render('menu/new.html.twig', [
          'menu' => $menu,
          'form' => $form->createView(),
      ]);


        }


    }

    /**
     * @Route("/{id}", name="cms_menu_show", methods={"GET"})
     */
    public function show(MenuRepository $menurep,$id): Response
    {
        $em=$this->getDoctrine()->getManager();
        $menu=$menurep->find($id); 
       
        return $this->render('menu/show.html.twig', [
            'menu' => $menu,
        ]);
    }

    /**
     * @Route("/{id}/editblockmenu/{idsite}/edit", name="cms_menu_edit", methods={"GET","POST"})
     */
    public function edit(UserRepository $userrep,TranslatorInterface $translator,Request $request, MenuRepository $menurep,$id,$idsite,UserInvitRepository $ui): Response
    {
        $menu=$menurep->find($id);
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $menu->setUpdatedAt($date);
              
               
               


                $entityManager->flush();
                $message=$translator->trans('menu has been updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'menu',
                    array(
                       
                        'message' => $message
                    )
                );
                return $this->redirectToRoute('cms_menu_index');
            }

            return $this->render('menu/edit.html.twig', [
          'menu' => $menu,
          'form' => $form->createView(),
      ]);
        } elseif ($idsite!="null") {
            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }

            if ($test==true) {
                if (($tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
           
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $menu->setUpdatedAt($date);
                        $site->addMenu($menu);
                        $menu->setSites($site);

                        // On boucle sur les images
               
                  
                 

                        $entityManager->flush();
                        $message=$translator->trans('menu has been updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'menu',
                            array(
                       
                        'message' => $message
                    )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('updating block menu...!');
                        $notif->setMessage('block menu for your website'. $site->getName().'has been updated by  '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_Menus', array(
                      'id' => $idsite,
                  ));
                    }
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to update  menu block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to update  menu block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }

            return $this->render('menu/edit.html.twig', [
          'menu' => $menu,
          'form' => $form->createView(),
      ]);
        }
    }

    /**
     * @Route("/{id}/deleteblockmenu/{idsite}/delete", name="cms_menu_delete")
     */
    public function delete($id,MenuRepository $menurep,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {   if ($idsite !="null") {
        $em=$this->getDoctrine()->getManager();

        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach ($userinvit as $usr) {
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                $test=true;
                $tt=$usr;
            }
        }
        if ($test==true) {
            if (($tt->hasRoles('ROLE_MODERATOR'))) {
                $menu =$menurep->find($id);
                $em->remove($menu);
                $em->flush();
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('deleting block menu...!');
                $notif->setMessage('deleting block menu for  website'. $site->getName().' by  '.$this->getUser());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->redirectToRoute('cms_invit_view_Menus', array(
                'id' => $idsite,
            ));
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete  menu block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete  menu block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    } else {
        $menu =$menurep->find($id);
     

        if (($this->getUser()->getUsername()==$menu->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
            $em=$this->getDoctrine()->getManager();

            $em->remove($menu);
            $em->flush();
            return $this->redirectToRoute('cms_menu_index');
        }
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete  menu block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

    }
    }
     /**
     * @Route("/{id}/check_active", name="cms_menu_active")
     */
    public function CheckMenu($id,MenuRepository $menurep): Response
    {  $em=$this->getDoctrine()->getManager();
        $menu =$menurep->find($id);
        if($menu->getActive()==true) 
          $menu->setActive(false);
          else
          $menu->setActive(true);

        $em->flush();
        return $this->redirectToRoute('cms_menu_index');
    }


    public function ShowItem($id,MenuRepository $menurep): Response
    {  $em=$this->getDoctrine()->getManager();
        $menu =$menurep->find($id);
        return $this->render('menu/show_item.html.twig', [
            'menu' => $menu,
        ]);
    }
    public function AddItem(UserRepository $userrep,TranslatorInterface $translator,$id,MenuRepository $menurep,$idsite,UserInvitRepository $ui): Response
    {  $Item = new Item();
        $menu=$menurep->find($id);
        if ($idsite=="null") {
            $em = $this->getDoctrine()->getManager();
            $Item->setName($_POST['name']);
            $url=$_POST['t'];
            if ($url != "") {
                $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                $Item->setUrlPage($page);
            } elseif ($_POST['urlitem'] != "") {
                $Item->setUrl($_POST['urlitem']);
            } else {
                $Item->setUrl("www.youdev-it.com");
            }

            $Item->setMenusItem($menu);
            $menu->addItem($Item);
            $em->persist($Item);
            $em->flush();
        
            $this->get('session')->getFlashBag()->add(
                'item',
                array(
                    
                        'message' => 'New item has been added successfully.'
                    )
            );
            return $this->redirectToRoute('cms_menu_show_item', [
                        'id'=>$id,
                    ]);
        }

                    elseif($idsite !="null"){
                        $em=$this->getDoctrine()->getManager();

                        $user=$this->getUser();
                        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
                        $userinvit=$ui->findByCreatedby($site->getAuthor());
                        $test= false;
                        $tt=null;
                        foreach ($userinvit as $usr) {
                            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                                $test=true;
                                $tt=$usr;
                            }
                        }
                        if ($test==true) {
                            if (($tt->hasRoles('ROLE_MODERATOR'))) {
                                $em = $this->getDoctrine()->getManager();
                                $Item->setName($_POST['name']);
                                $url=$_POST['t'];
                                if ($url != "") {
                                    $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                                    $Item->setUrlPage($page);
                                } elseif ($_POST['urlitem'] != "") {
                                    $Item->setUrl($_POST['urlitem']);
                                } else {
                                    $Item->setUrl("www.youdev-it.com");
                                }
                    
                                $Item->setMenusItem($menu);
                                $menu->addItem($Item);

                                $em->persist($Item);
                                $em->flush();
                                $message=$translator->trans('New item has been added successfully');

                                $this->get('session')->getFlashBag()->add(
                                    'item',
                                    array(
                                        
                                            'message' => $message
                                        )
                                );
                                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                                $notif = $this->manager->createNotification('Adding item for  block menu...!');
                                $notif->setMessage('adding item for menu to your website'. $site->getName().' by  '.$this->getUser());
                                $notif->setLink('https://Youdev-it.com/');
                               
                                $this->manager->addNotification(array($userrecivernot), $notif, true);
                                return $this->redirectToRoute('cms_menu_show_itemblock', [
                                            'id'=>$id,
                                            'idsite'=>$idsite,
                                        ]);
                            }
                            else{
                                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                                $notif = $this->manager->createNotification('OOps!');
                                $notif->setMessage('unauthorized user wants to add a item for   menu block for your website '.$site->getName());
                                $notif->setLink('https://Youdev-it.com/');
                                // or the one-line method :
                                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                        
                                // you can add a notification to a list of entities
                                // the third parameter `$flush` allows you to directly flush the entities
                                $this->manager->addNotification(array($userrecivernot), $notif, true);
                                return $this->render('Invits/page/erreur_permissions.html.twig');

                            }
                            
                            
                            }
                            else {
                                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                                $notif = $this->manager->createNotification('OOps!');
                                $notif->setMessage('unauthorized user wants to add a item for   menu block for your website '.$site->getName());
                                $notif->setLink('https://Youdev-it.com/');
                                // or the one-line method :
                                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                        
                                // you can add a notification to a list of entities
                                // the third parameter `$flush` allows you to directly flush the entities
                                $this->manager->addNotification(array($userrecivernot), $notif, true);
                                return $this->render('Invits/page/erreur_permissions.html.twig');

                            }

                    }
                    else {
                        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('OOps!');
                        $notif->setMessage('unauthorized user wants to add a item for   menu block for your website '.$site->getName());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->render('Invits/page/erreur_permissions.html.twig');

                    }

 
    }



    public function EditItem(UserRepository $userrep,$id,$idi,MenuRepository $menurep,$idsite,UserInvitRepository $ui,TranslatorInterface $translator): Response
    {  $Item =$this->getDoctrine()->getRepository(Item::class)->find($idi);
        $menu=$menurep->find($id);
       if ($idsite=="null") {
           $em = $this->getDoctrine()->getManager();
           $Item->setName($_POST['name']);
           $url=$_POST['t'];


           if ($Item->getUrlPage()!= null) {
               if ($_POST['urlitem'] != "") {
                   $Item->setUrl($_POST['urlitem']);
                   $Item->setUrlPage(null);
               } elseif ($url !="") {
                   if ($Item->getUrl()!="") {
                       $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                       $Item->setUrlPage($page);
                       $Item->setUrl("");
                   } else {
                       $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                       $Item->setUrlPage($page);
                   }
               } else {
                   $Item->setUrl("www.youdev-it.com");
                   $Item->setUrlPage(null);
               }
           } else {
               if ($_POST['urlitem'] != "") {
                   $Item->setUrl($_POST['urlitem']);
               } elseif ($url !="") {
                   if ($Item->getUrl()!="") {
                       $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                       $Item->setUrlPage($page);
                       $Item->setUrl("");
                   } else {
                       $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                       $Item->setUrlPage($page);
                   }
               } else {
                   $Item->setUrl("www.youdev-it.com");
                   $Item->setUrlPage(null);
               }
           }

           $Item->setMenusItem($menu);
           $menu->addItem($Item);

           $em->flush();
           $message=$translator->trans('New item has been added successfully');

           $this->get('session')->getFlashBag()->add(
               'item',
               array(
                    
                        'message' => $message
                    )
           );
           return $this->redirectToRoute('cms_menu_show_item', [
                        'id'=>$id,
                    ]);
       }


       else if($idsite !="null"){

        $em=$this->getDoctrine()->getManager();

        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach ($userinvit as $usr) {
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                $test=true;
                $tt=$usr;
            }
        }
        if ($test==true) {
            if (($tt->hasRoles('ROLE_MODERATOR'))) {
                $em = $this->getDoctrine()->getManager();
                $Item->setName($_POST['name']);
                $url=$_POST['t'];
     
     
                if ($Item->getUrlPage()!= null) {
                    if ($_POST['urlitem'] != "") {
                        $Item->setUrl($_POST['urlitem']);
                        $Item->setUrlPage(null);
                    } elseif ($url !="") {
                        if ($Item->getUrl()!="") {
                            $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                            $Item->setUrlPage($page);
                            $Item->setUrl("");
                        } else {
                            $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                            $Item->setUrlPage($page);
                        }
                    } else {
                        $Item->setUrl("www.youdev-it.com");
                        $Item->setUrlPage(null);
                    }
                } else {
                    if ($_POST['urlitem'] != "") {
                        $Item->setUrl($_POST['urlitem']);
                    } elseif ($url !="") {
                        if ($Item->getUrl()!="") {
                            $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                            $Item->setUrlPage($page);
                            $Item->setUrl("");
                        } else {
                            $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                            $Item->setUrlPage($page);
                        }
                    } else {
                        $Item->setUrl("www.youdev-it.com");
                        $Item->setUrlPage(null);
                    }
                }
     
                $Item->setMenusItem($menu);
                $menu->addItem($Item);

                $em->flush();
                $message=$translator->trans('New item has been added successfully');

                $this->get('session')->getFlashBag()->add(
                    'item',
                    array(
                         
                             'message' =>$message
                         )
                );
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('editing item for  block menu...!');
                $notif->setMessage('editing item for block menu for your website'. $site->getName().'by '.$this->getUser());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->redirectToRoute('cms_menu_show_itemblock', [
                             'id'=>$id,
                             'idsite'=>$idsite,
                         ]);
            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to edit a item for   menu block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }
            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to edit a item for   menu block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }





       }
       else{
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to edit a item for   menu block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
   

 
    }

    public function DeleteItem(UserRepository $userrep,$id,$idm,ItemRepository $itemrep,$idsite,UserInvitRepository $ui): Response
    {  
        
        
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();
    
            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_MODERATOR'))) {
                    $item =$itemrep->find($id);
                    $em->remove($item);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting item for  block menu...!');
                    $notif->setMessage('deleting item for block menu for your website'. $site->getName().'by  '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_menu_show_itemblock', [
                        'id'=>$idm,
                        'idsite'=>$idsite,
                    ]);
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete a item for   menu block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete a item for   menu block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $item =$itemrep->find($id);
         
    
            if (($this->getUser()->getUsername()==$item->getMenusItem()->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();
    
                $em->remove($item);
                $em->flush();
                return $this->redirectToRoute('cms_menu_show_item',[
                    'id'=>$idm,
                ]);
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete a item for   menu block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
    
        }   
        
        
        
        
        
        /*
        $em=$this->getDoctrine()->getManager();
        $item =$itemrep->find($id); 
        $em->remove($item);
        $em->flush();
        return $this->redirectToRoute('cms_menu_show_item',[
            'id'=>$idm,
        ]);
*/
    }

// crud for the subitem entity............

    public function ShowSubItem($id,ItemRepository $itemrep): Response
    {  $em=$this->getDoctrine()->getManager();
        $item =$itemrep->find($id); 
        return $this->render('menu/show_subitem.html.twig', [
            'item' => $item,
        ]);

    }



    public function AddSubItem($id,ItemRepository $itemrep,TranslatorInterface $translator,UserRepository $userrep): Response
    {  $subitem = new SubItem();
        $item=$itemrep->find($id);
       
        $em = $this->getDoctrine()->getManager();
       $subitem->setName($_POST['name']);
      $url=$_POST['t'];
     if($url != "")
     {
         $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
         $subitem->setUrlPage($page);

     }
     else if($_POST['urlitem'] != ""){
         $subitem->setUrl($_POST['urlitem']);
     }
     else{
     $subitem->setUrl("www.youdev-it.com");

     }

       $subitem->setItems($item);
    
        $em->persist($subitem);
        $em->flush();
        $message=$translator->trans('New subitem has been added successfully');

        $this->get('session')->getFlashBag()->add(
                    'subitem',
                    array(
                    
                        'message' => $message
                    )
                    );
                    return $this->redirectToRoute('cms_menu_show_subitems',[
                        'id'=>$id,
                    ]);
   

 
    }




    public function UpdateSubItem($id,$idi,SubItemRepository $subitem,TranslatorInterface $translator): Response
    {  $Item =$this->getDoctrine()->getRepository(Item::class)->find($idi);
        $subitem=$subitem->find($id);
       
        $em = $this->getDoctrine()->getManager();
       $subitem->setName($_POST['name']);
      $url=$_POST['t'];


      if ($subitem->getUrlPage()!= null) {
          if ($_POST['urlitem'] != "") {
              $subitem->setUrl($_POST['urlitem']);
              $subitem->setUrlPage(null);

          } elseif ($url !="") {
            if ($Item->getUrl()!="") {
                $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                $subitem->setUrlPage($page);
                $subitem->setUrl("");
            }else {
                $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                $subitem->setUrlPage($page);
            }
          } else {
              $subitem->setUrl("www.youdev-it.com");
              $subitem->setUrlPage(null);

          }
      }
    else {
        if ($_POST['urlitem'] != "") {
            $subitem->setUrl($_POST['urlitem']);
        } elseif ($url !="") {
            if ($subitem->getUrl()!="") {
                $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                $subitem->setUrlPage($page);
                $subitem->setUrl("");
            }else {
                $page=$this->getDoctrine()->getRepository(Page::class)->find($url);
                $subitem->setUrlPage($page);   
            }

        } else {
            $subitem->setUrl("www.youdev-it.com");
            $subitem->setUrlPage(null);

        }


     }

     $subitem->setItems($Item);
    
        $em->flush();
        $message=$translator->trans('New subitem has been added successfully');

        $this->get('session')->getFlashBag()->add(
                    'subitem',
                    array(
                    
                        'message' => $message
                    )
                    );
                    return $this->redirectToRoute('cms_menu_show_subitems',[
                        'id'=>$idi,
                    ]);
   

 
    }


    public function DeleteSubItem($id,$idi,SubItemRepository $subrep): Response
    {  $em=$this->getDoctrine()->getManager();
        $subitem =$subrep->find($id); 
        $em->remove($subitem);
        $em->flush();
        return $this->redirectToRoute('cms_menu_show_subitems',[
            'id'=>$idi,
        ]);

    }




    public function ShowItemBlock(UserRepository $userrep,MenuRepository $mn,$id,$idsite,UserInvitRepository $ui):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $menu=$this->getDoctrine()->getRepository(Menu::class)->find($id);

        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/viewblockitem.html.twig', array(
                   
          
            'menu' => $menu,
            'idsite'=>$idsite,
            
        ));
           }
           else{
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }
}
