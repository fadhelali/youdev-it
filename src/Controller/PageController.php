<?php

namespace App\Controller;
use App\Entity\Page;

use App\Entity\FAQ;
use App\Entity\Site;
use App\Repository\FAQRepository;
use App\Repository\PageRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

class PageController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    

    public function ListPage(PageRepository $pageRepository): Response
    {
        $em=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
       $page =$pageRepository->findByCreatedby($username); 
    return $this->render('page/list_page.html.twig',array('page'=>$page));
    
    }
    public function  AddPage(\Symfony\Component\HttpFoundation\Request $request,$idsite,UserRepository $userrep,TranslatorInterface $translator):Response
    {
        $page = new Page();
        $form = $this->createForm('App\Form\PageType',$page);
        $form->handleRequest($request);
    if ($idsite=="null") {
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
            $page->setCreatedBy($user->getUsername());
            $date = new \DateTime('now');
            $page->setCreatedAt($date);
            $page->setUpdatedAt($date);
            $page->setPublished(false);
   
      
            $em->persist($page);
            $em->flush();
            $message=$translator->trans('New subitem has been added successfully');

            $this->get('session')->getFlashBag()->add(
                'page',
                array(
       
        'message' => $message
    )
            );
            return $this->redirectToRoute('cms_page_list');
        }
    }
    else {
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
           $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $em = $this->getDoctrine()->getManager();
            $page->setCreatedBy($user->getUsername());
            $date = new \DateTime('now');
            $page->setCreatedAt($date);
            $page->setUpdatedAt($date);
            $page->setPublished(false);
            $site->addPage($page);

      
            $em->persist($page);
            $em->flush();
            $message=$translator->trans('New page has been added successfully');

            $this->get('session')->getFlashBag()->add(
                'page',
                array(
       
        'message' => $message
    )
            );
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('Adding page!');
            $notif->setMessage('adding page for  website named'.$site->getName().' by '.$this->getUser());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->redirectToRoute('cms_page_list');
        }
    }
        return $this->render('page/add_page.html.twig', array(
           
            'form' => $form->createView(),
        ));
    }

    public function UpdatePage(UserRepository $userrep,Request $request,PageRepository $pageRepository,$id,TranslatorInterface $translator): Response
    { $page = $pageRepository->find($id);
        $form = $this->createForm('App\Form\PageType',$page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
           
      
             $date = new \DateTime('now');
     
            $page->setUpdatedAt($date);
            
   
      
           
            $em->flush();
            $message=$translator->trans('page has been  updated');

            $this->get('session')->getFlashBag()->add(
    'page',
    array(
       
        'message' => $message
    )
);
            return $this->redirectToRoute('cms_page_list');
        }

        return $this->render('page/Modifier_page.html.twig', array(
           
            'form' => $form->createView(),
        ));
    
    }

    public function DeletePage(PageRepository $pageRepository,$id): Response
    {
                $em=$this->getDoctrine()->getManager();
                $page =$pageRepository->find($id); 
                $em->remove($page);
                $em->flush();
                return $this->redirect($this->generateUrl('cms_page_list'));    
    }

    // the crud of FAQ entity

    public function ListFaq(FAQRepository $faqRepository): Response
    {
        $em=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
       $faq =$faqRepository->findByCreatedby($username); 
    return $this->render('page/list_faq.html.twig',array('faq'=>$faq));
    
    }
    public function  AddFaq(\Symfony\Component\HttpFoundation\Request $request,TranslatorInterface $translator):Response
    {
        $FAQ = new FAQ();
        $form = $this->createForm('App\Form\FAQType',$FAQ);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $FAQ->setCreatedBy($user->getUsername());
             $date = new \DateTime('now');
             $FAQ->setCreatedAt($date);
             $FAQ->setUpdatedAt($date);
             $FAQ->setPublished(false);
   
      
            $em->persist($FAQ);
            $em->flush();
            $message=$translator->trans('New Frequently Asked Question has been added successfully');

            $this->get('session')->getFlashBag()->add(
    'faq',
    array(
       
        'message' =>$message
    )
);
            return $this->redirectToRoute('cms_faq_list');
        }
        return $this->render('page/add_faq.html.twig', array(
           
            'form' => $form->createView(),
        ));
    }
    public function UpdateFaq(Request $request,FAQRepository $faqRepository,$id,TranslatorInterface $translator): Response
    { $faq = $faqRepository->find($id);
        $form = $this->createForm('App\Form\FAQType',$faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
           
      
             $date = new \DateTime('now');
     
            $faq->setUpdatedAt($date);
            
   
      
           
            $em->flush();
            $message=$translator->trans('FAQ has been updated successfully');

            $this->get('session')->getFlashBag()->add(
    'faq',
    array(
       
        'message' => $message
    )
);
            return $this->redirectToRoute('cms_faq_list');
        }

        return $this->render('page/Modifier_faq.html.twig', array(
           
            'form' => $form->createView(),
        ));
    
    }

    public function DeleteFaq(FAQRepository $FAQRepository,$id): Response
    {
                $em=$this->getDoctrine()->getManager();
                $faq=$FAQRepository->find($id); 
                $em->remove($faq);
                $em->flush();
                return $this->redirect($this->generateUrl('cms_faq_list'));    
    }
    public function ShowFaq(FAQRepository $FAQRepository,$id,PaginatorInterface $paginator,Request $request): Response
    {
                $em=$this->getDoctrine()->getManager();
                $user = $this->getUser();
                $pagination=$FAQRepository->findByCreatedby($user->getUsername()); 
                $faq = $paginator->paginate(
                    $pagination, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    5 /*limit per page*/
                );
                
        return $this->render('page/show_faq.html.twig', array(
           
            'faqs' => $faq,
        ));  
    }
    public function PubFaq(FAQRepository $FAQRepository,$id): Response
        {
                    $em=$this->getDoctrine()->getManager();
                    $faq=$FAQRepository->find($id);
                    if($faq->getPublished()==true)
                    $faq->setPublished(false);
                    else
                    $faq->setPublished(true);
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('cms_faq_list'));
    }
   
    public function PubPage(PageRepository $pageRepository,$id): Response
    {
                    $em=$this->getDoctrine()->getManager();
                    $page=$pageRepository->find($id);
                    if($page->getPublished()==true)
                    $page->setPublished(false);
                    else
                    $page->setPublished(true);
                    $em->flush();

                    return $this->redirect($this->generateUrl('cms_page_list'));
    }
   
}
