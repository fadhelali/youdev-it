<?php

namespace App\Controller;
use App\Repository\UserRepository;

use App\Entity\Feature;
use App\Entity\Page;
use App\Entity\Site;
use App\Form\FeatureType;
use App\Repository\FeatureRepository;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/feature")
 */
class FeatureController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_feature_index")
     */
    public function index(FeatureRepository $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $features=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('feature/index.html.twig', [
            'list_feature' =>$features,
        ]);
    }

    /**
     * @Route("/new/featureblock/{idpage}/new/{idsite}", name="cms_feature_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {

        $Feature = new Feature();
        $form = $this->createForm(FeatureType::class, $Feature);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $Feature->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $Feature->setCreatedAt($date);
                $Feature->setUpdatedAt($date);
           

                $entityManager->persist($Feature);
                $entityManager->flush();
                $message=$translator->trans('New Block feature added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'feature',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_feature_index');
            }

            return $this->render('feature/new.html.twig', [
            'feature' => $Feature,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $Feature->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $Feature->setCreatedAt($date);
                        $Feature->setUpdatedAt($date);
                        $Feature->setPages($page);
                        $page->addFeature($Feature);
                   
        
                        $entityManager->persist($Feature);
                        $entityManager->flush();
                        $message=$translator->trans('New Block feature added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'feature',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block Feature!');
                        $notif->setMessage('Adding new block feature by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_feature', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to add new  feature block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add new  feature block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('feature/new.html.twig', [
            'feature' => $Feature,
            'form' => $form->createView(),

            ]);

















    }
    


    /**
     * @Route("/{id}/update/featureblock/{idpage}/update/{idsite}", name="cms_feature_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,FeatureRepository $sr, TranslatorInterface $translator,$id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $Feature =$sr->find($id);
        $form = $this->createForm(FeatureType::class, $Feature);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $Feature->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('Block feature updated successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'feature',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_feature_index');
            }

            return $this->render('feature/update.html.twig', [
            'feature' => $Feature,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $Feature->setUpdatedAt($date);
                        $Feature->setPages($page);
                        $page->addFeature($Feature);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans('Block feature updated successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'feature',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('updating block Feature!');
                        $notif->setMessage('Your block feature named '. $Feature->getTitre() .'has been updated by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_feature', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to update your feature block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to update your feature block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('feature/update.html.twig', [
            'feature' => $Feature,
            'form' => $form->createView(),

            ]);
    }
    /**
     * @Route("/{id}/delete/featureblock/{idpage}/delete/{idsite}", name="cms_feature_delete")
     */
    
    public function delete($id, FeatureRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $feature =$sr->find($id);
                    $em->remove($feature);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block Feature!');
                    $notif->setMessage('Your block feature named '. $feature->getTitre() .'has been deleted by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_feature', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete your feature block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete your feature block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $feature =$sr->find($id);
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

   
            if (($this->getUser()->getUsername()==$feature->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($feature);
                $em->flush();
                return $this->redirectToRoute('cms_feature_index');
            }
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete your feature block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }



    public function ViewFeatureInvitPage($id,$idsite,UserInvitRepository $ui, UserRepository $userrep,HeaderRepository $sr,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $features=$page->getFeatures();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewfeature.html.twig', array(
                   
          
            'list_features' => $features,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access your feature block');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$ui->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access your feature block');
        $notif->setLink('https://Youdev-it.com/');
       
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
}