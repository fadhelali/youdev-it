<?php

namespace App\Controller;

use App\Entity\Gallery;
use App\Entity\Images;
use App\Entity\Site;
use App\Form\GalleryType;
use App\Repository\GalleryRepository;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\UserRepository;


/**
 * @Route("/cms/galleryy")
 */
class GalleryController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }



    /**
     * @Route("/", name="cms_gallery_index")
     */
    public function index(GalleryRepository $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $gallerys=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('gallery/index.html.twig', [
            'list_gallery' =>$gallerys,
        ]);
    }

    /**
     * @Route("/new/{idpage}/site/{idsite}", name="cms_gallery_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idpage,$idsite,UserInvitRepository $ui,PageRepository $pr,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $Gallery = new Gallery();
        $form = $this->createForm(GalleryType::class, $Gallery);
        $form->handleRequest($request);
          if ($idpage =="null" && $idsite=="null") {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $Gallery->setCreatedby($user->getUsername());
                  $date = new \DateTime('now');
                  $Gallery->setCreatedAt($date);
                  $Gallery->setUpdatedAt($date);
                  $images = $form->get('images')->getData();
    
                  // On boucle sur les images
                  foreach ($images as $image) {
                      // On génère un nouveau nom de fichier
                      $fichier = md5(uniqid()).'.'.$image->guessExtension();
                
                      // On copie le fichier dans le dossier uploads
                      $image->move(
                          $this->getParameter('images_directory'),
                          $fichier
                      );
                
                      // On crée l'image dans la base de données
                      $img = new Images();
                      $img->setName($fichier);
                      $Gallery->addImage($img);
                  }

                  $entityManager->persist($Gallery);
                  $entityManager->flush();
                  $message=$translator->trans('New Block Gallery added successfully To Your Page');

                  $this->get('session')->getFlashBag()->add(
                      'gallery',
                      array(
                   
                    'message' => $message
                )
                  );
                 
                  return $this->redirectToRoute('cms_gallery_index');
              }

              return $this->render('gallery/new.html.twig', [
            'gallery' => $Gallery,
            'form' => $form->createView(),
        ]);
          }
          elseif($idpage !="null" && $idsite!="null"){

            $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }

        if ($test==true) {
            if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $user = $this->getUser();
             
                    $em = $this->getDoctrine()->getManager();
                    $Gallery->setCreatedby($user->getUsername());
                    $date = new \DateTime('now');
                    $Gallery->setCreatedAt($date);
                    $Gallery->setUpdatedAt($date);
                    $images = $form->get('images')->getData();
      
                    // On boucle sur les images
                    foreach ($images as $image) {
                        // On génère un nouveau nom de fichier
                        $fichier = md5(uniqid()).'.'.$image->guessExtension();
                  
                        // On copie le fichier dans le dossier uploads
                        $image->move(
                            $this->getParameter('images_directory'),
                            $fichier
                        );
                  
                        // On crée l'image dans la base de données
                        $img = new Images();
                        $img->setName($fichier);
                        $Gallery->addImage($img);
                    }
                    $page=$pr->find($idpage);
                    $Gallery->setPages($page);
                    $page->addGallery($Gallery);
  
                    $entityManager->persist($Gallery);
                    $entityManager->flush();
                    $message=$translator->trans('New Block Gallery added successfully To Your Page');

                    $this->get('session')->getFlashBag()->add(
                        'galleryinvit',
                        array(
                     
                      'message' => $message
                  )
                    );
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('Adding block Gallery!');
                    $notif->setMessage('Adding new block Gallery by '.$this->getUser().'for your page witch  website is '. $site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_gallery', array(
                        'id' => $idpage,
                        'idsite'=> $idsite,
                    ));
                }

            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add Gallery block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                   return $this->render('Invits/page/erreur_permissions.html.twig');

            }
        }
        else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add gallery block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
          return $this->render('Invits/page/erreur_permissions.html.twig');

        }

        return $this->render('gallery/new.html.twig', [
            'gallery' => $Gallery,
            'form' => $form->createView(),
        ]);


          }
         
    }
    


    /**
     * @Route("/{id}/update/{idpage}/gallery/{idsite}", name="cms_gallery_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,GalleryRepository $sr,TranslatorInterface $translator ,$id,$idpage,$idsite,UserInvitRepository $ui,PageRepository $pr): Response
    {
        $Gallery =$sr->find($id);
        $form = $this->createForm(GalleryType::class, $Gallery);
        $form->handleRequest($request);

        if ($idpage =="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $Gallery->setUpdatedAt($date);
                $images = $form->get('images')->getData();
  
                // On boucle sur les images
                foreach ($images as $image) {
                    // On génère un nouveau nom de fichier
                    $fichier = md5(uniqid()).'.'.$image->guessExtension();
              
                    // On copie le fichier dans le dossier uploads
                    $image->move(
                        $this->getParameter('images_directory'),
                        $fichier
                    );
              
                    // On crée l'image dans la base de données
                    $img = new Images();
                    $img->setName($fichier);
                    $Gallery->addImage($img);
                }

                $entityManager->flush();
                $message=$translator->trans('Block Gallery updated successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'gallery',
                    array(
                 
                  'message' => $message
              )
                );
                return $this->redirectToRoute('cms_gallery_index');
            }

            return $this->render('gallery/update.html.twig', [
          'gallery' => $Gallery,
          'form' => $form->createView(),
      ]);
        }
        elseif($idpage !="null" && $idsite!="null"){

          $user=$this->getUser();
      $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
      $userinvit=$ui->findByCreatedby($site->getAuthor());
      $test= false;
      $tt=null;
      foreach($userinvit as $usr){
          if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
              $test=true;
              $tt=$usr;
          }
      }

      if ($test==true) {
          if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $date = new \DateTime('now');
                  $Gallery->setUpdatedAt($date);
                  $images = $form->get('images')->getData();
    
                  // On boucle sur les images
                  foreach ($images as $image) {
                      // On génère un nouveau nom de fichier
                      $fichier = md5(uniqid()).'.'.$image->guessExtension();
                
                      // On copie le fichier dans le dossier uploads
                      $image->move(
                          $this->getParameter('images_directory'),
                          $fichier
                      );
                
                      // On crée l'image dans la base de données
                      $img = new Images();
                      $img->setName($fichier);
                      $Gallery->addImage($img);
                  }
                  $page=$pr->find($idpage);
                  $Gallery->setPages($page);
                  $page->addGallery($Gallery);

                  $entityManager->flush();
                  $message=$translator->trans('Block Gallery updated successfully To Your Page');

                  $this->get('session')->getFlashBag()->add(
                      'gallery',
                      array(
                   
                    'message' => $message
                )
                  );
                  $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                  $notif = $this->manager->createNotification('updating block Gallery!');
                  $notif->setMessage('updating block Gallery by '.$this->getUser().'for your page witch website is'. $site->getName());
                  $notif->setLink('https://Youdev-it.com/');
                 
                  $this->manager->addNotification(array($userrecivernot), $notif, true);
                  return $this->redirectToRoute('cms_invit_view_gallery', array(
                      'id' => $idpage,
                      'idsite'=> $idsite,
                  ));
              }

          }
          else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to update gallery block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                 return $this->render('Invits/page/erreur_permissions.html.twig');

          }
      }
      else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to update gallery block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);

        return $this->render('Invits/page/erreur_permissions.html.twig');

      }

      return $this->render('gallery/update.html.twig', [
          'gallery' => $Gallery,
          'form' => $form->createView(),
      ]);


        }
    }
    /**
     * @Route("/{id}/delete/{idpage}/{idsite}", name="cms_gallery_delete")
     */
    
    public function delete($id, GalleryRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();

       
        if ($idpage !="null" && $idsite !="null") {
            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $gallery =$sr->find($id);
                    $em->remove($gallery);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block Gallery...!');
                    $notif->setMessage('deleting  block Gallery by '.$this->getUser().'for your page witch website is '. $site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_gallery', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete gallery block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete gallery block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $gallery=$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$gallery->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($gallery);
                $em->flush();
                return $this->redirectToRoute('cms_gallery_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete gallery block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    }
   /**
    * @Route("/supprime/image/{id}", name="cms_delete_image", methods={"DELETE"})
    */
 public function deleteImage(Images $image, Request $request){
    $data = json_decode($request->getContent(), true);

    // On vérifie si le token est valide
    if($this->isCsrfTokenValid('delete'.$image->getId(), $data['_token'])){
        // On récupère le nom de l'image
        $nom = $image->getName();
        // On supprime le fichier
        unlink($this->getParameter('images_directory').'/'.$nom);

        // On supprime l'entrée de la base
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        // On répond en json
        return new JsonResponse(['success' => 1]);
    }else{
        return new JsonResponse(['error' => 'Token Invalide'], 400);
    }
}



public function ViewGalleryInvitPage(UserRepository $userrep,$id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr): Response
{
    $em=$this->getDoctrine()->getManager();
    $page =$pr->find($id);
    $gallerys=$page->getGalleries();
    $user=$this->getUser();
    $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
    $userinvit=$ui->findByCreatedby($site->getAuthor());
    $test= false;
    $tt=null;
    foreach($userinvit as $usr){
        if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
            $test=true;
            $tt=$usr;
        }
    }
   if ($test==true) {
       if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('accessing block Gallery!');
        $notif->setMessage('User '.$this->getUser()->getUsername(). 'has access to your block Gallery');
        $notif->setLink('https://Youdev-it.com/');
       
        $this->manager->addNotification(array($userrecivernot), $notif, true);
           return $this->render('Invits/page/viewGallery.html.twig', array(
               
      
        'list_gallery' => $gallerys,
        'idsite'=>$idsite,
        'idpage'=>$id,
        
    ));
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access your  footer block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
   }
   else{
    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
    $notif = $this->manager->createNotification('OOps!');
    $notif->setMessage('unauthorized user wants to access your  footer block for your website '.$site->getName());
    $notif->setLink('https://Youdev-it.com/');
    // or the one-line method :
    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

    // you can add a notification to a list of entities
    // the third parameter `$flush` allows you to directly flush the entities
    $this->manager->addNotification(array($userrecivernot), $notif, true);
    return $this->render('Invits/page/erreur_permissions.html.twig');

   }
}
}
