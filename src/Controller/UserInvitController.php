<?php

namespace App\Controller;

use App\Entity\UserInvit;
use App\Form\UserInviType;
use App\Repository\MotsClesRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/invitation_user")
 */
class UserInvitController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_UserInvit_index")
     */
    public function index(UserInvitRepository $ftr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $usersinvit=$ftr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('user_invit/index.html.twig', [
            'list_usersinvit' =>$usersinvit,
        ]);
    }

    /**
     * @Route("/new", name="cms_UserInvit_new", methods={"GET","POST"})
     */
    public function new(Request $request,\Swift_Mailer $mailer,MotsClesRepository $mc,TranslatorInterface $translator): Response
    {
    $Userinvit = new UserInvit();
        $form = $this->createForm(UserInviType::class, $Userinvit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
           
             $em = $this->getDoctrine()->getManager();
             $Userinvit->setCreatedBy($user->getUsername());
             $date = new \DateTime('now');
             $Userinvit->setCreatedAt($date);
             $Userinvit->setUpdatedAt($date);
             $Userinvit->addRole($Userinvit->getRole());
             $message = (new \Swift_Message())
             ->setFrom($user->getEmail())
             ->setSubject('Invitation To manage Site')
             ->setContentType('text/html')
             ->setTo($Userinvit->getReceiverInvit()->getEmail());
           // ->setBody($_POST['message'])
             
             $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
             if ($Userinvit->getRole()=="ROLE_MODERATOR") {
                        $role="Moderator";
                    }
             elseif($Userinvit->getRole()=="ROLE_EDITOR"){
                    $role="Editor";  
                }
             elseif($Userinvit->getRole()=="ROLE_AUTHOR"){
                    $role="AUthor";                           
                }
                $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
                $img1 = $message->embed(\Swift_Image::fromPath('images/ico-facebook.png'));
                $img2 = $message->embed(\Swift_Image::fromPath('images/ico-twitter.png'));
                $img3 = $message->embed(\Swift_Image::fromPath('images/ico-google-plus.png'));
                $img4 = $message->embed(\Swift_Image::fromPath('images/ico-linkedin.png'));
                $img7 = $message->embed(\Swift_Image::fromPath('images/img-07.jpg'));
                $img8 = $message->embed(\Swift_Image::fromPath('images/img-08.jpg'));
             
                $img9 = $message->embed(\Swift_Image::fromPath('images/img-09.jpg'));
                $img10 = $message->embed(\Swift_Image::fromPath('images/img-10.jpg'));
                $img11 = $message->embed(\Swift_Image::fromPath('images/img-11.jpg'));
                $img12 = $message->embed(\Swift_Image::fromPath('images/img-12.jpg'));
                $messagereturn="Invitation Sent To You BY";

        $messageusers ='You have been invited from  '.$user->getUsername().' to menage a web site  named<b> '. $Userinvit->getSites()->getName() .' </b>With a <b>  '. $role .' </b>Role'; 
            $message->setBody($this->render('Emails/contact.html.twig', ['messagereturn' =>$messagereturn,'messagesusers' =>$messageusers,'img' => $img,'messagesfrom' =>$user->getUsername(),'img1' => $img1,'img2' => $img2,'img3' => $img3,'img4' => $img4,'img7' => $img7,'img8' => $img8,'img9' => $img9,'img10' => $img10,'img11' => $img11,'img12' => $img12]));
        ;
        
        $mailer->send($message);
           $motscle=$mc->findByCreatedby($user->getUsername());
           $array=array();
           $array[]=$Userinvit->getReceiverInvit()->getUsername();

           foreach($motscle as $mt){
               $mt->setUseraccess($array);
           }


            $entityManager->persist($Userinvit);
            $entityManager->flush();
            $message=$translator->trans('New Invitation has been sent to user ');

            $this->get('session')->getFlashBag()->add(
                'userinvit',
                array(
                   
                    'message' => $message
                )
            );
            $notif = $this->manager->createNotification('Invitation !');
            $notif->setMessage('You are invited by '.$this->getUser().'to access site with role'.$role);
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($Userinvit->getReceiverInvit()), $notif, true);

            return $this->redirectToRoute('cms_UserInvit_index');
          
        }

        return $this->render('user_invit/new.html.twig', [
            'Userinvit' => $Userinvit,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}/update", name="cms_UserInvit_update", methods={"GET","POST"})
     */
    public function update(Request $request,UserInvitRepository $ftr,$id,TranslatorInterface $translator): Response
    {
    $Userinvit =$ftr->find($id);
        $form = $this->createForm(UserInviType::class, $Userinvit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
          
             $date = new \DateTime('now');
             $Userinvit->addRole($Userinvit->getRole());

            $Userinvit->setUpdatedAt($date);



            $entityManager->persist($Userinvit);
            $entityManager->flush();
            $message=$translator->trans('User Invit informations has been changed successfully ');

            $this->get('session')->getFlashBag()->add(
                'userinvit',
                array(
                   
                    'message' => $message
                )
            );
            return $this->redirectToRoute('cms_UserInvit_index');

        }

        return $this->render('user_invit/update.html.twig', [
            'Userinvit' => $Userinvit,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}/delete", name="cms_UserInvit_delete")
     */
    public function delete($id,UserInvitRepository $ftrrep): Response
    {  $em=$this->getDoctrine()->getManager();
        $userinvit =$ftrrep->find($id); 
        $em->remove($userinvit);
        $em->flush();
        return $this->redirectToRoute('cms_UserInvit_index');
    }

}

