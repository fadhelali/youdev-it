<?php

namespace App\Controller;
use App\Entity\Articles;
use App\Entity\Site;
use App\Entity\User;
use App\Entity\Categories;
use App\Entity\Commentaire;
use App\Entity\MotsCles;
use App\Repository\ArticlesRepository;
use App\Repository\CommentaireRepository;
use App\Repository\MotsClesRepository;
use App\Repository\SiteRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\ObjectType;
use Knp\Component\Pager\PaginatorInterface;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\Argument\ServiceLocator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArticlesController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    public function ListArticle(ArticlesRepository $artrep): Response
    {


        $em=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
       $list_art =$artrep->findByusers($user->getId()); 
        return $this->render('articles/index.html.twig', [
            'list_art' => $list_art,

        ]);
    }
    public function AddArticle(Request $request,TranslatorInterface $translator ,$name,UserRepository $userrep,$idsite,UserInvitRepository $ui): Response
    { 
        $Articles = new Articles();
        $form = $this->createForm('App\Form\ArticlesType',$Articles);
        $user = $userrep->findOneByUsername($name);

        $form->handleRequest($request);
        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
           // $file =$form->get('featured_image')->getData();
            $em = $this->getDoctrine()->getManager();
           $Articles->setUsers($user);
             $date = new \DateTime('now');
             $Articles->setcreated_at($date);
             $Articles->setupdated_at($date);
             $Articles->setCommentstate(false);

                $entityManager->persist($Articles);
                $entityManager->flush();
                $message=$translator->trans('New Post has been created successfully');
                $this->get('session')->getFlashBag()->add(
                    'art',
                    array(
                   
                    'message' => $message
                )
                );
              
                return $this->redirectToRoute('cms_list_articles');
            }

            return $this->render('articles/add_art.html.twig', array(
           
                'form' => $form->createView(),
            ));
        } elseif ($idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR')|| $tt->hasRoles('ROLE_AUTHOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $userrep->findOneByUsername($name);
                        // $file =$form->get('featured_image')->getData();
                         $em = $this->getDoctrine()->getManager();
                        $Articles->setUsers($user);
                          $date = new \DateTime('now');
                          $Articles->setcreated_at($date);
                          $Articles->setupdated_at($date);
                          $Articles->setCommentstate(false);
                             $entityManager->persist($Articles);
                             $entityManager->flush();
                        $entityManager->flush();
                        $message=$translator->trans('New Post has been Created successfully');
                        $this->get('session')->getFlashBag()->add(
                            'art',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                        $langue= $request->getSession()->get('_locale');
                        if ($langue=="fr") {
                            $notif = $this->manager->createNotification('AJout d\'un article !');
                            $notif->setMessage('Nouvelle article ajouter a vous par  '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } elseif ($langue=="ar") {
                            $notif = $this->manager->createNotification('إضافة مقال !');
                            $notif->setMessage($this->getUser().'تم إضافة مقال بواسطة');
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } elseif ($langue=="de") {
                            $notif = $this->manager->createNotification('Artikel hinzufügen!');
                            $notif->setMessage('Neuer Artikel zu Ihnen hinzufügen von '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } else {
                            $notif = $this->manager->createNotification('Adding an article!');
                            $notif->setMessage('New Post Added To You By'.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        }
                        return $this->redirectToRoute('cms_post_invit_view', array(
                            'name' => $name,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $user = $userrep->findOneByUsername($name);
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $langue= $request->getSession()->get('_locale');
                if ($langue=="fr") {
                    $notif = $this->manager->createNotification('Droit uilistion!');
                    $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous Articles manière non authorisèe');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="ar") {
                    $notif = $this->manager->createNotification('  حذاري!');
                    $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="de") {
                    $notif = $this->manager->createNotification('Nutzungsrecht!');
                    $notif->setMessage('die Person deren Name ist '.$this->getUser().'möchte Ihre Post-Informationen auf nicht autorisierte Weise ändern');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } else {
                    $notif = $this->manager->createNotification('unauthorized User!');
                    $notif->setMessage('the are a user named '.$this->getUser().'want to modifiy your Post information in unauthorized way');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                }
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $user = $userrep->findOneByUsername($name);
            $langue= $request->getSession()->get('_locale');
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous Articles manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Post-Informationen auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'want to modifiy your Post information in unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            }
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('articles/add_art.html.twig', array(
           
            'form' => $form->createView(),
        ));

       /*
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userrep->findOneByUsername($name);
           // $file =$form->get('featured_image')->getData();
            $em = $this->getDoctrine()->getManager();
           $Articles->setUsers($user);
             $date = new \DateTime('now');
             $Articles->setcreated_at($date);
             $Articles->setupdated_at($date);
             
              if ($file!=null) {
                  $fileName = md5(uniqid()) . '.' .$file->guessExtension();
                  //$imageDir = $this->container->params->get('kernel.root_dir') . '/../public/uploads/img/experts';
                  $imageDir = $this->getParameter('kernel.root_dir') . '/../public/uploads/img/experts';
                  // Generate a unique name for the file before saving it
                  $file->move($imageDir, $fileName);
                  $Articles->setfeatured_image($fileName);
              }
            $em->persist($Articles);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
    'art',
    array(
       
        'message' => 'New post has been added successfully.'
    )
);
            return $this->redirectToRoute('cms_list_articles');
        }
        return $this->render('articles/add_art.html.twig', array(
           
            'form' => $form->createView(),
        ));*/
    }
    public function UpdateArticle(UserRepository $userrep,TranslatorInterface $translator ,Request $request,ArticlesRepository $artrep,$id,$name,$idsite,UserInvitRepository $ui): Response
    {
        $art = $artrep->find($id);
        $form = $this->createForm('App\Form\ArticlesType', $art);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
           // $file =$form->get('featured_image')->getData();
           $user = $userrep->findOneByUsername($name);

            $em = $this->getDoctrine()->getManager();
             $date = new \DateTime('now');
             $art->setupdated_at($date);

                $entityManager->flush();
                $message=$translator->trans('Post has been updated successfully');
                $this->get('session')->getFlashBag()->add(
                    'art',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_list_articles');
            }

            return $this->render('articles/modifier_art.html.twig', array(
           
                'form' => $form->createView(),
            ));
        } elseif ($idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR')|| $tt->hasRoles('ROLE_AUTHOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $userrep->findOneByUsername($name);
                        // $file =$form->get('featured_image')->getData();
                         $em = $this->getDoctrine()->getManager();
                          $date = new \DateTime('now');
                          $art->setupdated_at($date);
             
                             $entityManager->flush();
                        $entityManager->flush();
                        $message=$translator->trans('Post has been updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'art',
                            array(
                           
                            'message' => $message
                        )
                        );
                       // $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                        $langue= $request->getSession()->get('_locale');
                        if ($langue=="fr") {
                            $notif = $this->manager->createNotification('Modification d\'un article !');
                            $notif->setMessage('Article modifier avec Succès par   '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } elseif ($langue=="ar") {
                            $notif = $this->manager->createNotification('تعديل مقال !');
                            $notif->setMessage($this->getUser().'تم تعديل مقال بواسطة');
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } elseif ($langue=="de") {
                            $notif = $this->manager->createNotification('Artikel Änderung!');
                            $notif->setMessage('Artikel bearbeiten mit Erfolg von'.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        } else {
                            $notif = $this->manager->createNotification('Adding an article!');
                            $notif->setMessage('New Post Added To You By'.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($user), $notif, true);
                        }
                        return $this->redirectToRoute('cms_post_invit_view', array(
                            'idsite'=> $idsite,
                            'name' => $name,

                        ));
                    }
                }
            } else {
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('articles/modifier_art.html.twig', array(
           
            'form' => $form->createView(),
        ));
    }
    public function DeleteArticle(ArticlesRepository $artrep,$id,$name,$idsite,UserInvitRepository $ui,UserRepository $userrep,Request $request): Response
    {


        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $post =$artrep->find($id);
                    $em->remove($post);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($post->getUsers()->getUsername());
                    $langue= $request->getSession()->get('_locale');
                    if ($langue=="fr") {
                        $notif = $this->manager->createNotification('suppression d\'un article !');
                        $notif->setMessage('votre Article supprimer avec Succès par   '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="ar") {
                        $notif = $this->manager->createNotification('حذف مقال !');
                        $notif->setMessage($this->getUser().'تم حذف مقال بواسطة');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="de") {
                        $notif = $this->manager->createNotification('Artikel Löschen!');
                        $notif->setMessage('Ihr Artikel wurde erfolgreich gelöscht von'.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } else {
                        $notif = $this->manager->createNotification('deleting an article!');
                        $notif->setMessage(' Post has been deleted successdfully By'.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    }
                    return $this->redirectToRoute('cms_post_invit_view', array(
                        'idsite'=> $idsite,
                        'name' => $name,

                    ));
                            
                } else {
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $post =$artrep->find($id);
         
   
            if (($this->getUser()->getUsername()==$post->getUsers()->getUsername()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($post);
                $em->flush();
                return $this->redirectToRoute('cms_list_articles');
            }
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }





        /*
        $em=$this->getDoctrine()->getManager();
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($id); 
        $em->remove($art);
        $em->flush();
        return $this->redirect($this->generateUrl('cms_list_articles'));  */
    }
    public function ShowArticle(ArticlesRepository $artrep,$id,CommentaireRepository $commentaire,PaginatorInterface $paginator,Request $request): Response
    {
                $em=$this->getDoctrine()->getManager();
                $art=$artrep->find($id); 
                $pagination=$commentaire->findByArticles($art);
        $commentaires = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        $idc=0;        
        return $this->render('articles/show_art.html.twig', array(
           
            'art' => $art,
            'idc' => $idc,
            'commentaires' => $commentaires,
        ));  
    }
    
    public function ShowListArticle(ArticlesRepository $artrep,PaginatorInterface $paginator,Request $request): Response
    {
        $user=$this->getUser();
                $em=$this->getDoctrine()->getManager();
                $pagination1=$artrep->findByusers($user->getId()); 
                $pagination2=$this->getDoctrine()->getRepository(Categories::class)->findAll();
                $art = $paginator->paginate(
                    $pagination1, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    3 /*limit per page*/
                );
                $cat= $paginator->paginate(
                    $pagination2, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    10 /*limit per page*/
                );
                
        return $this->render('articles/show_list2.html.twig', array(
           
            'list_art' => $art,
            'list_cat'=>$cat,
        ));  
    }
    public function ShowListArticles(ArticlesRepository $artrep): Response
    {
        $user=$this->getUser();
                $em=$this->getDoctrine()->getManager();
                $art=$artrep->findByusers($user->getId()); 
                $cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();
               
                
        return $this->render('articles/show_list2.html.twig', array(
           
            'list_art' => $art,
            'list_cat'=>$cat,
        ));  
    }

    public function ReadMore(ArticlesRepository $artrep,PaginatorInterface $paginator,Request $request,SiteRepository $siterep,$id,$ids,CommentaireRepository $com): Response
    {
        //$user=$this->getUser();
        $em=$this->getDoctrine()->getManager();
       $pagination1=$this->getDoctrine()->getRepository(Categories::class)->findAll();
       $cat = $paginator->paginate(
        $pagination1, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        6 /*limit per page*/
    );
        $post=$artrep->find($id);
        $pagination=$com->findByArticles($post);
        $commentaires = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        $site=$siterep->find($ids);

               
        $idc=0;
        if ($site->getTheme()=="default" || $site->getTheme()=="") {
            return $this->render('frontend/fontsite/blog_detail.html.twig', array(
           
            'art' => $post,
            'idc'=>$idc,
            'site'=>$site,
            'commentaires'=>$commentaires,
        ));
        }
        elseif($site->getTheme()=="template1") {
            return $this->render('frontend/fontsite/template1/blog_detail.html.twig', array(
           
            'art' => $post,
            'idc'=>$idc,
            'cat'=>$cat,
            'site'=>$site,
            'commentaires'=>$commentaires,
        ));}
        elseif($site->getTheme()=="template2") {
            return $this->render('frontend/fontsite/template2/blog_detail.html.twig', array(
           
            'art' => $post,
            'idc'=>$idc,
            'cat'=>$cat,
            'site'=>$site,
            'commentaires'=>$commentaires,
        ));}
        elseif($site->getTheme()=="template3") {
            return $this->render('frontend/fontsite/template3/blog_detail.html.twig', array(
           
            'art' => $post,
            'idc'=>$idc,
            'site'=>$site,
            'commentaires'=>$commentaires,
        ));}
        elseif($site->getTheme()=="template4") {
            return $this->render('frontend/fontsite/template4/blog_detail.html.twig', array(
           
            'art' => $post,
            'idc'=>$idc,
            'site'=>$site,
            'commentaires'=>$commentaires,
        ));}




    }


    

    // commentaire entity functions
    public function  AddComment(\Symfony\Component\HttpFoundation\Request $request,$id,UserRepository $userrep):Response
    {
        $commentaire = new Commentaire();
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($id);

       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $commentaire->setusers($user);
           $commentaire->setEmail($user->getEmail());
           $commentaire->setPseudo($user->getUsername());
           $commentaire->setActif(true);
           $commentaire->setRgpd(true);
           $commentaire->setArticles($art);
             $date = new \DateTime('now');
            $commentaire->setCreatedAt($date);
            $commentaire->setUpdatedAt($date);
            $commentaire->setContenu($_POST['contenu']);
   
      
            $em->persist($commentaire);
            $em->flush();
              $userrecivernot=$userrep->findOneByUsername($art->getUsers()->getUsername());
             $langue= $request->getSession()->get('_locale');
             if ($langue=="fr") {
                 $notif = $this->manager->createNotification('Ajout d\'un commentaire !');
                 $notif->setMessage('Nouvel commentaire ajouter a votre article'.$art->getTitre());
                 $notif->setLink('https://Youdev-it.com/');
                 // or the one-line method :
                 // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
       
                 // you can add a notification to a list of entities
                 // the third parameter `$flush` allows you to directly flush the entities
                 $this->manager->addNotification(array($userrecivernot), $notif, true);
             } elseif ($langue=="ar") {
                 $notif = $this->manager->createNotification(' اضافة تعليق !');
                 $notif->setMessage($art->getTitre().' تم إضافة تعليق جديد لمنشورك ');
                 $notif->setLink('https://Youdev-it.com/');
                 // or the one-line method :
                 // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
       
                 // you can add a notification to a list of entities
                 // the third parameter `$flush` allows you to directly flush the entities
                 $this->manager->addNotification(array($userrecivernot), $notif, true);
             } elseif ($langue=="de") {
                 $notif = $this->manager->createNotification('Kommentar hinzufügen !');
                 $notif->setMessage('Neuer Kommentar zu Ihrem Artikel hinzufügen'.$art->getTitre());
                 $notif->setLink('https://Youdev-it.com/');
                 // or the one-line method :
                 // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
       
                 // you can add a notification to a list of entities
                 // the third parameter `$flush` allows you to directly flush the entities
                 $this->manager->addNotification(array($userrecivernot), $notif, true);
             } else {
                 $notif = $this->manager->createNotification('New Comment Adding!');
                 $notif->setMessage('New comment has been added to your post'.$art->getTitre());
                 $notif->setLink('https://Youdev-it.com/');
                 // or the one-line method :
                 // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
       
                 // you can add a notification to a list of entities
                 // the third parameter `$flush` allows you to directly flush the entities
                 $this->manager->addNotification(array($userrecivernot), $notif, true);
             }
        
            return $this->redirectToRoute('cms_show_articles',[
                'id'=>$id
            ]);
       

     
        
    }
    public function  AddCommentPost(\Symfony\Component\HttpFoundation\Request $request,UserRepository $sr,$id,$ids,SiteRepository $siterep):Response
    {
        $commentaire = new Commentaire();
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($id);
        $site=$siterep->find($ids);
        //Request::setTrustedProxies(['10.0.0.1']);

        $Ip = $request->getClientIp();
            // the client is a known one, so give it some more privilege
        
        // $user = $this->getUser();
           
        $em = $this->getDoctrine()->getManager();
        $user=$sr->findOneByEmail($_POST['email']);
        if ($user !=null) {
            $commentaire->setusers($user);
        
        $commentaire->setEmail($user->getEmail());
        $commentaire->setPseudo($user->getUsername());
        $commentaire->setIp($Ip);

    } else{
              
        $commentaire->setEmail($_POST['email']);
        $commentaire->setPseudo($_POST['name']);
        $commentaire->setusers(null);
        $commentaire->setIp($Ip);

         }

           $commentaire->setActif(true);
           $commentaire->setRgpd(true);
           $commentaire->setArticles($art);
             $date = new \DateTime('now');
            $commentaire->setCreatedAt($date);
            $commentaire->setUpdatedAt($date);
            $commentaire->setContenu($_POST['contenu']);
   
      
            $em->persist($commentaire);
            $em->flush();
            
            $userrecivernot=$sr->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Ajout d\'un commentaire !');
                $notif->setMessage('Nouvel commentaire ajouter a votre article'.$art->getTitre());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification(' اضافة تعليق !');
                $notif->setMessage($art->getTitre().' تم إضافة تعليق جديد لمنشورك ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Kommentar hinzufügen !');
                $notif->setMessage('Neuer Kommentar zu Ihrem Artikel hinzufügen'.$art->getTitre());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('New Comment Adding!');
                $notif->setMessage('New comment has been added to your post'.$art->getTitre());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            }
       
            return $this->redirectToRoute('cms_article_read_more',[
                'id'=>$id,
                'ids'=>$ids
            ]);
       

     
        
    }

    public function UpdateFormCommentPost($id,$idt,$ids,CommentaireRepository $comm,SiteRepository $siterep,PaginatorInterface $paginator,Request $request): Response
    {
        $em=$this->getDoctrine()->getManager();
        $com=$this->getDoctrine()->getRepository(Commentaire::class)->find($id); 
        $post=$this->getDoctrine()->getRepository(Articles::class)->find($idt);
        $user=$this->getUser();
        $site=$siterep->find($ids);
        $pagination=$comm->findByArticles($post);
        $commentaires = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        $pagination1=$this->getDoctrine()->getRepository(Categories::class)->findAll();
        $cat = $paginator->paginate(
         $pagination1, /* query NOT result */
         $request->query->getInt('page', 1), /*page number*/
         6 /*limit per page*/
     );
     if ($user!=null) {
         if (($user->getEmail()== $com->getEmail()) or $user->hasRole('ROLE_ADMIN')) {
             if ($site->getTheme()=="default" || $site->getTheme()=="") {
                 return $this->render('frontend/fontsite/blog_detail.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'cat'=>$cat,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
             } elseif ($site->getTheme()=="template1") {
                 return $this->render('frontend/fontsite/template1/blog_detail.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'cat'=>$cat,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
             } elseif ($site->getTheme()=="template2") {
                 return $this->render('frontend/fontsite/template2/blog_detail.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'cat'=>$cat,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
             } elseif ($site->getTheme()=="template3") {
                 return $this->render('frontend/fontsite/template3/blog_detail.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'cat'=>$cat,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
             } elseif ($site->getTheme()=="template4") {
                 return $this->render('frontend/fontsite/template4/blog_detail.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'cat'=>$cat,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
             }
         } else {
             return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
         }
     }
    else
    {
        if (($request->getClientIp()== $com->getIp())) {
            if ($site->getTheme()=="default" || $site->getTheme()=="") {
                return $this->render('frontend/fontsite/blog_detail.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
            } elseif ($site->getTheme()=="template1") {
                return $this->render('frontend/fontsite/template1/blog_detail.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
            } elseif ($site->getTheme()=="template2") {
                return $this->render('frontend/fontsite/template2/blog_detail.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'cat'=>$cat,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
            } elseif ($site->getTheme()=="template3") {
                return $this->render('frontend/fontsite/template3/blog_detail.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
            } elseif ($site->getTheme()=="template4") {
                return $this->render('frontend/fontsite/template4/blog_detail.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
            }
        } else {
            return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
              
               'art' => $post,
               'idc'=>$id,
               'site'=>$site,
               'commentaires'=>$commentaires,
           ));
        } 
    }

    }
    public function  UpdateCommentPost(\Symfony\Component\HttpFoundation\Request $request,$id,$idt,$ids):Response
    {
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($idt);
        $commentaire=$this->getDoctrine()->getRepository(Commentaire::class)->find($id);
        $site=$this->getDoctrine()->getRepository(Site::class)->find($ids);

       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
         
          
             $date = new \DateTime('now');
        
            $commentaire->setUpdatedAt($date);
            $commentaire->setContenu($_POST['contenu']);
   
      
         
            $em->flush();
            
            return $this->redirectToRoute('cms_article_read_more',[
                'id'=>$idt,
                'ids'=>$ids
            ]);

     
        
    }

    public function DeleteCommentPost($id,$idt,$ids,UserRepository $userrep  ,SiteRepository $siterep,PaginatorInterface $paginator,Request $request,CommentaireRepository $comm): Response
    {
        $em=$this->getDoctrine()->getManager();
        $com=$this->getDoctrine()->getRepository(Commentaire::class)->find($id); 
        $post=$this->getDoctrine()->getRepository(Articles::class)->find($idt);
        $user=$this->getUser();
        $site=$siterep->find($ids);
        $pagination=$comm->findByArticles($post);
        $commentaires = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        if ($user!= null) {
            if (($user->getEmail() == $com->getEmail()) or $user->hasRole('ROLE_ADMIN')) {
                $em->remove($com);
                $em->flush();
                $usernotif=$userrep->findOneByUsername($post->getusers()->getUsername());
                $langue= $request->getSession()->get('_locale');
                if ($langue=="fr") {
                    $notif = $this->manager->createNotification('suppression commentaire!');
                    $notif->setMessage('nouvelle commentaire  associee a l\'articles '.$post->getTitre().'est supprimer  ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($usernotif), $notif, true);
                } elseif ($langue=="ar") {
                    $notif = $this->manager->createNotification('حذف تعليق!');
                    $notif->setMessage($post->getTitre().'تم حذف تعليق لمنشورك');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($usernotif), $notif, true);
                } elseif ($langue=="de") {
                    $notif = $this->manager->createNotification('Löschen der Kommentar !');
                    $notif->setMessage('neuer Kommentar zu den Artikeln'.$post->getTitre().'ist gelöscht');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($usernotif), $notif, true);
                } else {
                    $notif = $this->manager->createNotification('Comment Deleted!');
                    $notif->setMessage('a comment associated to the post  '.$post->getTitre().' has been deleted successfully');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($usernotif), $notif, true);
                }
                return $this->redirectToRoute('cms_article_read_more', [
            'id'=>$idt,
            'ids'=>$ids,
        ]);
            } else {
                return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
            }
        }
        else {
            if ($request->getClientIp() == $com->getIp()) {
                $em->remove($com);
                $em->flush();
                return $this->redirectToRoute('cms_article_read_more', [
            'id'=>$idt,
            'ids'=>$ids,
        ]);
            } else {
                return $this->render('frontend/fontsite/erreur_permissions.html.twig', array(
               
                'art' => $post,
                'idc'=>$id,
                'site'=>$site,
                'commentaires'=>$commentaires,
            ));
            }
            
        }
    }


    public function DeleteComment($id,$idt): Response
    {
        $em=$this->getDoctrine()->getManager();
        $com=$this->getDoctrine()->getRepository(Commentaire::class)->find($id); 
        $user=$this->getUser();
        if(($user->getId()== $com->getUsers()->getId()) or $user->hasRole('ROLE_ADMIN')){
        $em->remove($com);
        $em->flush();
        return $this->redirectToRoute('cms_show_articles',[
            'id'=>$idt
        ]);}
        else {
            return $this->render('articles/erreur_permissions.html.twig');
        }
    }
    public function UpdateComment($id,$idt): Response
    {
        $em=$this->getDoctrine()->getManager();
        $com=$this->getDoctrine()->getRepository(Commentaire::class)->find($id); 
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($idt);
        $user=$this->getUser();

        if (($user->getId()== $com->getUsers()->getId()) or $user->hasRole('ROLE_ADMIN')) {
            return $this->render('articles/show_art.html.twig', array(
           
            'art' => $art,
            'idc' => $id,
        ));
        } 
        else {
            return $this->render('articles/erreur_permissions.html.twig');
        }

    }




    public function  UpdateArticleComment(\Symfony\Component\HttpFoundation\Request $request,$id,$idd):Response
    {
        $art=$this->getDoctrine()->getRepository(Articles::class)->find($id);
        $commentaire=$this->getDoctrine()->getRepository(Commentaire::class)->find($id);

       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
         
          
             $date = new \DateTime('now');
        
            $commentaire->setUpdatedAt($date);
            $commentaire->setContenu($_POST['contenu']);
   
      
         
            $em->flush();
            
        
            return $this->redirectToRoute('cms_show_articles',[
                'id'=>$idd
            ]);
       

     
        
    }



        // crud for tags article


    public function ListTags(MotsClesRepository $tags): Response
    {
        $user=$this->getUser();
                $em=$this->getDoctrine()->getManager();
                $taglist=$tags->findBycreatedby($user->getUsername()); 
               
                
        return $this->render('articles/tags_list.html.twig', array(
           
            'tags' => $taglist,
           
        ));  
    }
    public function  AddTags(\Symfony\Component\HttpFoundation\Request $request,TranslatorInterface $translator):Response
    {
        $tag = new MotsCles();
        

       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $tag->setCreatedby($user->getUsername());
           $tag->setMotCle($_POST['name']);
           $tag->setSlug($_POST['url']);
             $date = new \DateTime('now');
            $tag->setCreatedAt($date);
            $tag->setUpdatedAt($date);
   
      
            $em->persist($tag);
            $em->flush();
            $message=$translator->trans('New tag has been added successfully');

            $this->get('session')->getFlashBag()->add(
    'tag',
    array(
       
        'message' => $message
    )
);
            return $this->redirectToRoute('cms_list_tags');
       

     
        
    }
    public function UpdateTags(Request $request,MotsClesRepository $tag,$id,TranslatorInterface $translator): Response
    { 
        $tags = $tag->find($id);
        $user= $this->getUser();

        if (($user->getUsername()== $tags->getCreatedby()) or $user->hasRole('ROLE_ADMIN')) {

            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
            $date = new \DateTime('now');
            $tags->setUpdatedAt($date);
   
            $tags->setMotCle($_POST['name1']);
            $tags->setSlug($_POST['slug']);
            
            $em->flush();
            $message=$translator->trans('tag with name ');
            $message2=$translator->trans(' has been updated successflly ');

            $this->get('session')->getFlashBag()->add(
                'tag',
                array(
       
        'message' => $message.''.$tags->getMotCle().''.$message2
    )
            );
            return $this->redirectToRoute('cms_list_tags');
        }
            else{
                return $this->render('articles/erreur_permissions.html.twig'); 
            }
     

     
    
    }
    public function DeleteTags($id): Response
    {
        $em=$this->getDoctrine()->getManager();
        $tag=$this->getDoctrine()->getRepository(MotsCles::class)->find($id); 
        $user=$this->getUser();
        if(($user->getUsername()== $tag->getCreatedby()) or $user->hasRole('ROLE_ADMIN')){
        $em->remove($tag);
        $em->flush();
        return $this->redirectToRoute('cms_list_tags');}
        else {
            return $this->render('articles/erreur_permissions.html.twig');
        }
    }




    public function ChangeStateComment($id,ArticlesRepository $artrep){
        $post=$artrep->find($id);
        $em=$this->getDoctrine()->getManager();
        if($post->getCommentstate()==true){
            $post->setCommentstate(false) ; 
            $em->flush();
        }
        else{
            $post->setCommentstate(true);
            $em->flush();
    
        }
        return $this->redirectToRoute('cms_list_articles');
    }
}
