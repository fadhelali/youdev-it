<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Pricing;
use App\Entity\Site;
use App\Form\PricingType;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\PricingRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/pricing")
 */
class PricingController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }


    /**
     * @Route("/", name="cms_pricing_index")
     */
    public function index(PricingRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $pricings=$pr->findByCreadtedby($this->getUser()->getUserName());
        return $this->render('pricing/index.html.twig', [
            'list_pricings' =>$pricings,
        ]);
    }

    /**
     * @Route("/new/{idpage}/pricing/{idsite}", name="cms_pricing_new", methods={"GET","POST"})
     */
    public function new(UserRepository $userrep,Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator): Response
    {
        $Pricing = new Pricing();
        $form = $this->createForm(PricingType::class, $Pricing);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $Pricing->setCreadtedby($user->getUsername());
                $date = new \DateTime('now');
                $Pricing->setCreatedAt($date);
                $Pricing->setUpdatedAt($date);
           

                $entityManager->persist($Pricing);
                $entityManager->flush();
                $message=$translator->trans('New Block pricing added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'pricing',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_pricing_index');
            }

            return $this->render('pricing/new.html.twig', [
            'pricing' => $Pricing,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $Pricing->setCreadtedby($user->getUsername());
                        $date = new \DateTime('now');
                        $Pricing->setCreatedAt($date);
                        $Pricing->setUpdatedAt($date);
                        $Pricing->setPage($page);
                        $page->addPricing($Pricing);
                   
        
                        $entityManager->persist($Pricing);
                        $entityManager->flush();
                        $message=$translator->trans('New Block pricing added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'pricing',
                            array(
                           
                            'message' =>$message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block pricing for page!');
                        $notif->setMessage('adding block pricing for  page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_pricing', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a  pricing block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add a  pricing block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('pricing/new.html.twig', [
            'pricing' => $Pricing,
            'form' => $form->createView(),

            ]);

    }
    


    /**
     * @Route("/{id}/update/{idpage}/pricingupdate/{idsite}", name="cms_pricing_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,TranslatorInterface $translator,Request $request, PricingRepository $sr, $id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $Pricing =$sr->find($id);
        $form = $this->createForm(PricingType::class, $Pricing);
        $form->handleRequest($request);

        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $Pricing->setUpdatedAt($date);
           

                $entityManager->persist($Pricing);
                $entityManager->flush();
                $message=$translator->trans('Block pricing updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'pricing',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_pricing_index');
            }

            return $this->render('pricing/update.html.twig', [
            'pricing' => $Pricing,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $Pricing->setUpdatedAt($date);
                        $Pricing->setPage($page);
                        $page->addPricing($Pricing);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans('Block pricing updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'pricing',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('editing block pricing for page!');
                        $notif->setMessage('editing block pricing for page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_pricing', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit a pricing block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to edit a pricing block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('pricing/update.html.twig', [
            'pricing' => $Pricing,
            'form' => $form->createView(),

            ]);
    }
    /**
     * @Route("/{id}/delete/{idpage}/deletepricing/{idsite}", name="cms_pricing_delete")
     */
    
    public function delete($id,PricingRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
       
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $pricing =$sr->find($id);
                    $em->remove($pricing);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block pricing for page!');
                    $notif->setMessage('deleting block pricing for page  to website named'.$site->getName().' by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_pricing', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {

                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete  a  pricing block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete a  pricing block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $pricing =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$pricing->getCreadtedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($pricing);
                $em->flush();
                return $this->redirectToRoute('cms_pricing_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a pricing block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }






    public function ViewPricingInvitPage(UserRepository $userrep,$id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $pricings=$page->getPricings();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewPricing.html.twig', array(
                   
          
            'list_pricings' => $pricings,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access pricing block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
               
    $notif = $this->manager->createNotification('OOps!');
    $notif->setMessage('unauthorized user wants to access pricing block for your website '.$site->getName());
    $notif->setLink('https://Youdev-it.com/');
    // or the one-line method :
    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

    // you can add a notification to a list of entities
    // the third parameter `$flush` allows you to directly flush the entities
    $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
    
}