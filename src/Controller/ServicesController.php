<?php

namespace App\Controller;

use App\Entity\Services;
use App\Entity\Site;
use App\Form\ServiceType;
use App\Repository\ServicesRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/services")
 */
class ServicesController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_service_index")
     */
    public function index(ServicesRepository $service): Response
    {
        $em=$this->getDoctrine()->getManager();
        $services=$service->findByCreatedby($this->getUser()->getUserName());
        return $this->render('services/index.html.twig', [
            'list_services' =>$services,
        ]);
    }

    /**
     * @Route("/new/services/{idsite}/newservice", name="cms_services_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $service = new Services();
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $service->setCreatedBy($user->getUsername());
                $date = new \DateTime('now');
                $service->setCreatedAt($date);
                $service->setUpdatedAt($date);
               

                $entityManager->persist($service);
                $entityManager->flush();
                $message=$translator->trans('New service has been added successfully');

                $this->get('session')->getFlashBag()->add(
                    'service',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_service_index');
            }

            return $this->render('services/new.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
        }
        elseif ($idsite != "null"){
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach($userinvit as $usr){
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $em = $this->getDoctrine()->getManager();
                        $service->setCreatedBy($user->getUsername());
                        $date = new \DateTime('now');
                        $service->setCreatedAt($date);
                        $service->setUpdatedAt($date);
                          $service->addSite($site);
                        $entityManager->persist($service);
                        $entityManager->flush();
                        $message=$translator->trans('New service has been added successfully');

                        $this->get('session')->getFlashBag()->add(
                            'service',
                            array(
                   
                    'message' => $message
                )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding  service for website!');
                        $notif->setMessage('adding  service  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_site_view_servicesInvit', array(
                    'id' => $idsite,
                ));
                    }
                } else {

                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add a  service for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            }
            else{

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a  service for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }

            return $this->render('services/new.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]); 
        }


    }
    /**
     * @Route("/{id}/update/{idsite}/updateservice", name="cms_service_update", methods={"GET","POST"})
     */
    public function update(UserRepository $userrep,Request $request, ServicesRepository $sr, $id,$idsite,UserInvitRepository $ui,TranslatorInterface $translator): Response
    {
        $service =$sr->find($id);
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $service->setUpdatedAt($date);
               

                $entityManager->flush();
                $message=$translator->trans('service has been updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'service',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_service_index');
            }

            return $this->render('services/update.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
        }
        elseif ($idsite != "null"){
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach($userinvit as $usr){
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $service->setUpdatedAt($date);
                          $service->addSite($site);
                        $entityManager->flush();
                        $message=$translator->trans('service has been updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'service',
                            array(
                   
                    'message' =>$message
                )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('editing  service for website!');
                        $notif->setMessage('editing  service for  website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_site_view_servicesInvit', array(
                    'id' => $idsite,
                ));
                    }
                } else {

                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to edit a service  for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            }
            else{

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit a service   for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }

            return $this->render('services/update.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]); 
        }
    }
    /**
     * @Route("/{id}/delete/{idsite}/deleteservices", name="cms_service_delete")
     */
    public function delete($id, ServicesRepository $sr,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (( $tt->hasRoles('ROLE_MODERATOR'))) {
                    $service =$sr->find($id);
                    $em->remove($service);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block service for website!');
                    $notif->setMessage('deleting  service for  website named'.$site->getName().' by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_site_view_servicesInvit', array(
                    'id' => $idsite,
                ));
                } else {

                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete a service  for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete a service  for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $service =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$service->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($service);
                $em->flush();
                return $this->redirectToRoute('cms_service_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a  service  for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }
}