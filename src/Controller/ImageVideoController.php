<?php

namespace App\Controller;

use App\Entity\ImageVideo;
use App\Entity\Page;
use App\Entity\Site;
use App\Form\ImageVideoType;
use App\Repository\HeaderRepository;
use App\Repository\ImageVideoRepository;
use App\Repository\PageRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\UserRepository;

/**
 * @Route("/cms/imagevideo")
 */
class ImageVideoController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }


    /**
     * @Route("/", name="cms_imagevideo_index")
     */
    public function index(ImageVideoRepository $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $imagevideo=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('image_video/index.html.twig', [
            'list_imagevideo' =>$imagevideo,
        ]);
    }

    /**
     * @Route("/new/{idpage}/blockvideo/{idsite}", name="cms_imagevideo_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $imagevideo = new ImageVideo();
        $form = $this->createForm(ImageVideoType::class, $imagevideo);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $imagevideo->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $imagevideo->setCreatedAt($date);
                $imagevideo->setUpdatedAt($date);
           

                $entityManager->persist($imagevideo);
                $entityManager->flush();
                $message=$translator->trans('New Block image && video added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'pricing',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_imagevideo_index');
            }

            return $this->render('image_video/new.html.twig', [
            'imagevideo' => $imagevideo,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $imagevideo->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $imagevideo->setCreatedAt($date);
                        $imagevideo->setUpdatedAt($date);
                        $imagevideo->setPages($page);
                        $page->addImageVideo($imagevideo);
                   
        
                        $entityManager->persist($imagevideo);
                        $entityManager->flush();
                        $message=$translator->trans('New Block image && video added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'pricing',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block images & videos!');
                        $notif->setMessage('Adding new block images a videos by '.$this->getUser().'for your page witch  website is '. $site->getName());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_imagevideo', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add a images & videos  block for your page witch website is '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a images & videos  block for your page witch website is '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('image_video/new.html.twig', [
            'imagevideo' => $imagevideo,
            'form' => $form->createView(),

            ]);
        
    }
    


    /**
     * @Route("/{id}/updateimagevideo/{idpage}/update/{idsite}", name="cms_imagevideo_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,ImageVideoRepository $sr,TranslatorInterface $translator ,$id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $imagevideo =$sr->find($id);
        
        $form = $this->createForm(ImageVideoType::class, $imagevideo);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $imagevideo->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('Block image && video updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'pricing',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_imagevideo_index');
            }

            return $this->render('image_video/update.html.twig', [
            'imagevideo' => $imagevideo,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $imagevideo->setUpdatedAt($date);
                        $imagevideo->setPages($page);
                        $page->addImageVideo($imagevideo);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans('Block image && video updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'imagevideo',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('updating block images & videos!');
                        $notif->setMessage('updating block images & videos  by '.$this->getUser().'for your page witch  website is '. $site->getName());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_imagevideo', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to update images & videos block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to update images & videos  header block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('image_video/update.html.twig', [
            'imagevideo' => $imagevideo,
            'form' => $form->createView(),

            ]);
        
    }
    /**
     * @Route("/{id}/deleteimagevideo/{idpage}/delete/{idsite}", name="cms_imagevideo_delete")
     */
    
    public function delete($id, ImageVideoRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $imagevideo =$sr->find($id);
                    $em->remove($imagevideo);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block images & videos!');
                    $notif->setMessage('deleting  block images & videos by '.$this->getUser().'for your page witch  website is '. $site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_imagevideo', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete images & videos block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete images & videos block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $imagevideo =$sr->find($id);
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            if (($this->getUser()->getUsername()==$imagevideo->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($imagevideo);
                $em->flush();
                return $this->redirectToRoute('cms_imagevideo_index');
            }
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete images & videos block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }




    public function ViewImageVideoInvitPage(UserRepository $userrep,$id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $imagevideo=$page->getImageVideos();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewImageVideo.html.twig', array(
                   
          
            'list_imagevideo' => $imagevideo,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access images & videos block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access images & videos block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
    
}
