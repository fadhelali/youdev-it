<?php

namespace App\Controller;

use App\Entity\Header;
use App\Entity\Page;
use App\Entity\Site;
use App\Repository\UserRepository;

use App\Form\HeaderType;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/header")
 */
class HeaderController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_header_index")
     */
    public function index(HeaderRepository $header): Response
    {
        $em=$this->getDoctrine()->getManager();
        $headers=$header->findByCreatedby($this->getUser()->getUserName());
        return $this->render('header/index.html.twig', [
            'list_headers' =>$headers,
        ]);
    }

    /**
     * @Route("/new/{idpage}/{idsite}", name="cms_header_new", methods={"GET","POST"})
     */
    public function new(UserRepository $userrep,Request $request,$idpage,$idsite,TranslatorInterface $translator,UserInvitRepository $ui,HeaderRepository $pr): Response
    {
        $header = new Header();
        $form = $this->createForm(HeaderType::class, $header);
        $form->handleRequest($request);
        if ($idsite=="null" && $idpage=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $header->setCreatedBy($user->getUsername());
                $date = new \DateTime('now');
                $header->setCreatedAt($date);
                $header->setUpdatedAt($date);
                $headers=$pr->findAll();
                
                if ($headers != null) {
                    foreach ($headers as $hd) {
                        if ($hd->getPage()->getName()==$header->getPage()->getName()) {
                            $entityManager->remove($hd);
                        }
                    }
                }

                $entityManager->persist($header);
                $entityManager->flush();
                $message=$translator->trans('New Header has been added successfully');

                $this->get('session')->getFlashBag()->add(
                    'header',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_header_index');
            }

            return $this->render('header/new.html.twig', [
            'header' => $header,
            'form' => $form->createView(),
        ]);
        }
        elseif ($idsite != "null" && $idpage !="null"){
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach($userinvit as $usr){
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $em = $this->getDoctrine()->getManager();
                        $header->setCreatedBy($user->getUsername());
                        $date = new \DateTime('now');
                        $header->setCreatedAt($date);
                        $header->setUpdatedAt($date);
                        $headers=$pr->findByPage($page);
                        foreach ($headers as $hd) {
                            if ($hd != null) {
                                $entityManager->remove($hd);
                            }
                        }
                        $header->setPage($page);

                        $page->addHeader($header);
                        $entityManager->persist($header);
                        $entityManager->flush();
                        $message=$translator->trans('New Header has been added successfully by invited user');

                        $this->get('session')->getFlashBag()->add(
                            'headerinvit',
                            array(
                   
                    'message' => $message
                )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block Header!');
                        $notif->setMessage('Adding new block Header by '.$this->getUser().'for your page witch  website is '. $site->getName());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_header', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                    }
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to add a header block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add a header block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }

            return $this->render('header/new.html.twig', [
            'header' => $header,
            'form' => $form->createView(),
        ]); 
        }
    }
    /**
     * @Route("/{id}/update/{idsite}/{idpage}/updateheader", name="cms_header_update", methods={"GET","POST"})
     */
    public function update(UserRepository $userrep,Request $request, HeaderRepository $sr,TranslatorInterface $translator ,$id,$idsite,$idpage,UserInvitRepository $ui): Response
    {
        $header =$sr->find($id);
        $form = $this->createForm(HeaderType::class, $header);
        $form->handleRequest($request);
        if ($idsite=="null" && $idpage=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $header->setUpdatedAt($date);
               

                $entityManager->flush();
                $message=$translator->trans('New Header has been updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'header',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_header_index');
            }

            return $this->render('header/update.html.twig', [
            'header' => $header,
            'form' => $form->createView(),
        ]);
        }
        elseif ($idsite != "null" && $idpage !="null"){
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach($userinvit as $usr){
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $header->setUpdatedAt($date);
                        $header->setPage($page);
                        $page->addHeader($header);
                       
                        $entityManager->flush();
                        $message=$translator->trans('New Header has been updated successfully by Invit User');

                        $this->get('session')->getFlashBag()->add(
                            'headerinvit',
                            array(
                   
                    'message' => $message
                )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('updating block header!');
                        $notif->setMessage('updating block header by '.$this->getUser().'for your page witch  website is '. $site->getName());
                        $notif->setLink('https://Youdev-it.com/');
                       
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_header', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                    }
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to update header block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to update header block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');

            }

            return $this->render('header/update.html.twig', [
            'header' => $header,
            'form' => $form->createView(),
        ]); 
        }
    }
    /**
     * @Route("/{id}/delete/{idsite}/{idpage}", name="cms_header_delete")
     */
    public function delete(UserRepository $userrep,$id, HeaderRepository $sr,$idsite,$idpage,UserInvitRepository $ui,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();

       
        if ($idpage !="null" && $idsite !="null") {
            $page =$pr->find($id);
            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach($userinvit as $usr){
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $header =$sr->find($id);
                    $em->remove($header);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block header!');
                    $notif->setMessage('deleting  block header by '.$this->getUser().'for your page witch  website is '. $site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_header', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete header block for your website '.$site->getName());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete header block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        }
        else{
            $header =$sr->find($id);
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

   
            if (($this->getUser()->getUsername()==$header->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($header);
                $em->flush();
                return $this->redirectToRoute('cms_header_index');
            }
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete header block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }
    public function ViewHeaderInvitPage($id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $headers=$page->getHeaders();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewHeader.html.twig', array(
                   
          
            'headers' => $headers,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access header block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access header block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
}