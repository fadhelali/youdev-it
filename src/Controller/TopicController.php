<?php

namespace App\Controller;

use App\Entity\Site;
use App\Entity\Topic;
use App\Form\TopicFormType;
use App\Repository\PageRepository;
use App\Repository\TopicRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/topic")
 */
class TopicController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }



    /**
     * @Route("/", name="cms_topic_index")
     */
    public function index(TopicRepository $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $topics=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('topic/index.html.twig', [
            'list_topics' =>$topics,
        ]);
    }

    /**
     * @Route("/new/topic/{idsite}", name="cms_topic_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui,PageRepository $pr,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $Topic = new Topic();
        $form = $this->createForm(TopicFormType::class, $Topic);
        $form->handleRequest($request);
          if ($idsite=="null") {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $Topic->setCreatedby($user->getUsername());
                  $date = new \DateTime('now');
                  $Topic->setCreatedAt($date);
                  $Topic->setUpdatedAt($date);
                  $Topic->setMessagestate(true);
                

                  $entityManager->persist($Topic);
                  $entityManager->flush();
                  $message=$translator->trans('New Topic added successfully');

                  $this->get('session')->getFlashBag()->add(
                      'topic',
                      array(
                   
                    'message' => $message
                )
                  );
                 
                  return $this->redirectToRoute('cms_topic_index');
              }

              return $this->render('topic/new.html.twig', [
            'Topic' => $Topic,
            'form' => $form->createView(),
        ]);
          }
          elseif($idsite!="null"){

            $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }

        if ($test==true) {
            if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $user = $this->getUser();
             
                    $em = $this->getDoctrine()->getManager();
                    $Topic->setCreatedby($user->getUsername());
                    $date = new \DateTime('now');
                    $Topic->setCreatedAt($date);
                    $Topic->setUpdatedAt($date);
                    $Topic->setMessagestate(true);

                   
                    $entityManager->persist($Topic);
                    $entityManager->flush();
                    $message=$translator->trans('New Topic added successfully ');

                    $this->get('session')->getFlashBag()->add(
                        'topic',
                        array(
                     
                      'message' => $message
                  )
                    );
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('Adding new Topic!');
                    $notif->setMessage('Adding Topic by '.$this->getUser().'for yours forums ');
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_topic', array(
                        'idsite'=> $idsite,
                    ));
                }

            }
            else{
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add topic to your formus ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                   return $this->render('Invits/page/erreur_permissions.html.twig');

            }
        }
        else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add topic to your formus');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
          return $this->render('Invits/page/erreur_permissions.html.twig');

        }

        return $this->render('topic/new.html.twig', [
            'Topic' => $Topic,
            'form' => $form->createView(),
        ]);


          }
         
    }
    


    /**
     * @Route("/{id}/update/topic/{idsite}", name="cms_topic_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,TopicRepository $sr,TranslatorInterface $translator ,$id,$idsite,UserInvitRepository $ui,PageRepository $pr): Response
    {
        $topic =$sr->find($id);
        $form = $this->createForm(TopicFormType::class, $topic);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $topic->setUpdatedAt($date);
               

                $entityManager->flush();
                $message=$translator->trans('topic updated successfully ');

                $this->get('session')->getFlashBag()->add(
                    'topic',
                    array(
                 
                  'message' => $message
              )
                );
                return $this->redirectToRoute('cms_topic_index');
            }

            return $this->render('topic/update.html.twig', [
          'topic' => $topic,
          'form' => $form->createView(),
      ]);
        }
        elseif($idsite!="null"){

          $user=$this->getUser();
      $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
      $userinvit=$ui->findByCreatedby($site->getAuthor());
      $test= false;
      $tt=null;
      foreach($userinvit as $usr){
          if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
              $test=true;
              $tt=$usr;
          }
      }

      if ($test==true) {
          if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $date = new \DateTime('now');
                  $topic->setUpdatedAt($date);
                 

                  $entityManager->flush();
                  $message=$translator->trans(' topic updated successfully To Your Page');

                  $this->get('session')->getFlashBag()->add(
                      'topic',
                      array(
                   
                    'message' => $message
                )
                  );
                  $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                  $notif = $this->manager->createNotification('updating block topic!');
                  $notif->setMessage('updating block topic by '.$this->getUser().'for your forums');
                  $notif->setLink('https://Youdev-it.com/');
                 
                  $this->manager->addNotification(array($userrecivernot), $notif, true);
                  return $this->redirectToRoute('cms_invit_view_topic', array(
                      'idsite'=> $idsite,
                  ));
              }

          }
          else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to update topic info');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                 return $this->render('Invits/page/erreur_permissions.html.twig');

          }
      }
      else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to update topic info');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);

        return $this->render('Invits/page/erreur_permissions.html.twig');

      }

      return $this->render('topic/update.html.twig', [
          'topic' => $topic,
          'form' => $form->createView(),
      ]);


        }
    }
    /**
     * @Route("/{id}/delete/{idsite}", name="cms_topic_delete")
     */
    
    public function delete($id, TopicRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();

       
        if ($idsite !="null") {
            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $gallery =$sr->find($id);
                    $em->remove($gallery);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('Deleting Topic Info...!');
                    $notif->setMessage('deleting topic info  by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                   
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_topic', array(
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete topic info');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete topic info');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $topic=$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$topic->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($topic);
                $em->flush();
                return $this->redirectToRoute('cms_topic_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete topic info');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    }
  


}
