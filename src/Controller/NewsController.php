<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Site;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/news")
 */
class NewsController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_news_index")
     */
    public function index(NewsRepository $np): Response
    {
        $em=$this->getDoctrine()->getManager();
        $news=$np->findByCreatedby($this->getUser()->getUserName());
        return $this->render('news/index.html.twig', [
            'list_news' =>$news,
        ]);
    }

    /**
     * @Route("/news/new/{idsite}", name="cms_news_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $news = new News();
        
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $news->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $news->setCreatedAt($date);
                $news->setUpdatedAt($date);
                $news->setEtat("yes");
           

                $entityManager->persist($news);
                $entityManager->flush();
                $message=$translator->trans('New  News  added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'news',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_news_index');
            }

            return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
        } elseif ( $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                   
                        $em = $this->getDoctrine()->getManager();
                        $news->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $news->setCreatedAt($date);
                        $news->setUpdatedAt($date);
                        $news->addSite($site);
                        $news->setEtat("yes");

        
                        $entityManager->persist($news);
                        $entityManager->flush();
                        $message=$translator->trans(' News added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'news',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block news!');
                        $notif->setMessage('adding block news to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_news', array(
                            'id'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a news block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add a  news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),

            ]);
        
    }
    


    /**
     * @Route("/{id}/updatenews/update/{idsite}", name="cms_news_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,TranslatorInterface $translator,NewsRepository $sr, $id,$idsite,UserInvitRepository $ui): Response
    {
        $news =$sr->find($id);
        
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $news->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('News  updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'news',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_news_index');
            }

            return $this->render('news/update.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
        } elseif ($idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $news->setUpdatedAt($date);
                        $news->addSite($site);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans(' News  updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'news',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('editing block news!');
                        $notif->setMessage('editing block news to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_news', array(
                            'id'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit your news block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to edit your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('news/update.html.twig', [
            'news' => $news,
            'form' => $form->createView(),

            ]);
        
    }
    /**
     * @Route("/{id}/deletenews/delete/{idsite}/#", name="cms_news_delete")
     */
    
    public function delete($id, NewsRepository $sr,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $news =$sr->find($id);
                    $em->remove($news);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block news!');
                    $notif->setMessage('deleting block news to website named'.$site->getName().' by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_news', array(
                    'id'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $news =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$news->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($news);
                $em->flush();
                return $this->redirectToRoute('cms_news_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }




    public function ViewNewsInvitPage($id,UserInvitRepository $ui,NewsRepository $pr,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();
     
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $news=$site->getNews();
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/Viewnews.html.twig', array(
                   
          
            'list_news' => $news,
            'idsite'=>$id,
           
            
        ));
           }
           else{

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access to your news  block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access to your news block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }

  
}