<?php

namespace App\Controller;

use App\Entity\Footer;
use App\Entity\Site;
use App\Form\FooterType;
use App\Repository\FooterRepository;
use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\UserRepository;

/**
 * @Route("/cms/footer")
 */
class FooterController extends AbstractController
{

    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }


    /**
     * @Route("/", name="cms_footer_index")
     */
    public function index(FooterRepository $ftr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $footer=$ftr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('footer/index.html.twig', [
            'list_footer' =>$footer,
        ]);
    }

    /**
     * @Route("/new/blockfooterview/{idsite}/#view", name="cms_footer_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idsite,UserInvitRepository $ui ,TranslatorInterface $translator,UserRepository $userrep): Response
    {
    $footer = new Footer();
        $form = $this->createForm(FooterType::class, $footer);
        $form->handleRequest($request);
        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $footer->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $footer->setCreatedAt($date);
                $footer->setUpdatedAt($date);
                $footers=$this->getDoctrine()->getRepository(Footer::class)->findAll();
            foreach($footers as $ft){
                if($ft->getSites()->getName()==$footer->getSites()->getName()){
                    $entityManager->remove($ft); 
                }
            }

                $entityManager->persist($footer);
                $entityManager->flush();
                $message=$translator->trans('New Block Footers added successfully To Your Site');

                $this->get('session')->getFlashBag()->add(
                    'footer',
                    array(
                 
                  'message' => $message
              )
                );
                return $this->redirectToRoute('cms_footer_index');
            }

            return $this->render('footer/new.html.twig', [
          'footer' => $footer,
          'form' => $form->createView(),
      ]);
        }
        elseif($idsite!="null"){

          $user=$this->getUser();
      $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
      $userinvit=$ui->findByCreatedby($site->getAuthor());
      $test= false;
      $tt=null;
      foreach($userinvit as $usr){
          if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
              $test=true;
              $tt=$usr;
          }
      }

      if ($test==true) {
          if (($tt->hasRoles('ROLE_MODERATOR'))) {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $footer->setCreatedby($user->getUsername());
                  $date = new \DateTime('now');
                  $footer->setCreatedAt($date);
                  $footer->setUpdatedAt($date);
                  $site->addFooter($footer);
                  $footers=$this->getDoctrine()->getRepository(Footer::class)->findAll();
            foreach($footers as $ft){
                if($ft->getSites()->getName()==$footer->getSites()->getName()){
                    $entityManager->remove($ft); 
                }
            }

                  $entityManager->persist($footer);
                  $entityManager->flush();
                  $message=$translator->trans('New Block Footers added successfully To Your Site');

                  $this->get('session')->getFlashBag()->add(
                    'footer',
                    array(
                 
                  'message' => $message
              )
                );
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('Adding block Footer!');
                $notif->setMessage('Adding new block Footer by '.$this->getUser().'for your website '. $site->getName());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                  return $this->redirectToRoute('cms_invit_view_footer', array(
                      'id' => $idsite,
                  ));
              }

          }
          else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add new  footer block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                 return $this->render('Invits/page/erreur_permissions.html.twig');

          }
      }
      else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add new  footer block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);

        return $this->render('Invits/page/erreur_permissions.html.twig');

      }

      return $this->render('footer/new.html.twig', [
          'footer' => $footer,
          'form' => $form->createView(),
      ]);


        }







        /*

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $footer->setCreatedBy($user->getUsername());
             $date = new \DateTime('now');
            $footer->setCreatedAt($date);
            $footer->setUpdatedAt($date);
            $footers=$this->getDoctrine()->getRepository(Footer::class)->findAll();
            foreach($footers as $ft){
                if($ft->getSites()->getName()==$footer->getSites()->getName()){
                    $entityManager->remove($ft); 
                }
            }


            $entityManager->persist($footer);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add(
                'footer',
                array(
                   
                    'message' => 'New footer has been added successfully.'
                )
            );
            return $this->redirectToRoute('cms_footer_index');
          
        }

        return $this->render('footer/new.html.twig', [
            'footer' => $footer,
            'form' => $form->createView(),
        ]);*/
    }
    /**
     * @Route("/{id}/updateviewfooter/{idsite}/#update", name="cms_footer_update", methods={"GET","POST"})
     */
    public function update(Request $request, TranslatorInterface $translator,FooterRepository $ftr,$id,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
    $footer =$ftr->find($id);
        $form = $this->createForm(FooterType::class, $footer);
        $form->handleRequest($request);
        if ($idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
         
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $footer->setUpdatedAt($date);
               

                $entityManager->flush();
                $message=$translator->trans('Block Footers Updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'footer',
                    array(
                 
                  'message' => $message
              )
                );
                return $this->redirectToRoute('cms_footer_index');
            }

            return $this->render('footer/update.html.twig', [
          'footer' => $footer,
          'form' => $form->createView(),
      ]);
        }
        elseif($idsite!="null"){

          $user=$this->getUser();
      $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
      $userinvit=$ui->findByCreatedby($site->getAuthor());
      $test= false;
      $tt=null;
      foreach($userinvit as $usr){
          if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
              $test=true;
              $tt=$usr;
          }
      }

      if ($test==true) {
          if (($tt->hasRoles('ROLE_MODERATOR'))) {
              if ($form->isSubmitted() && $form->isValid()) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $user = $this->getUser();
           
                  $em = $this->getDoctrine()->getManager();
                  $date = new \DateTime('now');
                  $footer->setUpdatedAt($date);
                  //$site->addFooter($footer);
                  $site->addFooter($footer);


                  $entityManager->flush();
                  $message=$translator->trans('Block Footers Updated successfully');

                  $this->get('session')->getFlashBag()->add(
                    'footer',
                    array(
                 
                  'message' => $message
              )
                );
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('updating  block Footer!');
                $notif->setMessage('block footer  for your website '.  $site->getName() .'has been updated by '. $this->getUser());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);

                  return $this->redirectToRoute('cms_invit_view_footer', array(
                      'id' => $idsite,
                  ));
              }

          }
          else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to update footer block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                 return $this->render('Invits/page/erreur_permissions.html.twig');

          }
      }
      else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to update footer block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);

        return $this->render('Invits/page/erreur_permissions.html.twig');

      }

      return $this->render('footer/update.html.twig', [
          'footer' => $footer,
          'form' => $form->createView(),
      ]);


        }
    }
    /**
     * @Route("/{id}/deleteviewfooter/{idsite}/#delete", name="cms_footer_delete")
     */
    public function delete($id,FooterRepository $ftrrep,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {   if ($idsite !="null") {
        $em=$this->getDoctrine()->getManager();

        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach ($userinvit as $usr) {
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                $test=true;
                $tt=$usr;
            }
        }
        if ($test==true) {
            if (($tt->hasRoles('ROLE_MODERATOR'))) {
                $footer =$ftrrep->find($id);
                $em->remove($footer);
                $em->flush();
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('deleting  block Footer!');
                $notif->setMessage('block footer  for your website '.  $site->getName() .'has been deleted by '. $this->getUser());
                $notif->setLink('https://Youdev-it.com/');
               
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->redirectToRoute('cms_invit_view_footer', array(
                'id' => $idsite,
            ));
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete   footer block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete  footer block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    } else {
        $footer =$ftrrep->find($id);
     

        if (($this->getUser()->getUsername()==$footer->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
            $em=$this->getDoctrine()->getManager();

            $em->remove($footer);
            $em->flush();
            return $this->redirectToRoute('cms_footer_index');
        }
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete footer block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

    }
    }

}
