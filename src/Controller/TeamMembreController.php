<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Site;
use App\Entity\TeamMembre;
use App\Form\TeamMembreType;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\TeamMembreRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/teamMembre")
 */
class TeamMembreController extends AbstractController
{
    private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    
    /**
     * @Route("/", name="cms_team_membre_index")
     */
    public function index(TeamMembreRepository $team): Response
    {
        $em=$this->getDoctrine()->getManager();
        $teams=$team->findByCreatedby($this->getUser()->getUserName());
        return $this->render('team_membre/index.html.twig', [
            'list_teams' =>$teams,
        ]);
    }

    /**
     * @Route("/new/eamblock/{idpage}/new/{idsite}", name="cms_team_membre_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $team = new TeamMembre();
        $form = $this->createForm(TeamMembreType::class, $team);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $team->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $team->setCreatedAt($date);
                $team->setUpdatedAt($date);
           

                $entityManager->persist($team);
                $entityManager->flush();
                $message=$translator->trans('New Block team added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'team',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_team_membre_index');
            }

            return $this->render('team_membre/new.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $team->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $team->setCreatedAt($date);
                        $team->setUpdatedAt($date);
                        $team->setPage($page);
                        $page->addTeamMembre($team);
                   
        
                        $entityManager->persist($team);
                        $entityManager->flush();
                        $message=$translator->trans('New Block team added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'team',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('Adding block team & membre for page!');
                        $notif->setMessage('adding block team & membre for  page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_team', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a team & membre block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to add a team & membre block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('team_membre/new.html.twig', [
            'team' => $team,
            'form' => $form->createView(),

            ]);

    }
    /**
     * @Route("/{id}/update/teaminvit/{idpage}/update/{idsite}", name="cms_team_membre_update", methods={"GET","POST"})
     */
    public function update(UserRepository $userrep,TranslatorInterface $translator,Request $request, TeamMembreRepository $sr, $id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $team =$sr->find($id);
        $form = $this->createForm(TeamMembreType::class, $team);
        $form->handleRequest($request);

        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $team->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans(' Block team has been updaed successfully ');

                $this->get('session')->getFlashBag()->add(
                    'team',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_team_membre_index');
            }

            return $this->render('team_membre/update.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $team->setUpdatedAt($date);
                        $team->setPage($page);
                        $page->addTeamMembre($team);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans(' Block team has been updaed successfully ');

                        $this->get('session')->getFlashBag()->add(
                            'team',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                        $notif = $this->manager->createNotification('updating block team & membre for page!');
                        $notif->setMessage('updating block team & membre for  page  to website named'.$site->getName().' by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                        return $this->redirectToRoute('cms_invit_view_team', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to edit a team & membre block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a team & membre block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('team_membre/update.html.twig', [
            'team' => $team,
            'form' => $form->createView(),

            ]);
    }
    /**
     * @Route("/{id}/delete/teaminvitblock/{idpage}/delete/{idsite}", name="cms_team_membre_delete")
     */
    public function delete($id,TeamMembreRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $team =$sr->find($id);
                    $em->remove($team);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleing block team & membre for page!');
                    $notif->setMessage('deleting block team & membre for  page  to website named'.$site->getName().' by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
            
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_team', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {

                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                           
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete a team & membre block for your website '.$site->getName());
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
        
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {

                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete a team & membre block for your website '.$site->getName());
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
    
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $team =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$team->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($team);
                $em->flush();
                return $this->redirectToRoute('cms_team_membre_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to delete a team & membre block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }





    public function ViewTeamInvitPage(UserRepository $userrep,$id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $teams=$page->getTeamMembres();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewteam.html.twig', array(
                   
          
            'list_teamMembres' => $teams,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                   
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access a team & membre block for your website '.$site->getName());
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{

        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
               
    $notif = $this->manager->createNotification('OOps!');
    $notif->setMessage('unauthorized user wants to access a team & membre block for your website '.$site->getName());
    $notif->setLink('https://Youdev-it.com/');
    // or the one-line method :
    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

    // you can add a notification to a list of entities
    // the third parameter `$flush` allows you to directly flush the entities
    $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
}