<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Page;
use App\Entity\Site;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Repository\HeaderRepository;
use App\Repository\PageRepository;
use App\Repository\UserRepository;

use App\Repository\UserInvitRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/cms/contactblock")
 */
class ContactController extends AbstractController
{

private $manager;

    public function __construct(NotificationManager $manager)
    {
        $this->manager=$manager;
       
    }
    /**
     * @Route("/", name="cms_contactblock_index")
     */
    public function index(ContactRepository $fr): Response
    {
        $em=$this->getDoctrine()->getManager();
        $contacts=$fr->findByCreatedby($this->getUser()->getUserName());
        return $this->render('contact/index.html.twig', [
            'list_contacts' =>$contacts,
        ]);
    }

    /**
     * @Route("/new/contactnew/{idpage}/new/{idsite}", name="cms_contactblock_new", methods={"GET","POST"})
     */
    public function new(Request $request,$idpage,$idsite,UserInvitRepository $ui,TranslatorInterface $translator,UserRepository $userrep): Response
    {
        $contact = new Contact();
        
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $contact->setCreatedby($user->getUsername());
                $date = new \DateTime('now');
                $contact->setCreatedAt($date);
                $contact->setUpdatedAt($date);
           

                $entityManager->persist($contact);
                $entityManager->flush();
                $message=$translator->trans('New Block contact added successfully To Your Page');

                $this->get('session')->getFlashBag()->add(
                    'contact',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_contactblock_index');
            }

            return $this->render('contact/new.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $contact->setCreatedby($user->getUsername());
                        $date = new \DateTime('now');
                        $contact->setCreatedAt($date);
                        $contact->setUpdatedAt($date);
                        $contact->setPages($page);
                        $page->addContact($contact);
                   
        
                        $entityManager->persist($contact);
                        $entityManager->flush();
                        $message=$translator->trans('New Block contact added successfully To Your Page');

                        $this->get('session')->getFlashBag()->add(
                            'contact',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                        $langue= $request->getSession()->get('_locale');
                        if ($langue=="fr") {
                            $notif = $this->manager->createNotification('Ajout d\' un bloc contact  !');
                            $notif->setMessage('Nouvelle block contact ajouter a votre page par  '. $this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } elseif ($langue=="ar") {
                            $notif = $this->manager->createNotification(' إضافة  الاتصال!');
                            $notif->setMessage($this->getUser().'إضافة كتلة اتصال جديدة إلى صفحتك بواسطة     ');
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } elseif ($langue=="de") {
                            $notif = $this->manager->createNotification('Block kontakt hinzufügen !');
                            $notif->setMessage('neuer Blockkontakt zu unserer Seite von '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } else {
                            $notif = $this->manager->createNotification('Adding block contact!');
                            $notif->setMessage('new block contact has been added to your website page by '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        }
                        return $this->redirectToRoute('cms_invit_view_contact', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to add a new contact block for you');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to add a new contact block for you');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('contact/new.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),

            ]);
        
    }
    


    /**
     * @Route("/{id}/updatecontact/{idpage}/update/{idsite}", name="cms_contactblock_update", methods={"GET","POST"})
     */
    
    public function update(UserRepository $userrep,Request $request,TranslatorInterface $translator,ContactRepository $sr, $id,$idpage,$idsite,UserInvitRepository $ui): Response
    {
        $contact =$sr->find($id);
        
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($idpage=="null" && $idsite=="null") {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $contact->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('Block contact updated successfully');

                $this->get('session')->getFlashBag()->add(
                    'contact',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_contactblock_index');
            }

            return $this->render('contact/update.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
        } elseif ($idpage !="null" && $idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
            $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $page=$this->getDoctrine()->getRepository(Page::class)->find($idpage);
                   
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $contact->setUpdatedAt($date);
                        $contact->setPages($page);
                        $page->addContact($contact);
                   
        
                        $entityManager->flush();
                        $message=$translator->trans('Block contact updated successfully');

                        $this->get('session')->getFlashBag()->add(
                            'contact',
                            array(
                           
                            'message' => $message
                        )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                            $notif = $this->manager->createNotification('updating block contact!');
                            $notif->setMessage('Your block contact named '. $contact->getName().' has been updating successfully by '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        
                        return $this->redirectToRoute('cms_invit_view_contact', array(
                            'id' => $idpage,
                            'idsite'=> $idsite,
                        ));
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to update your contact block');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to update your contact block');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('contact/update.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),

            ]);
        
    }
    /**
     * @Route("/{id}/deletecontact/{idpage}/delete/{idsite}/#", name="cms_contactblock_delete")
     */
    
    public function delete($id, ContactRepository $sr,$idpage,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        if ($idpage !="null" && $idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
                    $contact =$sr->find($id);
                    $em->remove($contact);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('deleting block contact!');
                    $notif->setMessage('Your block contact named '. $contact->getName().' has been deleted successfully by '.$this->getUser());
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->redirectToRoute('cms_invit_view_contact', array(
                    'id' => $idpage,
                    'idsite'=> $idsite,
                ));
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                    $notif = $this->manager->createNotification('OOps!');
                    $notif->setMessage('unauthorized user wants to delete your contact block');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
                $notif = $this->manager->createNotification('OOps!');
                $notif->setMessage('unauthorized user wants to delete your contact block');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $contact =$sr->find($id);
         
   
            if (($this->getUser()->getUsername()==$contact->getCreatedby()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($contact);
                $em->flush();
                return $this->redirectToRoute('cms_contactblock_index');
            }
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to delete your contact block');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

        }
    }




    public function ViewContactInvitPage($id,$idsite,UserInvitRepository $ui, HeaderRepository $sr,PageRepository $pr,UserRepository $userrep): Response
    {
        $em=$this->getDoctrine()->getManager();
        $page =$pr->find($id);
        $contacts=$page->getContacts();
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewcontact.html.twig', array(
                   
          
            'list_contacts' => $contacts,
            'idsite'=>$idsite,
            'idpage'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
            $notif = $this->manager->createNotification('OOps!');
            $notif->setMessage('unauthorized user wants to access to  your contact block');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                       
        $notif = $this->manager->createNotification('OOps!');
        $notif->setMessage('unauthorized user wants to access to  your contact block');
        $notif->setLink('https://Youdev-it.com/');
        // or the one-line method :
        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');

        // you can add a notification to a list of entities
        // the third parameter `$flush` allows you to directly flush the entities
        $this->manager->addNotification(array($userrecivernot), $notif, true);
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    }
}