<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Categories;
use App\Entity\FAQ;
use App\Entity\MotsCles;
use App\Entity\Page;
use App\Entity\Services;
use App\Entity\User;
use App\Entity\Site;
use App\Entity\Themes;
use App\Repository\CategoriesRepository;
use App\Repository\ArticlesRepository;
use App\Repository\CommentaireRepository;
use App\Repository\FAQRepository;
use App\Repository\MenuRepository;
use App\Repository\GalleryRepository;
use App\Repository\ContactRepository;
use App\Repository\NewsRepository;
use App\Repository\TeamMembreRepository;
use App\Repository\MotsClesRepository;
use App\Repository\PageRepository;
use App\Repository\ServicesRepository;
use App\Repository\SiteRepository;
use App\Repository\ThemesRepository;
use App\Repository\UserInvitRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Types\ObjectType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\Form\Factory\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Fos\UserBundle\Controller\ProfileController;
use Knp\Component\Pager\PaginatorInterface;
use phpDocumentor\Reflection\File;
use PhpParser\Node\Expr\BinaryOp\Equal;
use Swift_Mailer;
use Symfony\Component\HttpFoundation\File\File as FileFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\Argument\ServiceLocator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints\EqualTo;
use Stripe\Stripe;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent as tt;
use FOS\UserBundle\Event\FormEvent as tt2;
use FOS\UserBundle\Event\GetResponseUserEvent as tt3;
use FOS\UserBundle\Form\Factory\FactoryInterface as tt4;
use FOS\UserBundle\FOSUserEvents as tt5;
use FOS\UserBundle\Model\UserInterface as tt6;
use FOS\UserBundle\Model\UserManagerInterface as tt7;
use Mgilet\NotificationBundle\Entity\NotifiableNotification;
use Mgilet\NotificationBundle\Entity\Repository\NotifiableEntityRepository;
use Mgilet\NotificationBundle\Entity\Repository\NotifiableNotificationRepository;
use Mgilet\NotificationBundle\Entity\Repository\NotificationRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Mgilet\NotificationBundle\NotifiableDiscovery;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as mtt;
use Symfony\Component\DependencyInjection\Container;

class DefaultController extends AbstractController
{ private $formFactory;
private $manager;
protected $container;
protected $discovery;
protected $om;
protected $dispatcher;
protected $notifiableRepository;
protected $notificationRepository;
protected $notifiableNotificationRepository;

    public function __construct(FormFactory $formFactory,NotificationManager $manager,Container $container, NotifiableDiscovery $discovery)
    {
        $this->formFactory = $formFactory;
        $this->manager=$manager;
        $this->container = $container;
        $this->discovery = $discovery;
        $this->om = $container->get('doctrine.orm.entity_manager');
        $this->dispatcher = $container->get('event_dispatcher');
        $this->notifiableRepository = $this->om->getRepository('MgiletNotificationBundle:NotifiableEntity');
        $this->notificationRepository = $this->om->getRepository('MgiletNotificationBundle:Notification');
        $this->notifiableNotificationRepository = $this->om->getRepository('MgiletNotificationBundle:NotifiableNotification');
    }
    
    public function index(CategoriesRepository $catrep,MotsClesRepository $motclerep,UserInvitRepository $userinvitrep,FAQRepository $faqrep,PageRepository $pagerep,Request $request,ChartBuilderInterface  $chartBuilder,SiteRepository $siterep,ArticlesRepository $artrep,NewsRepository $newsrep,GalleryRepository $galleryrep,ContactRepository $contactrep,TeamMembreRepository $team_membre): Response
    {
        $request->getSession()->set('_locale', 'en');
        $sites = $siterep->findAll();
        $posts=$artrep->findByUsers($this->getUser());
        $cats=$catrep->findByCreatedby($this->getUser()->getusername());
        $siteuser=$siterep->findByAuthor($this->getUser()->getusername());
        $pageuser=$pagerep->findByCreatedby($this->getUser()->getusername());
        $faquser=$faqrep->findByCreatedby($this->getUser()->getusername());
        $motclesuser=$motclerep->findByCreatedby($this->getUser()->getusername());
        $invitsent=$userinvitrep->findByCreatedby($this->getUser()->getusername());
        $invitreceived=$userinvitrep->findByReceiverinvit($this->getUser());
        $newsuser=$newsrep->findByCreatedby($this->getUser()->getusername());
        $contactuser=$contactrep->findByCreatedby($this->getUser()->getusername());
        $galleryuser=$galleryrep->findByCreatedby($this->getUser()->getusername());
        $teammembreuser=$team_membre->findByCreatedby($this->getUser()->getusername());
 
       
        
        $catname = [];
        $catcount = [];
        foreach ($cats as $cat) {
          
            $catname[]=$cat->getName();
            $catcount[]=count($cat->getArticles());
        }
 

      $postcount_tab=$artrep->CountByDate($this->getUser());
      $dates=[];
      $count_post=[];
      foreach($postcount_tab as $post)
      {
          $dates[]= $post["createddate"];
          $count_post[]= $post["countpost"];
      }
      $sitecount_tab=$siterep->CountSitesByDate($this->getUser()->getUsername());
      $dates_site=[];
      $count_site=[];
      foreach($sitecount_tab as $site)
      {
          $dates_site[]= $site["createddate"];
          $count_site[]= $site["countsite"];
      }
      $pagecount_tab=$pagerep->CountPagesByDate($this->getUser()->getUsername());
      $dates_page=[];
      $count_page=[];
      foreach($pagecount_tab as $page)
      {
          $dates_page[]= $page["createddate"];
          $count_page[]= $page["countpage"];
      }

  return $this->render('default/index.html.twig', [
            'posts' => $posts,
            'cats' => $cats,
            'siteuser' => $siteuser,
            'pageuser' => $pageuser,
            'motclesuser' => $motclesuser,
            'faquser' => $faquser,
            'invitsent' => $invitsent,
            'invitreceived' => $invitreceived,
            'newsuser' => $newsuser,
            'contactuser' => $contactuser,
            'galleryuser' => $galleryuser,
            'teammembreuser' => $teammembreuser,
            'catname' => json_encode($catname),
            'catcount' => json_encode($catcount),
            'dates' => json_encode($dates),
            'countpost' => json_encode($count_post),
            'dates_site' => json_encode($dates_site),
            'count_site' => json_encode($count_site),
            'dates_page' => json_encode($dates_page),
            'count_page' => json_encode($count_page),
        ]);
    }
    public function index1(): Response
    {
       

  return $this->render('default/index1.html.twig', [
           
        ]);
    }
    public function profil(): Response
    {
         $profil=$this->getUser() ;
            $em = $this->getDoctrine()->getManager();
 
           $user = $em->getRepository(User::class)->find($profil->getId());
   
    return $this->render('default/profil.html.twig',array('profil'=>$user));
    
    }
    public function  AjouterSite(\Symfony\Component\HttpFoundation\Request $request):Response
    {
        $site = new site();
        $form = $this->createForm('App\Form\SiteType',$site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $site->setAuthor($user->getUsername());
             $date = new \DateTime('now');
            $site->setCreatedAt($date);
            $site->setUpdatedAt($date);
            $site->setPublished(false);
   
      
            $em->persist($site);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
    'site',
    array(
       
        'message' => 'New site has been added successfully.'
    )
);
            return $this->redirectToRoute('cms_site_list');
        }

        return $this->render('default/ajouter_site.html.twig', array(
           
            'form' => $form->createView(),
        ));
        
    }
    public function ListSite(SiteRepository $siteRepository,ServicesRepository $serv,UserInvitRepository $userinvit): Response
    {
        $em=$this->getDoctrine()->getManager();
        $user = $this->getUser();
        $username = $user->getUsername();
       $site =$siteRepository->findByAuthor($username);
       $siteinvit =$userinvit->findByReceiverinvit($user->getId());

       $services=$serv->findByCreatedby($user->getUsername());
    return $this->render('default/List_site.html.twig',array('site'=>$site,'services'=>$services,'siteinvit'=>$siteinvit));
    
    }
    public function DeleteSite(SiteRepository $siteRepository,FAQRepository $faqrep,PageRepository $pagerep ,$id,$idsite,UserInvitRepository $ui,Request $request,UserRepository $userrep): Response
    {
        if ($idsite !="null") {
            $em=$this->getDoctrine()->getManager();

            $user=$this->getUser();
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
           $pages=$site->getPages();
            $page=null;
            foreach($pages as $value){
                if($value->getName()=='Home'){
                    $page=$value;
                }
            }
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $test= false;
            $tt=null;
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if (($tt->hasRoles('ROLE_MODERATOR'))) {
                    $site =$siteRepository->find($id);
                    $em->remove($site);
                    $em->remove($page);
                    $em->flush();
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                    $langue= $request->getSession()->get('_locale');
                    if ($langue=="fr") {
                        $notif = $this->manager->createNotification('suppression Site!');
                        $notif->setMessage('Votre site dont le nom est '.$site->getName().' est supprimer par'.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="ar") {
                        $notif = $this->manager->createNotification('حذف موقع!');
                        $notif->setMessage($this->getUser().'تم حذف موقعك بواسطة');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="de") {
                        $notif = $this->manager->createNotification('Löschen der Site !');
                        $notif->setMessage('Ihre Site, deren Name ist'. $site->getName().'wird gelöscht von'.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } else {
                        $notif = $this->manager->createNotification('Site Deleted!');
                        $notif->setMessage('Your WebSite named'.$site->getName().' has been deleted by '.$this->getUser());
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    }
                    return $this->redirectToRoute('cms_site_list');
                } else {
                    $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                    $langue= $request->getSession()->get('_locale');
                    if ($langue=="fr") {
                        $notif = $this->manager->createNotification('Droit uilistion!');
                        $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous information de site de manière non authorisèe');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="ar") {
                        $notif = $this->manager->createNotification('  حذاري!');
                        $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } elseif ($langue=="de") {
                        $notif = $this->manager->createNotification('Nutzungsrecht!');
                        $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Site-Informationen auf nicht autorisierte Weise ändern ');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    } else {
                        $notif = $this->manager->createNotification('unauthorized User!');
                        $notif->setMessage('the are a user named '.$this->getUser().'want to delete your site information in unauthorized way');
                        $notif->setLink('https://Youdev-it.com/');
                        // or the one-line method :
                        // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
              
                        // you can add a notification to a list of entities
                        // the third parameter `$flush` allows you to directly flush the entities
                        $this->manager->addNotification(array($userrecivernot), $notif, true);
                    }
                    return $this->render('Invits/page/erreur_permissions.html.twig');
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                $langue= $request->getSession()->get('_locale');
                if ($langue=="fr") {
                    $notif = $this->manager->createNotification('Droit uilistion!');
                    $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut supprimer vous information de site de manière non authorisèe');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="ar") {
                    $notif = $this->manager->createNotification('  حذاري!');
                    $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="de") {
                    $notif = $this->manager->createNotification('Nutzungsrecht!');
                    $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Site-Informationen auf nicht autorisierte Weise ändern ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } else {
                    $notif = $this->manager->createNotification('unauthorized User!');
                    $notif->setMessage('the are a user named '.$this->getUser().'want to delete your site information in unauthorized way');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                }
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
            $pages=$site->getPages();
            $page=null;
if($pages != null){
            foreach($pages as $value){
                if($value->getName()=='Home'){
                    $page=$value;
                }
            }}
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());

            if (($this->getUser()->getUsername()==$site->getAuthor()) || $this->getUser()->hasRole('ROLE_ADMIIN')) {
                $em=$this->getDoctrine()->getManager();

                $em->remove($site);
if($page != null){
                $em->remove($page);}

                $em->flush();
                return $this->redirectToRoute('cms_site_list');
            }
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'want to delete your site information in unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }


    }
    public function UpdateSite(TranslatorInterface $translator,Request $request,SiteRepository $siteRepository,$id,$idsite,UserInvitRepository $ui,UserRepository $userrep): Response
    {
        $site = $siteRepository->find($id);
        $form = $this->createForm('App\Form\SiteType', $site);
        $form->handleRequest($request);

        if ($idsite=="null") {
            if ($form->isSubmitted()) {
                $entityManager = $this->getDoctrine()->getManager();
                $user = $this->getUser();
           
                $em = $this->getDoctrine()->getManager();
                $date = new \DateTime('now');
                $site->setUpdatedAt($date);
           

                $entityManager->flush();
                $message=$translator->trans('Site Informations has been updated successfully');
                $this->get('session')->getFlashBag()->add(
                    'site',
                    array(
                   
                    'message' => $message
                )
                );
                return $this->redirectToRoute('cms_site_list');
            }

            return $this->render('default/Modifier_site.html.twig', array(
           
                'form' => $form->createView(),
            ));
        } elseif ($idsite!="null") {
            $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
            $userinvit=$ui->findByCreatedby($site->getAuthor());
            $user=$this->getUser();
            $test= false;
            $tt=null;
              
            foreach ($userinvit as $usr) {
                if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite) {
                    $test=true;
                    $tt=$usr;
                }
            }
            if ($test==true) {
                if ($tt->hasRoles('ROLE_MODERATOR')) {
                    if ($form->isSubmitted()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $user = $this->getUser();
                        $em = $this->getDoctrine()->getManager();
                        $date = new \DateTime('now');
                        $site->setUpdatedAt($date);
                

                        $entityManager->flush();
                        $message=$translator->trans('Site Informations has been updated successfully');
                        $this->get('session')->getFlashBag()->add(
                            'site',
                            array(
                   
                    'message' => $message
                )
                        );
                        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                        $langue= $request->getSession()->get('_locale');
                        if ($langue=="fr") {
                            $notif = $this->manager->createNotification('Modification Pour Site!');
                            $notif->setMessage('Votre site dont le nom est '.$site->getName().' est modifiè par'.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } elseif ($langue=="ar") {
                            $notif = $this->manager->createNotification(' تعديل للموقع!');
                            $notif->setMessage($this->getUser().'تم تعديل موقعك بواسطة');
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } elseif ($langue=="de") {
                            $notif = $this->manager->createNotification('Änderung für WebSite!');
                            $notif->setMessage('Ihre Site, deren Name ist'. $site->getName().'wird geändert von'.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        } else {
                            $notif = $this->manager->createNotification('Site Updated!');
                            $notif->setMessage('Your WebSite named'.$site->getName().' has been updated successfully by '.$this->getUser());
                            $notif->setLink('https://Youdev-it.com/');
                            // or the one-line method :
                            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
                  
                            // you can add a notification to a list of entities
                            // the third parameter `$flush` allows you to directly flush the entities
                            $this->manager->addNotification(array($userrecivernot), $notif, true);
                        }
        
                      
                        return $this->redirectToRoute('cms_site_list');
                    }
                }
            } else {
                $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
                $langue= $request->getSession()->get('_locale');
                if ($langue=="fr") {
                    $notif = $this->manager->createNotification('Droit uilistion!');
                    $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous information de site de manière non authorisèe');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="ar") {
                    $notif = $this->manager->createNotification('  حذاري!');
                    $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } elseif ($langue=="de") {
                    $notif = $this->manager->createNotification('Nutzungsrecht!');
                    $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Site-Informationen auf nicht autorisierte Weise ändern ');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                } else {
                    $notif = $this->manager->createNotification('unauthorized User!');
                    $notif->setMessage('the are a user named '.$this->getUser().'want to update your site information in unauthorized way');
                    $notif->setLink('https://Youdev-it.com/');
                    // or the one-line method :
                    // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
          
                    // you can add a notification to a list of entities
                    // the third parameter `$flush` allows you to directly flush the entities
                    $this->manager->addNotification(array($userrecivernot), $notif, true);
                }
                return $this->render('Invits/page/erreur_permissions.html.twig');
            }
        } else {
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous information de site de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص الذي اسمه '.$this->getUser().'يريد تعديل معلومات موقعك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Site-Informationen auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'want to update your site information in unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            }
            return $this->render('Invits/page/erreur_permissions.html.twig');
        }
    
        return $this->render('default/Modifier_site.html.twig', array(
           
            'form' => $form->createView(),
        ));
    

    
    }
    public function  AddSite(TranslatorInterface $translator,\Symfony\Component\HttpFoundation\Request $request,ServicesRepository $serv):Response
    {
        $site = new site();
        $sites=$this->getDoctrine()->getRepository(Site::class)->findAll();
        foreach($sites as $value){
            if((strtoupper($value->getName()) == strtoupper($_POST['name']))){
                $message=$translator->trans('site with name');
                $message2=$translator->trans('is already exist');
              
                $this->get('session')->getFlashBag()->add(
                    'site',
                    array(
                       
                        'message' => $message. $value->getName() .$message2
                    )
                );
                            return $this->redirectToRoute('cms_site_list');  
            }
        }


       
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
           $site->setAuthor($user->getUsername());
           $site->setName($_POST['name']);
           $site->setDescription($_POST['description']);
           $site->setUrl($_POST['url']);
           $site->setTheme("default");
          
           /*
           $file=$_POST['file'];
           if ($file!=null) {
              // $fileName = md5(uniqid()) . '.' . $file->guessExtension();
               $imageDir = $this->container->getParameter('kernel.root_dir') . '/../public/uploads/img/experts';
               // Generate a unique name for the file before saving it
               $file->move($imageDir, $file);
               $site->setLogo($file);
               $site->setImageFile($file);
           }*/
             $date = new \DateTime('now');
            $site->setCreatedAt($date);
            $site->setUpdatedAt($date);
            $site->setPublished(false);
            $page=new Page();
            $page->setName("Home");
            $page->setDescription("Home");
            $page->setContent("welcom to ".$site->getName());
           ;
            $page->setCreatedAt($date);
            $page->setUpdatedAt($date);
            $page->setPublished(false);
            $page->setCreatedby($user->getUsername());
            $page->addSite($site);
            $site->addPage($page);
      
            $em->persist($site);
            $em->persist($page);

            $em->flush();
            $message1=$translator->trans('New site has been added successfully');

            $this->get('session')->getFlashBag()->add(
    'site',
    array(
       
        'message' => $message1
    )
);

   
            return $this->redirectToRoute('cms_site_list');
       

     
        
    }
    public function ModifySite(TranslatorInterface $translator,Request $request,SiteRepository $siteRepository,$id): Response
    { $site = $siteRepository->find($id);

        
            $user = $this->getUser();
           
            $em = $this->getDoctrine()->getManager();
             $date = new \DateTime('now');
            $site->setUpdatedAt($date);
            /*
            $site->setImageFile($_FILES['file1']);
            $file=$_POST['file'];
            if ($file!=null) {
                $fileName = md5(uniqid()) . '.' . (File)($file)->guessExtension();
                $imageDir = $this->container->getParameter('kernel.root_dir') . '/../public/uploads/img/experts';
                // Generate a unique name for the file before saving it
                $file->move($imageDir, $fileName);
                $site->setLogo($fileName);
                $site->setImageFile($fileName);
            }*/
            $site->setName($_POST['name1']);
            $site->setDescription($_POST['description1']);
            $site->setUrl($_POST['url1']);
              $date = new \DateTime('now');
            
            $em->flush();
            $message=$translator->trans('site updated sucessffully');
            $this->get('session')->getFlashBag()->add(
    'site',
    array(
       
        'message' => $message
    )
);
            return $this->redirectToRoute('cms_site_list');
     

     
    
    }
    public function PubSite(SiteRepository $siteRepository,$id): Response
    {
                $em=$this->getDoctrine()->getManager();
                $site=$siteRepository->find($id);
                if($site->getPublished()==true)
                $site->setPublished(false);
                else
                $site->setPublished(true);
               $em->flush();
                
               return $this->redirect($this->generateUrl('cms_site_list'));
    }
    public function ViewSite(ArticlesRepository $art,UserRepository $user,SiteRepository $siteRepository,MenuRepository $mn,$name,PaginatorInterface $paginator,FAQRepository $faq,Request $request,PageRepository $pagerep): Response
    {
                $em=$this->getDoctrine()->getManager();
                $site=$siteRepository->findOneByName($name);
                $pages=$site->getPages();
                $page=null;
                foreach($pages as $pr)
                {
                 if($pr->getName()=="Home"){
                     $page=$pr;
                 }
                }
                $pagination1=$faq->findBySites($site);
                $faqs = $paginator->paginate(
                    $pagination1, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    4 /*limit per page*/
                );
                $utilisateur=$user->findOneByUsername($site->getAuthor());
                $pagination2=$art->findByUsers($utilisateur);
                $cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();
                $posts = $paginator->paginate(
                    $pagination2, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    4 /*limit per page*/
                );
                // trouver les 3 post recent
                $recent=$art->findDESC();
                if ($site->getTheme()=="default"  || $site->getTheme()=="") {
                    return $this->render('frontend/fontsite/view_site.html.twig', array(
           
                    'site' => $site,
                    'rep' => $siteRepository,
                    'list_art'=> $posts,
                    'list_cat'=> $cat,
                    'recent'=> $recent,
                    'faqs'=> $faqs,
                    'page'=> $page,
      
                    
                ));
                } 
                elseif ($site->getTheme()=="template1") {
                    $posts = $paginator->paginate(
                        $pagination2, /* query NOT result */
                        $request->query->getInt('page', 1), /*page number*/
                        6 /*limit per page*/
                    );
                    return $this->render('frontend/fontsite/template1/view_site.html.twig', array(
           
                    'site' => $site,
                    'list_art'=> $posts,
                    'list_cat'=> $cat,
                    'recent'=> $recent,
                    'faqs'=> $faqs,
                    'page'=> $page,

                    
                ));
                }
                elseif ($site->getTheme()=="template2") {
                    $posts = $paginator->paginate(
                        $pagination2, /* query NOT result */
                        $request->query->getInt('page', 1), /*page number*/
                        6 /*limit per page*/
                    );
                    return $this->render('frontend/fontsite/template2/view_site.html.twig', array(
           
                    'site' => $site,
                    'list_art'=> $posts,
                    'list_cat'=> $cat,
                    'recent'=> $recent,
                    'faqs'=> $faqs,
                    'page'=> $page,

                    
                ));
                }
                elseif ($site->getTheme()=="template3") {
                    return $this->render('frontend/fontsite/template3/view_site.html.twig', array(
           
                    'site' => $site,
                    'list_art'=> $posts,
                    'list_cat'=> $cat,
                    'recent'=> $recent,
                    'faqs'=> $faqs,
                    'page'=> $page,

                    
                ));
                }     
                elseif ($site->getTheme()=="template4") {
                    return $this->render('frontend/fontsite/template4/view_site.html.twig', array(
           
                    'site' => $site,
                    'list_art'=> $posts,
                    'list_cat'=> $cat,
                    'recent'=> $recent,
                    'faqs'=> $faqs,
                    'page'=> $page,

                    
                ));
                } 
            
            
            
            
            
            
            
            }
        public function FaqPage(SiteRepository $siteRepository,PaginatorInterface $paginator,Request $request,FAQRepository $faq,$id): Response
        {
        $em=$this->getDoctrine()->getManager();
        $site=$siteRepository->find($id);
        $pagination=$faq->findBySites($site);
        $faqs = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        if ($site->getTheme()=="default" || $site->getTheme()=="") {
            return $this->render('frontend/fontsite/view_faq_front.html.twig', array(

        'site' => $site,
        'faqs'=> $faqs,

        ));
        }
       elseif ($site->getTheme()=="template1") {
            return $this->render('frontend/fontsite/template1/view_faq_front.html.twig', array(

        'site' => $site,
        'faqs'=> $faqs,

        ));
        }    
        elseif ($site->getTheme()=="template2") {
            return $this->render('frontend/fontsite/template2/view_faq_front.html.twig', array(

        'site' => $site,
        'faqs'=> $faqs,

        ));
        } 
        elseif ($site->getTheme()=="template3") {
            return $this->render('frontend/fontsite/template3/view_faq_front.html.twig', array(

        'site' => $site,
        'faqs'=> $faqs,

        ));
        } 
        elseif ($site->getTheme()=="template4") {
            return $this->render('frontend/fontsite/template4/view_faq_front.html.twig', array(

        'site' => $site,
        'faqs'=> $faqs,

        ));
        } 
    
    
    
    
    
    
    
    }
        public function BlogPage(SiteRepository $siteRepository,PaginatorInterface $paginator,Request $request,ArticlesRepository $art,UserRepository $user,$id): Response
        {
        $em=$this->getDoctrine()->getManager();
        $site=$siteRepository->find($id);
        $utilisateur=$user->findOneByUsername($site->getAuthor());
        $pagination=$art->findByUsers($utilisateur);
        $cat=$this->getDoctrine()->getRepository(Categories::class)->findAll();
        $posts = $paginator->paginate(
            $pagination, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            4 /*limit per page*/
        );
        $recent=$art->findDESC();
      
        if ($site->getTheme()=="default" || $site->getTheme()=="") {
            return $this->render('frontend/fontsite/view_blog_front.html.twig', array(

        'site' => $site,
        'list_art'=> $posts,
        'list_cat'=> $cat,
        'recent'=> $recent,

        ));
        }  
        elseif ($site->getTheme()=="template1") {
            $posts = $paginator->paginate(
                $pagination, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                6 /*limit per page*/
            );
            return $this->render('frontend/fontsite/template1/view_blog_front.html.twig', array(

        'site' => $site,
        'list_art'=> $posts,
        'list_cat'=> $cat,

        ));
        } 
        elseif ($site->getTheme()=="template2") {
            $posts = $paginator->paginate(
                $pagination, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                6 /*limit per page*/
            );
            return $this->render('frontend/fontsite/template2/view_blog_front.html.twig', array(

        'site' => $site,
        'list_art'=> $posts,
        'list_cat'=> $cat,

        ));
        }  
        elseif ($site->getTheme()=="template3") {
            return $this->render('frontend/fontsite/template3/view_blog_front.html.twig', array(

        'site' => $site,
        'list_art'=> $posts,
        'list_cat'=> $cat,
        'recent'=> $recent,


        ));
        }
        elseif ($site->getTheme()=="template4") {
            return $this->render('frontend/fontsite/template4/view_blog_front.html.twig', array(

        'site' => $site,
        'list_art'=> $posts,
        'list_cat'=> $cat,

        ));
        }     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    }
   
        public function AddSiteTheme(SiteRepository $siteRepository,ThemesRepository $themerep,$id): Response
        {
        $em=$this->getDoctrine()->getManager();
        $site=$siteRepository->find($id);
        $theme=$themerep->findAll();
            return $this->render('default/apply_theme.html.twig', array(

        'sites' => $site,
        'theme'=> $theme,
    
        ));
         }
         public function ApplyTheme(TranslatorInterface $translator,SiteRepository $siteRepository,ThemesRepository $themerep,$id,$idt): Response
         {
         $em=$this->getDoctrine()->getManager();
         $site=$siteRepository->find($idt);
         $theme=$themerep->find($id);
         $site->setTheme($theme->getName());
         $em->flush();
         $message=$translator->trans('new Theme  has been affected to your site');
         $this->get('session')->getFlashBag()->add(
            'sites',
            array(
               
                'message' => $message
            )
        );
                    return $this->redirectToRoute('cms_site_list');
          }

   //   crud for the themes proposed to user to add for tthiere site created



   public function ListThemes(ThemesRepository $themeRep): Response
   {
       $em=$this->getDoctrine()->getManager();
      $theme =$themeRep->findAll(); 
   return $this->render('Theme/list_theme.html.twig',array('themes'=>$theme));
   
   }
   public function DeleteThemes(ThemesRepository $themeRep,$id): Response
   {
       $em=$this->getDoctrine()->getManager();
      $theme =$themeRep->find($id); 
      $em->remove($theme);
           $em->flush();
           return $this->redirect($this->generateUrl('cms_list_themes'));    
   }


   public function  AddThemes(\Symfony\Component\HttpFoundation\Request $request):Response
   {
       $theme = new Themes();
       $form = $this->createForm('App\Form\ThemeType',$theme);
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
          
           $em = $this->getDoctrine()->getManager();
            $date = new \DateTime('now');
           $theme->setCreatedAt($date);
           $theme->setUpdatedAt($date);
  
     
           $em->persist($theme);
           $em->flush();
           
           $this->get('session')->getFlashBag()->add(
   'theme',
   array(
      
       'message' => 'New theme has been added successfully.'
   )
);
           return $this->redirectToRoute('cms_list_themes');
       }

       return $this->render('Theme/add_theme.html.twig', array(
          
           'form' => $form->createView(),
       ));
       
   }



   public function UpdateThemes(Request $request,ThemesRepository $themerep,$id): Response
   { $theme = $themerep->find($id);
       $form = $this->createForm('App\Form\ThemeType',$theme);
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
          
           $em = $this->getDoctrine()->getManager();
            $date = new \DateTime('now');
           $theme->setUpdatedAt($date);
  
     
         
           $em->flush();
           
           $this->get('session')->getFlashBag()->add(
   'theme',
   array(
      
       'message' => 'theme has been updated successfully.'
   )
);
           return $this->redirectToRoute('cms_list_themes');
       }

       return $this->render('Theme/update_theme.html.twig', array(
          
           'form' => $form->createView(),
       ));
   
   }
public function DemoThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if($theme->getName()=="template1"){
        return $this->render('Theme/template1/index.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template2"){
        return $this->render('Theme/template2/index.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template3"){
        return $this->render('Theme/template3/index.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template4"){
        return $this->render('Theme/template4/index.html.twig',
        
        array('id'=>$id)
        );
    }
}
public function AboutThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if($theme->getName()=="template1"){
        return $this->render('Theme/template1/about.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template2"){
        return $this->render('Theme/template2/about.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template3"){
        return $this->render('Theme/template3/about.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template4"){
        return $this->render('Theme/template4/about.html.twig',
        
        array('id'=>$id)
        );
    }
}
public function TeamThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if($theme->getName()=="template1"){
        return $this->render('Theme/template1/team.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template2"){
        return $this->render('Theme/template2/team.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template3"){
        return $this->render('Theme/template3/team.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template4"){
        return $this->render('Theme/template4/team.html.twig',
        
        array('id'=>$id)
        );
    }

}
public function ServiceThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if ($theme->getName()=="template1") {
        return $this->render(
            'Theme/template1/service.html.twig',
            array('id'=>$id)
        );
    } elseif ($theme->getName()=="template2") {
        return $this->render(
            'Theme/template2/service.html.twig',
            array('id'=>$id)
        );
    } elseif ($theme->getName()=="template3") {
        return $this->render(
            'Theme/template3/service.html.twig',
            array('id'=>$id)
        );
    } elseif ($theme->getName()=="template4") {
        return $this->render(
            'Theme/template4/service.html.twig',
            array('id'=>$id)
        );
    }
}
public function BlogThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if($theme->getName()=="template1"){
        return $this->render('Theme/template1/blog.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template2"){
        return $this->render('Theme/template2/blog.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template3"){
        return $this->render('Theme/template3/blog.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template4"){
        return $this->render('Theme/template4/blog.html.twig',
        
        array('id'=>$id)
        );
    }
}
public function ContactThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
    if($theme->getName()=="template1"){
        return $this->render('Theme/template1/contact.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template2"){
        return $this->render('Theme/template2/contact.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template3"){
        return $this->render('Theme/template3/contact.html.twig',
        
        array('id'=>$id)
        );
    }
    elseif($theme->getName()=="template4"){
        return $this->render('Theme/template4/contact.html.twig',
        
        array('id'=>$id)
        );
    }
}

public function ShopThemes(ThemesRepository $themerep,$id):Response
{
    $theme=$themerep->find($id);
  
    
        return $this->render('Theme/template2/shop.html.twig',
        
        array('id'=>$id)
        );
  
  
}


public function SendMail( TranslatorInterface $translator,\Swift_Mailer $mailer,$id,$ids,UserRepository $userrep,SiteRepository $st):Response
{
$site=$st->find($ids);
$user=$userrep->findOneByUsername($site->getAuthor());
    $message = (new \Swift_Message())
    ->setFrom($_POST['email'])
    ->setSubject($_POST['subject'])
    ->setContentType('text/html')
    ->setTo("alifadhel619@gmail.com");
   // ->setBody($_POST['message'])
   $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
   $img1 = $message->embed(\Swift_Image::fromPath('images/ico-facebook.png'));
   $img2 = $message->embed(\Swift_Image::fromPath('images/ico-twitter.png'));
   $img3 = $message->embed(\Swift_Image::fromPath('images/ico-google-plus.png'));
   $img4 = $message->embed(\Swift_Image::fromPath('images/ico-linkedin.png'));
   $img7 = $message->embed(\Swift_Image::fromPath('images/img-07.jpg'));
   $img8 = $message->embed(\Swift_Image::fromPath('images/img-08.jpg'));

   $img9 = $message->embed(\Swift_Image::fromPath('images/img-09.jpg'));
   $img10 = $message->embed(\Swift_Image::fromPath('images/img-10.jpg'));
   $img11 = $message->embed(\Swift_Image::fromPath('images/img-11.jpg'));
   $img12 = $message->embed(\Swift_Image::fromPath('images/img-12.jpg'));
   $messagereturn="New Message Sent To You by";





/*2*/
    $message->setBody($this->render('Emails/contact.html.twig', ['messagereturn'=>$messagereturn,'messagesusers' =>$_POST['message'], 'img' => $img,'img1' => $img1,'img2' => $img2,'img3' => $img3,'img4' => $img4,'img7' => $img7,'img8' => $img8,'img9' => $img9,'img10' => $img10,'img11' => $img11,'img12' => $img12,'messagesfrom' =>$_POST['name']]));
;

$mailer->send($message);
$message=$translator->trans('Message was sent successfully');
$this->get('session')->getFlashBag()->add(
    'messagesent',
    array(
       
        'message' => $message
    )
 );


return $this->redirectToRoute('cms_view_page',array('id'=>$id,'idsite'=>$ids));

}
public function SendMailForClient( TranslatorInterface $translator,\Swift_Mailer $mailer):Response
{

    $message = (new \Swift_Message())
    ->setFrom($_POST['email'])
    ->setSubject($_POST['subject'])
    ->setContentType('text/html')
    ->setTo("alifadhel619@gmail.com");
   // ->setBody($_POST['message'])
   $img = $message->embed(\Swift_Image::fromPath('images/logo_title.png'));
   $img1 = $message->embed(\Swift_Image::fromPath('images/ico-facebook.png'));
   $img2 = $message->embed(\Swift_Image::fromPath('images/ico-twitter.png'));
   $img3 = $message->embed(\Swift_Image::fromPath('images/ico-google-plus.png'));
   $img4 = $message->embed(\Swift_Image::fromPath('images/ico-linkedin.png'));
   $img7 = $message->embed(\Swift_Image::fromPath('images/img-07.jpg'));
   $img8 = $message->embed(\Swift_Image::fromPath('images/img-08.jpg'));

   $img9 = $message->embed(\Swift_Image::fromPath('images/img-09.jpg'));
   $img10 = $message->embed(\Swift_Image::fromPath('images/img-10.jpg'));
   $img11 = $message->embed(\Swift_Image::fromPath('images/img-11.jpg'));
   $img12 = $message->embed(\Swift_Image::fromPath('images/img-12.jpg'));
   $messagereturn="New Message Sent To You by";





/*2*/
    $message->setBody($this->render('Emails/contact.html.twig', ['messagereturn'=>$messagereturn,'messagesusers' =>$_POST['message'], 'img' => $img,'img1' => $img1,'img2' => $img2,'img3' => $img3,'img4' => $img4,'img7' => $img7,'img8' => $img8,'img9' => $img9,'img10' => $img10,'img11' => $img11,'img12' => $img12,'messagesfrom' =>$_POST['name']]));
;

$mailer->send($message);
$message=$translator->trans('Message was sent successfully');
$this->get('session')->getFlashBag()->add(
    'messagesent',
    array(
       
        'message' => $message
    )
 );


return $this->redirectToRoute('index');

}

public function ViewServices(SiteRepository $siteRepository,ServicesRepository $serv,$id): Response
{
    $em=$this->getDoctrine()->getManager();
    $user = $this->getUser();
    $username = $user->getUsername();
   $site =$siteRepository->find($id);
return $this->render('default/List_services.html.twig',array('site'=>$site));

}


public function DeleteServices(SiteRepository $siteRepository,$id,$ids,ServicesRepository $serv): Response
    {
        $em=$this->getDoctrine()->getManager();
        $service=$serv->find($ids);
       $site =$siteRepository->find($id);
       $site->removeService($service); 
            $em->flush();
            return $this->redirect($this->generateUrl('cms_site_view_services',array('id'=>$id)));    
    }






    public function ViewPage(PageRepository $pagerep,UserRepository $user,SiteRepository $siteRepository,MenuRepository $mn,PaginatorInterface $paginator,Request $request,$id,$idsite): Response
    {
                $em=$this->getDoctrine()->getManager();
                $site=$siteRepository->find($idsite);
                $page=$pagerep->find($id);

               
                if ($site->getTheme()=="default"  || $site->getTheme()=="") {
                    return $this->render('frontend/fontsite/view_page.html.twig', array(
           
                    'site' => $site,
                    'page' => $page, 
                ));
                } 
                elseif ($site->getTheme()=="template1") {
                  
                    return $this->render('frontend/fontsite/template1/view_page.html.twig', array(
           
                        'site' => $site,
                        'page' => $page,
                    
                ));
                }
                elseif ($site->getTheme()=="template2") {
                  
                    return $this->render('frontend/fontsite/template2/view_page.html.twig', array(
           
                        'site' => $site,
                        'page' => $page,
                ));
                }
                elseif ($site->getTheme()=="template3") {
                    return $this->render('frontend/fontsite/template3/view_page.html.twig', array(
                        'site' => $site,
                        'page' => $page,
                    
                ));
                }     
                elseif ($site->getTheme()=="template4") {
                    return $this->render('frontend/fontsite/template4/view_page.html.twig', array(
           
                        'site' => $site,
                        'page' => $page,
                    
                ));
                } 
            
            
            
            
            
            
            
            }

    



            //serach action



            public function Search(ArticlesRepository $art,UserRepository $user,SiteRepository $siteRepository,MenuRepository $mn,$id,PaginatorInterface $paginator,FAQRepository $faq,Request $request): Response
            {
                        $em=$this->getDoctrine()->getManager();
                        $site=$siteRepository->find($id);
                        $gg=$_GET['search'];
                        if(strtoupper($gg)==strtoupper("blog"))
                        {
                            return $this->redirectToRoute('list_blog_front',array('id'=>$id));
                        }
                       elseif(strtoupper($gg)==strtoupper("faq"))
                        {
                            return $this->redirectToRoute('list_faq_front',array('id'=>$id));
                        }
                        elseif(strtoupper($gg)==strtoupper("home"))
                        {
                            return $this->redirectToRoute('cms_site_view',array('name'=>$site->getName()));
                        }
                        else {
                       
                        if ($site->getTheme()=="default"  || $site->getTheme()=="") {
                            return $this->render('frontend/fontsite/NotFound.html.twig', array(
                   
                            'site' => $site,
                           
                            
                        ));
                        } 
                        elseif ($site->getTheme()=="template1") {
                           
                            return $this->render('frontend/fontsite/template1/NotFound.html.twig', array(
                   
                            'site' => $site,
                        
                            
                        ));
                        }
                        elseif ($site->getTheme()=="template2") {
                          
                            return $this->render('frontend/fontsite/template2/NotFound.html.twig', array(
                   
                            'site' => $site,
                           
                            
                        ));
                        }
                        elseif ($site->getTheme()=="template3") {
                            return $this->render('frontend/fontsite/template3/NotFound.html.twig', array(
                   
                            'site' => $site,
                           
                            
                        ));
                        }     
                        elseif ($site->getTheme()=="template4") {
                            return $this->render('frontend/fontsite/template4/NotFound.html.twig', array(
                   
                            'site' => $site,
                            
                            
                        ));
}} }



    /**
     * @Route("/create-checkout-session", name="checkout")
     */
    public function checkout(Request $request)
    {

        \Stripe\Stripe::setApiKey('sk_test_51IfYN7FoJvPd8QoJbBwp7eM5VYG2mgyM28KAhiGv2WURCvIVYEj2HTJMyEaRv2qor0dZGrR2tR7PXstPETSFyW9Z00yljHgN7n');
        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
              'price_data' => [
                'currency' => 'Eur',
                'product_data' => [
                  'name' => 'Any Product',
                ],
                'unit_amount' => 2000,
              ],
              'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('success',[],UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('error',[],UrlGeneratorInterface::ABSOLUTE_URL),
          ]);
          return new JsonResponse([ 'id' => $session->id ]);
    }

    /**
     * @Route("/success", name="success")
     */
    public function success():Response 
    {
        return $this->render('default/success.html.twig');
    }
    /**
     * @Route("/error", name="error")
     */
    public function error():Response 
    {
        return $this->render('default/error.html.twig');

    }


    // functionnality for user invit


    public function ViewInvitPost(ArticlesRepository $artrep,UserRepository $userrep,$name,$idsite,UserInvitRepository $ui,Request $request):Response
    {
        $user=$userrep->findOneByUsername($name);

        $em=$this->getDoctrine()->getManager();
        $user2=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($user->getUsername());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user2 && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR') || $tt->hasRoles('ROLE_AUTHOR'))) {
            return $this->render('Invits/post/view.html.twig', array(
                   
                'createdby' => $name,
                'list_art' => $artrep->findByUsers($user),
                'idsite' => $idsite,
                
                
            ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous articles de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل مقالاتك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Artikel auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your posts in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            }
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
        $langue= $request->getSession()->get('_locale');
        if ($langue=="fr") {
            $notif = $this->manager->createNotification('Droit uilistion!');
            $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous articles de manière non authorisèe');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="ar") {
            $notif = $this->manager->createNotification('  حذاري!');
            $notif->setMessage('الشخص  يريد تعديل مقالاتك بطريقة غير مصرح بها ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="de") {
            $notif = $this->manager->createNotification('Nutzungsrecht!');
            $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Artikel auf nicht autorisierte Weise ändern');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } else {
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'wants access to your posts in a  unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        }
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
    
    }


    public function ViewInvitPage(Request $request,PageRepository $pagerep,UserRepository $userrep,$idsite,$name,UserInvitRepository $ui):Response
    {
        $user=$userrep->findOneByUsername($name);
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);

        $em=$this->getDoctrine()->getManager();
        $user2=$this->getUser();
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user2 && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
            return $this->render('Invits/page/view.html.twig', array(
                   
                'createdby' => $name,
                'idsite' =>$idsite, 
                'page' => $site->getPages(),
                
                
            ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous page de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل صفحاتك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Seite auf nicht autorisierte Weise ändern  ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your pages in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);}
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
        $langue= $request->getSession()->get('_locale');
        if ($langue=="fr") {
            $notif = $this->manager->createNotification('Droit uilistion!');
            $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous page de manière non authorisèe');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="ar") {
            $notif = $this->manager->createNotification('  حذاري!');
            $notif->setMessage('الشخص  يريد تعديل صفحاتك بطريقة غير مصرح بها ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="de") {
            $notif = $this->manager->createNotification('Nutzungsrecht!');
            $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Seite auf nicht autorisierte Weise ändern ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } else {
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'wants access to your pages in a  unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);}
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }
       

    }
    public function ViewBlockPage(PageRepository $pagerep,UserRepository $userrep,$id,$idsite,UserInvitRepository $ui):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($idsite);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$idsite){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_EDITOR') || $tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/page/viewblock.html.twig', array(
                   
          
            'page' => $this->getDoctrine()->getRepository(Page::class)->find($id),
            'idsite'=>$idsite,
            
        ));
           }
           else{
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }

    public function ViewServicesInvit(PageRepository $pagerep,UserRepository $userrep,$id,UserInvitRepository $ui,Request $request):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if ($tt->hasRoles('ROLE_MODERATOR')) {
               return $this->render('Invits/Sites/viewservices.html.twig', array(
                   
          
            'services' => $site->getServices(),
            'idsite'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous blocs pour pages de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل صفحاتك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Seite auf nicht autorisierte Weise ändern  ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your pages blocks in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);}
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
        $langue= $request->getSession()->get('_locale');
        if ($langue=="fr") {
            $notif = $this->manager->createNotification('Droit uilistion!');
            $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut modifier vous blocs pour pages de manière non authorisèe');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="ar") {
            $notif = $this->manager->createNotification('  حذاري!');
            $notif->setMessage('الشخص  يريد تعديل صفحاتك بطريقة غير مصرح بها ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="de") {
            $notif = $this->manager->createNotification('Nutzungsrecht!');
            $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre Seite auf nicht autorisierte Weise ändern ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } else {
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'wants access to your pages blocks in a  unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);}
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }

    public function ViewSiteInvit(PageRepository $pagerep,UserRepository $userrep,$id,UserInvitRepository $ui,Request $request):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/viewblocksite.html.twig', array(
                   
          
            'site' => $site,
            'idsite'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut accèes a vous site blocs  de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل مواقعك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre WebSeite auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your website blocks in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);}
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }

    public function ViewMenuInvit(Request $request,PageRepository $pagerep,UserRepository $userrep,$id,UserInvitRepository $ui):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/viewblockMenu.html.twig', array(
                   
          
            'menus' => $site->getMenus(),
            'idsite'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut accèes a vous site menu  de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل مواقعك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre WebSeite Menu auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your website Menu  blocks in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);}
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
        $langue= $request->getSession()->get('_locale');
        if ($langue=="fr") {
            $notif = $this->manager->createNotification('Droit uilistion!');
            $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut accèes a vous site menu  de manière non authorisèe');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="ar") {
            $notif = $this->manager->createNotification('  حذاري!');
            $notif->setMessage('الشخص  يريد تعديل مواقعك بطريقة غير مصرح بها ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="de") {
            $notif = $this->manager->createNotification('Nutzungsrecht!');
            $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre WebSeite Menu auf nicht autorisierte Weise ändern ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } else {
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'wants access to your website Menu  blocks in a  unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);}
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }
    public function ViewFooterInvit(Request $request,PageRepository $pagerep,UserRepository $userrep,$id,UserInvitRepository $ui):Response
    {
        $user=$this->getUser();
        $site=$this->getDoctrine()->getRepository(Site::class)->find($id);
        $userinvit=$ui->findByCreatedby($site->getAuthor());
        $test= false;
        $tt=null;
        foreach($userinvit as $usr){
            if ($usr->getReceiverinvit()==$user && $usr->getSites()->getId()==$id){
                $test=true;
                $tt=$usr;
            }
        }
       if ($test==true) {
           if (($tt->hasRoles('ROLE_MODERATOR'))) {
               return $this->render('Invits/Sites/viewblockFooter.html.twig', array(
                   
          
            'footers' => $site->getFooters(),
            'idsite'=>$id,
            
        ));
           }
           else{
            $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
            $langue= $request->getSession()->get('_locale');
            if ($langue=="fr") {
                $notif = $this->manager->createNotification('Droit uilistion!');
                $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut accèes a vous site footer  de manière non authorisèe');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="ar") {
                $notif = $this->manager->createNotification('  حذاري!');
                $notif->setMessage('الشخص  يريد تعديل مواقعك بطريقة غير مصرح بها ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } elseif ($langue=="de") {
                $notif = $this->manager->createNotification('Nutzungsrecht!');
                $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre WebSeite Footer auf nicht autorisierte Weise ändern ');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);
            } else {
                $notif = $this->manager->createNotification('unauthorized User!');
                $notif->setMessage('the are a user named '.$this->getUser().'wants access to your website Footer  blocks in a  unauthorized way');
                $notif->setLink('https://Youdev-it.com/');
                // or the one-line method :
                // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
      
                // you can add a notification to a list of entities
                // the third parameter `$flush` allows you to directly flush the entities
                $this->manager->addNotification(array($userrecivernot), $notif, true);}
            return $this->render('Invits/page/erreur_permissions.html.twig');

           }
       }
       else{
        $userrecivernot=$userrep->findOneByUsername($site->getAuthor());
        $langue= $request->getSession()->get('_locale');
        if ($langue=="fr") {
            $notif = $this->manager->createNotification('Droit uilistion!');
            $notif->setMessage('le personne dont le nom est '.$this->getUser().' veut accèes a vous site footer  de manière non authorisèe');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="ar") {
            $notif = $this->manager->createNotification('  حذاري!');
            $notif->setMessage('الشخص  يريد تعديل مواقعك بطريقة غير مصرح بها ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } elseif ($langue=="de") {
            $notif = $this->manager->createNotification('Nutzungsrecht!');
            $notif->setMessage('die Person, deren Name ist '.$this->getUser().'möchte Ihre WebSeite Footer auf nicht autorisierte Weise ändern ');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);
        } else {
            $notif = $this->manager->createNotification('unauthorized User!');
            $notif->setMessage('the are a user named '.$this->getUser().'wants access to your website Footer  blocks in a  unauthorized way');
            $notif->setLink('https://Youdev-it.com/');
            // or the one-line method :
            // $manager->createNotification('Notification subject', 'Some random text', 'https://google.fr/');
  
            // you can add a notification to a list of entities
            // the third parameter `$flush` allows you to directly flush the entities
            $this->manager->addNotification(array($userrecivernot), $notif, true);}
        return $this->render('Invits/page/erreur_permissions.html.twig');

       }

    }


    public function changeLocale($locale, Request $request)
{
    // On stocke la langue dans la session
    $request->getSession()->set('_locale', $locale);

    // On revient sur la page précédente
    return $this->redirect($request->headers->get('referer'));
}

// function for user manipulations.................
public function ListUsers(UserRepository $userrep):Response
{

return $this->render('Users/list.html.twig',array(
'listuser'=>$userrep->findAll()
));

}

public function UpdateUsers(UserRepository $userrep,Request $request,$id):Response
{

    $user = $userrep->find($id);
    $dispatcher = $this->get('event_dispatcher');

    $event = new tt3($user, $request);
    $dispatcher->dispatch(tt5::PROFILE_EDIT_INITIALIZE, $event);

    if (null !== $event->getResponse()) {
        return $event->getResponse();
    }


    $form = $this->formFactory->createForm();
    $form->setData($user);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
    
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
           // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $user->getPhoto();
    
              if ($file != null) {
                  // Generate a unique name for the file before saving it
                  $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                  $imageDir = $this->getParameter('kernel.root_dir') . '/../public/uploads/img/experts';
                  // Generate a unique name for the file before saving it
                  $file->move($imageDir, $fileName);
                  $user->setPhotoname($fileName);
                  $user->setPhoto($fileName);
              }
        $event = new tt2($form, $request);
        $dispatcher->dispatch(tt5::PROFILE_EDIT_SUCCESS, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('cms_users_list');
            $response = new RedirectResponse($url);
        }


        $dispatcher->dispatch(tt5::PROFILE_EDIT_COMPLETED, new tt($user, $request, $response));

        return $response;
    }

    return $this->render('default/modifier_user.html.twig', array(
        'form' => $form->createView(),
        'user'=>$user,
    ));
}





public function EnableDisableUser(UserRepository $userrep,$id){
$user=$userrep->find($id);
if($user->isEnabled()==true){
    $user->setEnabled(false);
    return $this->redirectToRoute('cms_users_list');
}
else{
    $user->setEnabled(true);
    return $this->redirectToRoute('cms_users_list');
}

}

public function DeleteUsers(UserRepository $userrep,$id){

$user=$userrep->find($id);
if($this->getUser()->hasRole('ROLE_ADMIN')){
    $em=$this->getDoctrine()->getManager();
    $em->remove($user);
    $em->flush();
    return $this->redirectToRoute('cms_users_list');

}
else{
    return $this->render('articles/erreur_permissions.html.twig');

}





}





    /**
     * @Route("/your_notifications/{identifier}", name="youdev_notificationbyusers", methods={"GET"})
     * @param NotifiableInterface $identifier
     */
    public function listbyuserAction($identifier,PaginatorInterface $paginator,Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity =  $this->notifiableRepository->findOneByIdentifier($identifier);
                 if($entity==null)
        {
           $this->get('session')->getFlashBag()->add(
    'not',
    array(
        'message' => 'User ne possed pas des notifications.'
    )
); 
           return $this->render('default/index.html.twig');
           
        }
        else{
        
        
        
        
        $notifiable = $entity->getId();
   
        $pagination1=$this->notifiableNotificationRepository->findAllForNotifiableId($notifiable);
        $notif = $paginator->paginate(
            $pagination1, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            5 /*limit per page*/
        );
    
        return $this->render('default/notificationbyuser.html.twig', array(
            'notifiableNotifications' =>$notif     ));
    }
    }



  /**
     * @Route("/{notifiable}/mark_as_seen/{lu}", name="seen_list2",methods={"GET"})
     */
    public function markAsSeenAction($notifiable, $notification)
    {
        $this->manager->markAsSeen(
            $this->manager->getNotifiableInterface( $this->manager->getNotifiableEntityById($notifiable)),
            $this->manager->getNotification($notification),
            true
        );

        return new JsonResponse(true);
    }


    /**
     * @Route("/abs/{lu}", name="seen_list", methods={"GET"})
     */
   public function luAction($lu):JsonResponse
           
   {


$em = $this->getDoctrine()->getManager();
$tt= $this->notificationRepository->findById($lu);
$en =  $this->notifiableNotificationRepository->findOneByNotification($lu);
if($en->isSeen()){
$en->setSeen(TRUE);
$em->flush();
}
else {

$en->setSeen(TRUE);
$em->flush();
}
return new JsonResponse(true);
}


// mark all notifications as seen

    /**
     * @Route("/noti/", name="youdev_seen_notification",methods={"GET"})
     */
    public function notiAction()
           
    {
   
   
       $em = $this->getDoctrine()->getManager();
   
   $entity =  $this->notifiableNotificationRepository->findAll();
   
   foreach ($entity as $value) {
         if($value->isSeen()){
    $value->setSeen(TRUE);
    $em->flush();
}
else {
    
    $value->setSeen(TRUE);
    $em->flush();
}
   
       
   }


   
   
       return new JsonResponse(true);
  }   






  public function ViewNewsFront($name,SiteRepository $siteRepository,PaginatorInterface $paginator,Request $request,NewsRepository $newsrep,$id): Response
  {
  $em=$this->getDoctrine()->getManager();
  $site=$siteRepository->find($id);
  $pagination=$site->getNews();
  $news = $paginator->paginate(
      $pagination, /* query NOT result */
      $request->query->getInt('page', 1), /*page number*/
      4 /*limit per page*/
  );
  if ($site->getTheme()=="default" || $site->getTheme()=="") {
      return $this->render('frontend/fontsite/view_news_front.html.twig', array(

  'site' => $site,
  'news'=> $news,

  ));
  }
 elseif ($site->getTheme()=="template1") {
      return $this->render('frontend/fontsite/template1/view_news_front.html.twig', array(

        'site' => $site,
        'news'=> $news,

  ));
  }    
  elseif ($site->getTheme()=="template2") {
      return $this->render('frontend/fontsite/template2/view_news_front.html.twig', array(

        'site' => $site,
        'news'=> $news,

  ));
  } 
  elseif ($site->getTheme()=="template3") {
      return $this->render('frontend/fontsite/template3/view_news_front.html.twig', array(

        'site' => $site,
        'news'=> $news,

  ));
  } 
  elseif ($site->getTheme()=="template4") {
      return $this->render('frontend/fontsite/template4/view_news_front.html.twig', array(

        'site' => $site,
        'news'=> $news,

  ));
  } 







}

}






