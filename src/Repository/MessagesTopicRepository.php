<?php

namespace App\Repository;

use App\Entity\MessagesTopic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MessagesTopic|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessagesTopic|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessagesTopic[]    findAll()
 * @method MessagesTopic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesTopicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessagesTopic::class);
    }

    // /**
    //  * @return MessagesTopic[] Returns an array of MessagesTopic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessagesTopic
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
