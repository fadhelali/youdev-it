<?php

namespace App\Repository;

use App\Entity\UserInvit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserInvit|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInvit|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInvit[]    findAll()
 * @method UserInvit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserInvitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserInvit::class);
    }

    // /**
    //  * @return UserInvit[] Returns an array of UserInvit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserInvit
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
