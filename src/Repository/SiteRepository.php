<?php

namespace App\Repository;

use App\Entity\Site;
use App\Entity\User;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Site|null find($id, $lockMode = null, $lockVersion = null)
 * @method Site|null findOneBy(array $criteria, array $orderBy = null)
 * @method Site[]    findAll()
 * @method Site[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Site::class);
    }
    public function findByAuthor($value){
        //$user=$this->getDoctrine()->getRepository(User::class)->findAll();
      
        //$site=$this->getDoctrine()->getRepository(Site::class)->findByAuthor($user->getUsername());
        return $this->createQueryBuilder('p')
                 ->andWhere('p.author = :val')
                 ->setParameter('val',$value )
                 ->getQuery()
                 ->getResult();
     
     
     
     
         }
    // /**
    //  * @return Site[] Returns an array of Site objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Site
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function CountSitesByDate($value){
    $query=$this->createQueryBuilder('a')
             ->Select('SUBSTRING(a.created_at,1,7) as createddate,COUNT(a) as countsite')
             ->andWhere('a.author = :val')
             ->setParameter('val',$value)
             ->groupBy('createddate');

            return $query->getQuery()->getResult();




    }
}
