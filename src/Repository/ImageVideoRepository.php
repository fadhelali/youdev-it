<?php

namespace App\Repository;

use App\Entity\ImageVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageVideo[]    findAll()
 * @method ImageVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageVideo::class);
    }

    // /**
    //  * @return ImageVideo[] Returns an array of ImageVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageVideo
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
