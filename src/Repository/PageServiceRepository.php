<?php

namespace App\Repository;

use App\Entity\PageService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PageService|null find($id, $lockMode = null, $lockVersion = null)
 * @method PageService|null findOneBy(array $criteria, array $orderBy = null)
 * @method PageService[]    findAll()
 * @method PageService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PageService::class);
    }

    // /**
    //  * @return PageService[] Returns an array of PageService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PageService
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
