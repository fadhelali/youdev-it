<?php
// src/Twig/AppExtension.php
namespace App\Twig;

use App\Entity\Forum;
use App\Entity\Site;
use App\Repository\ForumRepository;
use App\Repository\SiteRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Environment;
use Twig\Extra\TwigExtraBundle\DependencyInjection\TwigExtraExtension;
use Twig\Template;
use Symfony\Component\HttpKernel\KernelInterface;

class AppExtension extends AbstractExtension
{
    protected $twig;
    protected $doctrine;
    protected $forumrep;



    public function __construct(EntityManagerInterface $em,\Twig\Environment $twig,ForumRepository $rep)
    {
        $this->twig=$twig;
        $this->doctrine=$em;
        $this->forumrep=$rep;
    }
   
   public function getFilters(): array
    {
        return [
            new TwigFilter('filter_name', [$this, 'ListForum']),
        ];
    }
    public function getFunctions()
    {
        return array(
            // on déclare notre fonction.
            // Le 1er paramètre est le nom de la fonction utilisée dans notre template
            // le 2ème est un tableau dont le 1er élément représente la classe où trouver la fonction associée (en l'occurence $this, c'est à dire cette classe puisque notre fonction est déclarée un peu plus bas). Et le 2ème élément du tableau est le nom de la fonction associée qui sera appelée lorsque nous l'utiliserons dans notre template.
            new TwigFunction('youdev_cms_forum', array($this, 'ListForum')),
        );
    }

    // chemin relatif de notre fichier en paramètre
    public function ListForum(Site $sites)
    {
        
       $site=$this->doctrine->getRepository(Site::class)->find($sites->getId());
       $forum=$this->forumrep->findByCreatedby($sites->getAuthor());
      if($forum != null){
          return true;
      }
      else{
          return false;
      }

    }
}
