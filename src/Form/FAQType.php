<?php

namespace App\Form;
use App\Entity\Site;

use App\Entity\FAQ;
use App\Repository\SiteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Security;

class FAQType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $builder
            ->add('question')
            ->add('reponse',\Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array('attr' => array('rows' => '5','cols' => '5')))
            ->add('sites', EntityType::class, [
                'class' => Site::class,
                'query_builder' => function (SiteRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.author = :user')
                    ->setParameter('user', $user->getUsername())
                ;
 },
                'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FAQ::class,
        ]);
    }
}
