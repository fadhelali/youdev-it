<?php

namespace App\Form;

use App\Entity\Forum;
use App\Entity\Topic;
use App\Repository\ForumRepository;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class TopicFormType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('titre')
            ->add('contenu',FroalaEditorType::class, [
                'froala_language'      => 'fr',
                'froala_toolbarInline' => true,
                'froala_tableColors'   => ['#FFFFFF', '#FF0000'],
                'froala_saveParams'    => ['id' => 'myEditorField'],
                'required'=>false,
            ])
            
            ->add('formus', EntityType::class, [
                'class' => Forum::class,
                'query_builder' => function (ForumRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername())
                ;
 },
                'choice_label' => 'name',
                

            ]);
        
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Topic::class,
        ]);
    }
}
