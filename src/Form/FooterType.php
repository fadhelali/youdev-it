<?php

namespace App\Form;

use App\Entity\Footer;
use App\Entity\Site;
use App\Repository\SiteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FooterType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('adresse')
            ->add('phone')
            ->add('emailcontact')
            ->add('sites',EntityType::class, [
                'class' => Site::class,
                'query_builder' => function (SiteRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.author = :user')
                    ->setParameter('user', $user->getUsername());
 },
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('linkfacebook')
            ->add('linktwitter')
            ->add('linkgithub')
            ->add('linklinkedin');
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Footer::class,
        ]);
    }
}
