<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\User;
use App\Entity\UserInvit;
use App\Repository\SiteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserInviType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
        ->add('role', ChoiceType::class, [
            'choices'  => [
                'Moderator' => "ROLE_MODERATOR",
                'EDITOR' => "ROLE_EDITOR",
                'AUTHER' => "ROLE_AUTHOR",
            ],
        'multiple'=>false,'expanded'=>true

            ])            
        ->add('receiverinvit',EntityType::class, [
                'class' => User::class,
                'choice_label' => 'email',
            ])
        ->add('sites',EntityType::class, [
                'class' => Site::class,
                'query_builder' => function (SiteRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.author = :user')
                    ->setParameter('user', $user->getUsername());
                },
                'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInvit::class,
        ]);
    }
}
