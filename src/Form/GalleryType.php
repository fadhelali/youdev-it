<?php

namespace App\Form;

use App\Entity\Gallery;
use App\Entity\Page;
use App\Repository\PageRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class GalleryType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('name')
            
            ->add('pages',EntityType::class, [
                'class' => Page::class,
                'query_builder' => function (PageRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername());
 },
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('images', FileType::class,[
                'label' => false,
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);
    }
}
