<?php

namespace App\Form;

use App\Entity\Services;
use App\Entity\Site;
use App\Repository\ServicesRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SiteType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
           
            ->add('imageFile',VichImageType::class, [
                'delete_label' => 'Remove ',
                'required'=>false,

                

            ])
            ->add('services', EntityType::class,
            [
                'class' => Services::class,
                'query_builder' => function (ServicesRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername())
                ;
 },
                'choice_label' => 'name',
                'multiple' => true,
                'required'=>false,
            ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
