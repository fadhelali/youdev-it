<?php

namespace App\Form;

use App\Entity\News;
use App\Entity\Site;
use App\Repository\SiteRepository;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class NewsType extends AbstractType
{


    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('title')
            ->add('content',FroalaEditorType::class, [
                'froala_language'      => 'fr',
                'froala_toolbarInline' => true,
                'froala_tableColors'   => ['#FFFFFF', '#FF0000'],
                'froala_saveParams'    => ['id' => 'myEditorField'],
            ])
            ->add('sites',EntityType::class, [
                'class' => Site::class,
                'query_builder' => function (SiteRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.author = :user')
                    ->setParameter('user', $user->getUsername());
 },
                'choice_label' => 'name',
                'required' => false,
                'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
