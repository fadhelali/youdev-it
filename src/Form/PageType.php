<?php

namespace App\Form;

use App\Entity\Page;
use App\Entity\Site;
use App\Entity\User;
use App\Repository\PageRepository;
use App\Repository\SiteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Form\Type;
use Symfony\Component\Security\Core\Security;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;


class PageType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('name')
            ->add('description',\Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array('attr' => array('rows' => '5','cols' => '5')))
            ->add('content',FroalaEditorType::class, [
                'froala_language'      => 'fr',
                'froala_toolbarInline' => true,
                'froala_tableColors'   => ['#FFFFFF', '#FF0000'],
                'froala_saveParams'    => ['id' => 'myEditorField'],
                'required'=>false,
            ])
            ->add('sites', EntityType::class, [
                'class' => Site::class,
                'query_builder' => function (SiteRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.author = :user')
                    ->setParameter('user', $user->getUsername())
                ;
 },
                'choice_label' => 'name',
                'multiple' => true,'required' => false,

            ]);
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
