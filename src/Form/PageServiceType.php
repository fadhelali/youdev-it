<?php

namespace App\Form;

use App\Entity\Page;
use App\Entity\PageService;
use App\Repository\PageRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PageServiceType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        $builder
            ->add('name')
            ->add('description',\Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array('attr' => array('rows' => '5','cols' => '5')))
            ->add('imageFile',VichImageType::class, [
                'delete_label' => 'Remove file',
                'required'=>false,
                'allow_extra_fields'=>true,
                'allow_file_upload'=>true,

                 ])
            ->add('pages',EntityType::class, [
                'class' => Page::class,
                'query_builder' => function (PageRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername());
 },
                'choice_label' => 'name',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PageService::class,
        ]);
    }
}
