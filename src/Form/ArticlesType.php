<?php

namespace App\Form;

use App\Entity\Articles;
use App\Entity\MotsCles;
use App\Entity\Categories;
use App\Repository\CategoriesRepository;
use App\Repository\MotsClesRepository;
use App\Repository\UserInvitRepository;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Count;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Security\Core\Security;

class ArticlesType extends AbstractType
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
    

        $builder
            ->add('titre')
            ->add('slug')
            ->add('contenu',\Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array('attr' => array('rows' => '5','cols' => '5')))
            ->add('imageFile',VichImageType::class, [
                'delete_label' => 'Remove file',
                'required'=>false,

            ]

            )
            ->add('mots_cles', EntityType::class,
            [
                'class' => MotsCles::class,
                'query_builder' => function (MotsClesRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                        
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername())

                ;
 },
                'choice_label' => 'mot_cle',
                'multiple' => true,
                'required'=>false,
            ]
            )
            ->add('categories', EntityType::class,
            [
                'class' => Categories::class,
                'query_builder' => function (CategoriesRepository $ur)  use ($user){
                    return $ur->createQueryBuilder('u')
                    ->where('u.createdby = :user')
                    ->setParameter('user', $user->getUsername())
                ;
 },
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false,
                'attr' => ['class' => 'tinymce'],
                'trim'=>true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Articles::class,
        ]);
    }
}
